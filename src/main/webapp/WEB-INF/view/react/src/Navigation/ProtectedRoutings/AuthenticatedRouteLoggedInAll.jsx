import AuthenticationService from 'Support/LoginService/AuthenticationService'
import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'

class AuthenticatedRouteLoggedInAll extends Component {
    render() {
        if (AuthenticationService.isUserLoggedIn()) {
            return <Route {...this.props} />
        } else {
            return <Redirect to="/login" />
        }
    }
}

export default AuthenticatedRouteLoggedInAll
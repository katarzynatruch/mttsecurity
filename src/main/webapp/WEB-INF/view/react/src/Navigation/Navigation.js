import About from "../Components/Pages/CommonComponents/About";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect,
} from "react-router-dom";
import React from "react";
import Logout from "Components/Pages/AuthenticationComponents/Logout/Logout";
import AddEmployee from "Components/Pages/AuthenticationComponents/Registration/AddEmployee";
import ForgotPassword from "Components/Pages/AuthenticationComponents/Login/ForgotPassword";
import ResetPassword from "Components/Pages/AuthenticationComponents/Login/ResetPassword";
import PasswordChangedComponent from "Components/Pages/AuthenticationComponents/Login/PasswordChangedComponent";
import ResetTokenSentComponent from "Components/Pages/AuthenticationComponents/Login/ResetTokenSentComponent";
import ResetTokenExpired from "Components/Pages/AuthenticationComponents/Login/ResetTokenExpired";
import EmployeeManagement from "Components/Pages/ManagerComponents/EmployeeManagement/EmployeeManagement";
import AssignPassword from "Components/Pages/AuthenticationComponents/Registration/AssignPassword";
import ChangePassword from "Components/Pages/AuthenticationComponents/Login/ChangePassword";
import AvailabilityDiagramComponent from "Components/Pages/ManagerComponents/Availability/AvailabilityDiagramComponent";
import CapacityDiagramComponent from "Components/Pages/ManagerComponents/CapacityDiagram/CapacityDiagramComponent";
import HoursEstimationComponent
    from "Components/Pages/ManagerComponents/HoursEstimationDiagram/HoursEstimationComponent";
import ComboComponent from "Components/Pages/ManagerComponents/StatisticsComboDiagram/ComboComponent";
import EmployeesInTeamsManagement from "Components/Pages/ManagerComponents/TeamManagement/EmployeesInTeamsManagement";
import ProjectEmployeeManagement
    from "Components/Pages/ManagerComponents/ProjectEmployeesManagement/ProjectEmployeeManagement";
import AccountSettingsComponent from "../Components/Pages/CommonComponents/AccountSettingsComponent";
import AuthenticatedRouteAdmin from "./ProtectedRoutings/AuthenticatedRouteAdmin";
import AuthenticatedRouteManager from "./ProtectedRoutings/AuthenticatedRouteManager";
import AuthenticatedRouteLoggedInAll from "./ProtectedRoutings/AuthenticatedRouteLoggedInAll";
import LoginComponent from "../Components/Pages/AuthenticationComponents/Login/LoginComponent";
import LogoutComponent from "../Components/Pages/AuthenticationComponents/Login/LogoutComponent";
import NewProject from "../Components/Pages/ManagerComponents/ProjectManagement/NewProject";
import ProjectManagement from "../Components/Pages/ManagerComponents/ProjectManagement/ProjectManagement";
import TimeReport from "../Components/Pages/EmployeeComponents/TimeReport/TimeReport";
import TimeReportManagement from "../Components/Pages/ManagerComponents/LazyEmployees/TimeReportManagement";
import TopNavbar from "../Components/Navbar/TopNavbar";
import Homepage from "../Components/Pages/CommonComponents/Homepage";
import Footer from "../Components/Footer/Footer";
import Support from "../Components/Pages/CommonComponents/Support";
import SupportSent from "../Components/Pages/CommonComponents/SupportSent";
import NotLoggedHomepage from "../Components/Pages/CommonComponents/Registration/NotLoggedHomepage";
import OnSubmit from "../Components/Pages/CommonComponents/Registration/OnSubmit";
import ToolsMenu from "../Components/Pages/ManagerComponents/ToolsMenu";
import TimeReportMenu from "../Components/Pages/EmployeeComponents/TimeReportMenu";
import SessionTimeout from "../Support/SessionTimeout";
import AuthChecker from "../Support/AuthChecker";


const USERS_VIEW_URL = 'http://localhost:8080/admin/loadUsers';

const Navigation = () => {

    return (
        <Router>
            <div>
                <AuthChecker/>
                <Switch>
                    <Route exact path="/login" component={LoginComponent}>
                    </Route>
                    <Route exact path="/" component={LoginComponent}>
                    </Route>
                    <Route exact path="/logout=true" component={LogoutComponent}>
                    </Route>
                    <Route exact path="/sign-up">
                        <TopNavbar/>
                        <NotLoggedHomepage/>
                    </Route>
                    <Route exact path="/registration-ongoing">
                        <TopNavbar/>
                        <OnSubmit/>
                        <Footer/>
                    </Route>
                    <Route path="/homepage">
                        <TopNavbar/>
                        <Homepage/>
                    </Route>
                    <Route path="/support">
                        <TopNavbar/>
                        <Support/>
                        <Footer/>
                    </Route>
                    <Route path="/support-successful">
                        <TopNavbar/>
                        <SupportSent/>
                        <Footer/>
                    </Route>
                    <Route path="/about">
                        <TopNavbar/>
                        <About/>
                        <Footer/>
                    </Route>
                    <AuthenticatedRouteAdmin path="/admin/about">
                        <TopNavbar/>
                        <Logout/>
                    </AuthenticatedRouteAdmin>
                    <AuthenticatedRouteManager exact path="/add-employee">
                        <TopNavbar/>
                        <AddEmployee/>
                    </AuthenticatedRouteManager>
                    <Route exact path="/forgot-password">
                        <TopNavbar/>
                        <ForgotPassword/>
                        <Footer/>
                    </Route>
                    <Route exact path="/reset-password">
                        <TopNavbar/>
                        <ResetPassword/>
                        <Footer/>
                    </Route>
                    <Route exact path="/assign-password">
                        <TopNavbar/>
                        <AssignPassword/>
                        <Footer/>
                    </Route>
                    <AuthenticatedRouteLoggedInAll exact path="/change-password">
                        <TopNavbar/>
                        <ChangePassword/>
                        <Footer/>
                    </AuthenticatedRouteLoggedInAll>
                    <AuthenticatedRouteLoggedInAll exact path="/password-changed">
                        <TopNavbar/>
                        <PasswordChangedComponent/>
                        <Footer/>
                    </AuthenticatedRouteLoggedInAll>
                    <Route exact path="/password-reset-token">
                        <TopNavbar/>
                        <ResetTokenSentComponent/>
                        <Footer/>
                    </Route>
                    <Route exact path="/reset-token-expired">
                        <TopNavbar/>
                        <ResetTokenExpired/>
                        <Footer/>
                    </Route>
                    <AuthenticatedRouteLoggedInAll exact path="/account-settings">
                        <TopNavbar/>
                        <AccountSettingsComponent/>
                        <Footer/>
                    </AuthenticatedRouteLoggedInAll>
                    <AuthenticatedRouteManager exact path="/tools-menu">
                        <TopNavbar/>
                        <ToolsMenu/>
                        <Footer/>
                    </AuthenticatedRouteManager>
                    <AuthenticatedRouteManager exact path="/employee-management">
                        <TopNavbar/>
                        <EmployeeManagement/>
                        <Footer/>
                    </AuthenticatedRouteManager>
                    <AuthenticatedRouteManager exact path="/availability-diagram">
                        <TopNavbar/>
                        <AvailabilityDiagramComponent/>
                        <Footer/>
                    </AuthenticatedRouteManager>
                    <AuthenticatedRouteManager exact path="/capacity-diagram">
                        <TopNavbar/>
                        <CapacityDiagramComponent/>
                        <Footer/>
                    </AuthenticatedRouteManager>
                    <AuthenticatedRouteManager exact path="/hours-estimation-diagram">
                        <TopNavbar/>
                        <HoursEstimationComponent/>
                        <Footer/>
                    </AuthenticatedRouteManager>
                    <AuthenticatedRouteManager exact path="/combo-diagram">
                        <TopNavbar/>
                        <ComboComponent/>
                        <Footer/>
                    </AuthenticatedRouteManager>
                    <AuthenticatedRouteManager exact path="/team-management">
                        <TopNavbar/>
                        <EmployeesInTeamsManagement/>
                        <Footer/>
                    </AuthenticatedRouteManager>
                    <AuthenticatedRouteManager exact path="/project-employee-management">
                        <TopNavbar/>
                        <ProjectEmployeeManagement/>
                        <Footer/>
                    </AuthenticatedRouteManager>
                    <AuthenticatedRouteManager exact path="/project-management">
                        <TopNavbar/>
                        <ProjectManagement/>
                        <Footer/>
                    </AuthenticatedRouteManager>
                    <AuthenticatedRouteManager exact path="/project-add">
                        <TopNavbar/>
                        <NewProject/>
                        <Footer/>
                    </AuthenticatedRouteManager>
                    <AuthenticatedRouteLoggedInAll exact path="/time-report-menu">
                        <TopNavbar/>
                        <TimeReportMenu/>
                        <Footer/>
                    </AuthenticatedRouteLoggedInAll>
                    <AuthenticatedRouteLoggedInAll exact path="/time-report">
                        <TopNavbar/>
                        <TimeReport/>
                        <Footer/>
                    </AuthenticatedRouteLoggedInAll>
                    <AuthenticatedRouteManager exact path="/time-report-management">
                        <TopNavbar/>
                        <TimeReportManagement/>
                        <Footer/>
                    </AuthenticatedRouteManager>
                </Switch>
                <SessionTimeout/>
            </div>

        </Router>
    )
}

export default Navigation;
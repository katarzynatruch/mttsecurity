import AuthenticationService from 'Support/LoginService/AuthenticationService'
import React, { Component } from 'react'
import { Route, Redirect, useHistory } from 'react-router-dom'
import axios from 'axios'
import SupportService from "../../Support/SupportService";

const API_URL = 'http://localhost:8080'
export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'

class AuthenticatedRouteManager extends Component {

    constructor(props) {
        super(props)

        this.state = {
        }
    }

    loadRolesAndCheckIfManager() {
        const loggedInUserIdentificator = localStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        return axios.post(`${API_URL}/user/get_roles`, { loggedInUserIdentificator })
            .then(response => response.data)
            .then(data => {
                this.checkIfManager(data.userRoleNameList);
            })
            // .catch((error) => {
            //     if(error.response.status === 401){
            //         <Redirect to='/logout=true'/>
            //     }
            // })

    }
    checkIfManager(roles) {
        return SupportService.checkIfContainsManagerRole(roles)
    }
    
    render() {

        if (this.loadRolesAndCheckIfManager()) {
            return <Route {...this.props} />
        } else {
            return <Redirect to="/login" />
        }
    }
}

export default AuthenticatedRouteManager
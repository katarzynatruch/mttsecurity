import React, {useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import axios from 'axios';
import DoneIcon from '@material-ui/icons/Done';
import {green, red} from '@mui/material/colors';
import CloseIcon from '@material-ui/icons/Close';
import {Link, useHistory,} from "react-router-dom";
import AddCardRoundedIcon from "@mui/icons-material/AddCardRounded";
import "./EmployeeManagement.css"
import {styled} from '@mui/material/styles';
import {tableCellClasses} from "@mui/material";
import {useStateIfMounted} from "use-state-if-mounted";

const StyledTableCell = styled(TableCell)(({theme}) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: "#5f7375",
        color: theme.palette.common.white,
        fontWeight: 'bold',
    },
    [`&.${tableCellClasses.body}`]: {
        // fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({theme}) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: "#bccccf",
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

function Row(props) {
    const {row} = props;
    const [open, setOpen] = useStateIfMounted(false);
    const [activateButtonDisabled, setActivateButtonDisabled] = useStateIfMounted(false)
    let history = useHistory();


    const checkIfShouldBeDisabled = (row) => {
        if (row.registered) {
            setActivateButtonDisabled(true);
        } else {
            console.log(row.firstName)
            console.log(row.registered)
            setActivateButtonDisabled(false);
            axios.get(`http://localhost:8080/token/get-verification-token/${row.identificator}`)
                .then(response => {
                    if (response.data) {
                        console.log(response.data)
                        setActivateButtonDisabled(true);
                    }
                })
        }
    }

    useEffect(() => {
        if (row)
            checkIfShouldBeDisabled(row);
    }, [])

    const editButtonClicked = (user) => {
        //done
    }

    const activateButtonClicked = (user) => {
        axios.get(`http://localhost:8080/token/get-verification-token/${user.identificator}`)
            .then(response => {
                if (response.data) {
                    setActivateButtonDisabled(true);
                } else {
                    axios.post('http://localhost:8080/account/user/sendRegistrationToken', createJSONDataUserDTO(user))
                        .then(response => {
                            if (response.data.message) {
                                history.push(`/employee-management?msg=${response.data.message}`);
                                props.triggerUp();
                            }
                        })
                }

            })


    }

    const resendButtonClicked = (user) => {
        let token = '';
        axios.get(`http://localhost:8080/token/get-verification-token/${user.identificator}`)
            .then(response => {
                    token = response.data;
                    axios.get(`http://localhost:8080/account/user/resendRegistrationToken/${token}`, createJSONDataUserDTO(user))
                        .then(response => {
                            if (response.data.message) {
                                history.push(`/employee-management?msg=${response.data.message}`);
                                props.triggerUp();
                            }
                        })
                }
            )
    }

    const deactivateButtonClicked = (user) => {
        //TODO Window with confirmation that deactivation is permanent and user would not show again on the list
        props.setWarningWindow(user);

    }

    return (
        <React.Fragment>
            <TableRow>
                <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon/> : <KeyboardArrowDownIcon/>}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    {row.identificator}
                </TableCell>
                <TableCell align="left">{row.firstName}</TableCell>
                <TableCell align="left">{row.lastName}</TableCell>
                <TableCell align="left">{row.email}</TableCell>
                <TableCell align="left">
                    {row.registered ? <DoneIcon style={{color: green[500]}}/> :
                        <CloseIcon style={{color: red[500]}}/>}
                </TableCell>
                <TableCell align="left">
                    {row.activated ? <DoneIcon style={{color: green[500]}}/> :
                        <CloseIcon style={{color: red[500]}}/>}
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{paddingBottom: 0, paddingTop: 0}} colSpan={7}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={1}>
                            <Typography variant="h6" gutterBottom component="div">
                                Actions
                            </Typography>
                            <Table size="small">
                                <TableHead>
                                    <TableRow>
                                        <TableCell colSpan={6}>EDIT click to change user data</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell colSpan={6}>ACTIVATE click to change user data</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell colSpan={6}>RESEND click to resend registration mail with token
                                            to
                                            user in case of some issues</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell colSpan={6}>DEACTIVATE click if you would like to keep all data
                                            of
                                            this user. Deactivated user could not be managed anymore</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow>
                                        <TableCell>
                                            <Link to={{
                                                pathname: "/add-employee",
                                                state: {
                                                    firstName: row.firstName,
                                                    lastName: row.lastName,
                                                    email: row.email,
                                                    identificator: row.identificator,
                                                }
                                            }}
                                            >
                                                <button className="time-button"
                                                        onClick={() => editButtonClicked(row)}>EDIT
                                                </button>
                                            </Link>
                                        </TableCell>
                                        <TableCell>
                                            <button className="time-button"
                                                    disabled={activateButtonDisabled}
                                                    onClick={() => activateButtonClicked(row)}>ACTIVATE
                                            </button>
                                        </TableCell>
                                        <TableCell>
                                            <button className="time-button"
                                                    onClick={() => resendButtonClicked(row)}>RESEND
                                            </button>
                                        </TableCell>
                                        <TableCell>
                                            <button className="time-button"
                                                    onClick={() => deactivateButtonClicked(row)}>DEACTIVATE
                                            </button>
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

const createJSONDataUserDTO = (user) => {
    return {
        firstName: user.firstName,
        lastName: user.lastName,
        identificator: user.identificator,
        email: user.email,
        enabled: user.registered,
        active: user.activated,
    };
}

Row.propTypes = {
    row: PropTypes.shape({
        identificator: PropTypes.string.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired,
    }).isRequired,
};

function EmployeeManagement() {
    const [rows, setRows] = useStateIfMounted([]);
    const [trigger, setTrigger] = useStateIfMounted(0);
    const [shouldDisplayInfoWindow, setShouldDisplayInfoWindow] = useStateIfMounted(false);
    const [shouldDisplayWarningWindow, setShouldDisplayWarningWindow] = useStateIfMounted(false);
    const [tempUserToDeactivate, setTempUserToDeactivate] = useStateIfMounted('');
    let history = useHistory();

    useEffect(() => {
        loadUsersData();
        if (new URLSearchParams(window.location.search).has("msg")) {
            setShouldDisplayInfoWindow(true);
        }
        return cleanupSub();
    }, [trigger])

    const triggerUp = () => {
        setTrigger(trigger + 1);
    }

    const cleanupSub = () => {
        setRows([]);
    }

    const createData = (identificator, firstName, lastName, email, registered, activated) => {
        setRows(rows => [...rows, {identificator, firstName, lastName, email, registered, activated}]);
    }

    const loadUsersData = () => {
        axios.get('http://localhost:8080/user/load-list-all')
            .then(response => response.data)
            .then(data => {
                Array.from(data.users)
                    .map(user => {
                        createData(user.identificator, user.firstName, user.lastName, user.email, user.enabled, user.active)
                    })
            })
    }

    const MessageWindow = () => {
        const message = new URLSearchParams(window.location.search).get("msg");
        let startTimerInterval = useRef();
        startTimerInterval.current = setTimeout(() => {
            setShouldDisplayInfoWindow(false);
            history.push("/employee-management")
        }, (2500));
        return (
            <div className="message-window">
                {message}
            </div>
        )
    }

    const WarningWindow = () => {
        const handleYesClick = () => {
            axios.post(`http://localhost:8080/account/user/remove/${tempUserToDeactivate}`)
                .then(response => {
                    if (response.data.message) {
                        history.push(`/employee-management?msg=${response.data.message}`);
                    }
                })
            setShouldDisplayWarningWindow(false);
            setTempUserToDeactivate('');
            window.location.reload();
        }
        const handleCancelClick = () => {
            setShouldDisplayWarningWindow(false);
            history.push(`/employee-management?`);
            setTempUserToDeactivate('');
        }
        return (
            <div className="message-window">
                <div>
                    If you deactivate user all data will not be accessible anymore. Refers to hours used in projects,
                    time report, etc. If you would like to keep this data, remove user from your team.
                </div>
                <div style={{display: "flex", flexDirection: "row", justifyContent: "space-evenly", flexWrap: "wrap"}}>
                    <button className="time-button"
                            onClick={handleYesClick}
                    >Yes
                    </button>
                    <button className="time-button"
                            onClick={handleCancelClick}
                    >Cancel
                    </button>
                </div>
            </div>
        )
    }

    const setWarningWindow = (user) => {
        setTempUserToDeactivate(user.identificator)
        setShouldDisplayWarningWindow(true);
    }

    return (
        <div className="e-m-container">
            {shouldDisplayInfoWindow ? <MessageWindow/> : <div/>}
            {shouldDisplayWarningWindow ? <WarningWindow /> : <div/>}
            <Paper>
                <TableContainer>
                    <Table stickyHeader aria-label="collapsible table">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell/>
                                <StyledTableCell>Identificator</StyledTableCell>
                                <StyledTableCell align="left">First Name</StyledTableCell>
                                <StyledTableCell align="left">LastName</StyledTableCell>
                                <StyledTableCell align="left">Email</StyledTableCell>
                                <StyledTableCell align="left">Registered</StyledTableCell>
                                <StyledTableCell align="left">Activated</StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {(Array.from(rows)).map((row) => (
                                <Row key={row.identificator} row={row} triggerUp={triggerUp}
                                     setWarningWindow={setWarningWindow}/>
                            ))}
                        </TableBody>
                    </Table>
                    <Link className="link-p-management" to="add-employee">
                        <div className="add-p-container">
                            <AddCardRoundedIcon/>
                            <div>Add employee to database</div>
                        </div>
                    </Link>
                </TableContainer>
            </Paper>
        </div>
    );
}

export default EmployeeManagement;
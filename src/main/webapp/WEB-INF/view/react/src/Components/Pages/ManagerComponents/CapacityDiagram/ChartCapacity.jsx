import React, {useEffect, useState} from "react";
import '../../../../index.css'
import {Line} from "react-chartjs-2";
import styled from "styled-components";
import {differenceInCalendarDays} from "date-fns";

const ChartCapacity = (props) => {

    const [trigger, setTrigger] = useState(0);

    useEffect(() => {
    }, [trigger])

    const chartColorsList = ['rgb(255, 99, 132)',
        'rgb(255, 159, 64)',
        'rgb(255, 205, 86)',
        'rgb(75, 192, 192)',
        'rgb(54, 162, 235)',
        'rgb(231,233,237)']

    let colorIterator = 0;
    let minValue = {
        week: 99,
        year: 9999,
    };
    let maxValue = {
        week: 0,
        year: 0,
    };
    const createDataset = (capacity, backgroundColor) => {
        colorIterator++;
        const label = capacity.employee.firstName + " " + capacity.employee.lastName + " " + capacity.employee.identificator;
        let data = [];
        Array.from(capacity.capacityParameters).forEach(c => {
            if (minValue.year > c.year) {
                minValue = {
                    week: c.week,
                    year: c.year
                }
            } else if (minValue.year === c.year) {
                if (minValue.week > c.week) {
                    minValue = {
                        week: c.week,
                        year: c.year
                    }
                }
            }
            if (maxValue.year < c.year) {
                maxValue = {
                    week: c.week,
                    year: c.year
                }
            } else if (maxValue.year === c.year) {
                if (maxValue.week < c.week) {
                    minValue = {
                        week: c.week,
                        year: c.year
                    }
                }
            }
            data.push({
                x: c.year + " week " + c.week,
                y: c.workedHoursInSum,
                projects: c.projects,
            })
        })

        return {
            label: label,
            xAxisID: 'xAxis1',
            fill: false,
            showLine: true,
            data: data,
            backgroundColor: backgroundColor,
            borderColor: backgroundColor,
        }
    }

    const weekLabels = [];
    if (colorIterator === chartColorsList.length) {
        colorIterator = 0;
    }
    let datasets = [];
    if (props.data.capacityOfEmployees) {
        datasets = Array.from(props.data.capacityOfEmployees).map(c => createDataset(c, chartColorsList[colorIterator]))
    }
    datasets.push({
        label: 'norm',
        xAxisID: 'xAxis1',
        fill: false,
        showLine: true,
        data: [{x: minValue.year + " week " + minValue.week, y: 40}, {
            x: maxValue.year + " week " + maxValue.week,
            y: 40
        }],
        backgroundColor: 'rgb(255,0,0)',
        borderColor: 'rgb(255,0,0)',
        pointRadius: 0,
        pointHoverRadius: 0,
        borderWidth: 0.6,
    })

    const afterLabel = (tooltipItem) => {
        let summary = '';
        tooltipItem.raw.projects.forEach(p => {
            const singleProject = p.name + " " + p.number + " Limit: " + p.maxHours + "\n";
            summary = summary + singleProject
        })
        return summary;
    }

    const pointRadius = 3;
    const amountOfWeeks = 12;
    const widthNumber = amountOfWeeks * 100;
    const widthString = widthNumber + "px";
    let heightNumber = props.data.capacityOfEmployees.length * 100;
    if (heightNumber < 400)
        heightNumber = 400;
    const heightString = heightNumber + "px";
    const ratio = amountOfWeeks / heightNumber;

    const cfg = {
        id: 'capacity',
        type: 'line',
        data: {
            labels: weekLabels,
            datasets: datasets,
            // datasets: [],
        },
        options: {
            plugins: {
                title: {
                    display: true,
                    text: 'Capacity 12 weeks ahead',
                    font: {
                        // family: 'Times',
                        size: 20,
                        // style: 'normal',
                        // lineHeight: 1.2
                    },
                },
                legend: {
                    position: 'bottom',
                },
                tooltip: {
                    callbacks: {
                        // footer: footer,
                        afterLabel: afterLabel,
                    }
                },
            },
            scales: {
                Axis1:
                    {
                        offset: false,
                        type: "time",
                        stacked: true,
                        distribution: 'series',
                        time: {
                            unit: 'week',
                            displayFormats: {
                                week: 'W'
                            }
                        },
                        grid: {
                            display: true,
                            drawBorder: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                        },
                        ticks: {
                            display: true,
                            autoSkip: false,
                        },

                    },
                xAxis3:
                    {
                        type: "time",
                        time: {
                            unit: 'year',
                            displayFormats: {
                                year: 'YYYY'
                            }
                        },
                        grid: {
                            display: false,
                            drawBorder: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                        },
                        ticks: {
                            display: true,
                            autoSkip: false,
                        },
                        offset: false,

                    },
                y: {
                    stacked: false,
                    offset: false,
                    beginAt0: true,
                    labels: [0, 40],
                    grid: {
                        display: true,
                        drawBorder: true,
                        drawOnChartArea: true,
                        drawTicks: true,
                    },
                    ticks: {
                        backdropColor: 'rgba(255, 255, 255, 0)',
                    },
                    title: {
                        display: true,
                        text: 'Hours worked',
                        padding: {top: 20, left: 0, right: 0, bottom: 10},
                        font: {
                            // family: 'Times',
                            size: 20,
                            // style: 'normal',
                            // lineHeight: 1.2
                        },
                    },

                }
            },
            elements: {
                point: {
                    radius: pointRadius,
                    hoverRadius: pointRadius * 2,
                },
            },
            responsive: true,
            maintainAspectRatio: false,
            aspectRatio: ratio,
        },
        animation: {
            onComplete: function () {
            }
        }
    }


    const ChartAreaWrapper = styled.div`
        height: ${heightString};
        width: ${widthString};
    `;

    const ChartWrapper = styled.div`
        position: relative;
        width: ${window.innerWidth + "px"};
        padding: 20px;
        overflow-x: scroll;
    `;

    return (
        <div>
            <ChartWrapper>
                <ChartAreaWrapper>
                    <Line type={cfg.type} options={cfg.options} data={cfg.data}/>
                </ChartAreaWrapper>
            </ChartWrapper>
        </div>
    )

}

export default ChartCapacity;
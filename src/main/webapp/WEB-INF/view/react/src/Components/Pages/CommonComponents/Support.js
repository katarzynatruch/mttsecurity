import './Support.css'
import imageQuestions from '../../../resources/questions.jpg'
import {useHistory} from "react-router-dom";

const Support = () => {

    let history = useHistory();

    return (
        <div className="support-container">
            <div className="question">
                Need some help?
            </div>
            <div className="row-flex">
                <input className="input" type="text" placeholder="What are you having trouble with?">
                </input>
                <button className="button" onClick={() => history.push("/support-successful")}>Send</button>
            </div>
        </div>
    )
}
export default Support;
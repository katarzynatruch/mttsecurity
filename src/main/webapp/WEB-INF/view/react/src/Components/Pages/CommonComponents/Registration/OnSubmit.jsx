import {Link} from "react-router-dom";
import "./Registration.css"

const OnSubmit = () => {

    const message = new URLSearchParams(window.location.search).get("msg");
    const proper = new URLSearchParams(window.location.search).get("proper");
    console.log(proper)
    return (
        <div className="reg-container">
            {proper === true ?
                <div>
                    <div className="good-message">{message}</div>
                    Check your email inbox for further instructions.
                </div>
                :
                <div>
                    <div className="bad-message">{message}</div>
                </div>
            }
            <Link to="/login">Go to log in page</Link>
        </div>
    )
}

export default OnSubmit;
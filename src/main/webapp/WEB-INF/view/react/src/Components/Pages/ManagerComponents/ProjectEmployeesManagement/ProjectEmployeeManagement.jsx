import axios from "axios";
import AuthenticationService from "Support/LoginService/AuthenticationService";
import React from "react";
import {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import './ProjectManagement.css'
import {IconButton, Tooltip} from "@mui/material";
import MultiselectForEmployeesInProjects from "./MultiselectForEmployeesInProjects";
import MultiselectForProjectsInEmployees from "./MultiselectForProjectsInEmployees";
import DeleteIcon from "@mui/icons-material/Delete";
import MaxHoursInput from "./MaxHoursInput";
import {useStateIfMounted} from "use-state-if-mounted";

const ProjectEmployeeManagement = () => {

    const [projectsWithEmployees, setProjectsWithEmployees] = useStateIfMounted([]);
    const [employeesWithProjects, setEmployeesWithProjects] = useStateIfMounted([]);
    const [maxHoursCombination, setMaxHoursCombination] = useStateIfMounted([]);
    const [projectActive, setProjectActive] = useStateIfMounted(true);

    const [trigger, setTrigger] = useState(0);
    let history = useHistory();


    useEffect(() => {
        loadProjectAndEmployeesData();
        // return cleanupSub();
    }, [trigger])

    const cleanupSub = () => {
        setProjectsWithEmployees([]);
        setEmployeesWithProjects([]);
        setMaxHoursCombination([]);
    }

    const loadProjectAndEmployeesData = () => {
        const loggedInUserIdentificator = AuthenticationService.getLoggedInUserName();
        axios.get(`http://localhost:8080/project-management/list/${loggedInUserIdentificator}`)
            .then(response => response.data)
            .then(data => {
                setProjectsWithEmployees(data.projectWithEmployeeListDs);
                setEmployeesWithProjects(data.employeeWithProjectListDs);
                setMaxHoursCombination(data.maxHoursDs);
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
    }
    const triggerUp = () => {
        setTrigger(trigger + 1);
    }

    function handleDeleteSelected(employee, project) {
        axios.post(`http://localhost:8080/project-management/deleteProjectFromList`, {
            projectNumber: project.number,
            employeeIdentificator: employee.identificator
        })
            .then(() => {
                    loadProjectAndEmployeesData();
                }
            )
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
    }

    const findMaxHours = (employee, project) => {
        const employees = maxHoursCombination.filter(mhc => mhc.employee.identificator === employee.identificator);
        const maxHours = employees.filter(mhc => mhc.project.number === project.number).map(mhc => mhc.maxHours);
        if (maxHours.length > 0) {
            return maxHours[0];
        } else {
            return 0;
        }
    }

    const sumUsedHours = (projectWithEmployees) => {
        let usedHoursInProject = 0
        const employeesAndHours = maxHoursCombination.filter(mhc => mhc.project.number === projectWithEmployees.project.number);
        employeesAndHours.forEach(e => usedHoursInProject = usedHoursInProject + findMaxHours(e.employee, projectWithEmployees.project))

        return usedHoursInProject;
    }

    const borderColorEmployee = projectActive ? '#85a2a6' : '#bccccf';
    const borderColorProject = !projectActive ? '#85a2a6' : '#bccccf';
    const frameColorEmployee = projectActive ? '#85a2a6' : '#bccccf';
    const frameColorProject = !projectActive ? '#85a2a6' : '#bccccf';
    const cardColorEmployee = projectActive ? 'rgba(210, 210, 210, 1)' : 'rgba(255, 255, 255, 1)';
    const cardColorProject = !projectActive ? 'rgba(210, 210, 210, 1)' : 'rgba(255, 255, 255, 1)';

    return (
        <div className='p-e-m-container'>
            <div className="tab-background" />
            <div className="module-container-project">
                <button className="module-tab"
                        disabled={projectActive}
                        onClick={() => {
                            setProjectActive(!projectActive)
                        }}
                >
                    By project
                </button>
                <div className="module-frame-project"
                     style={{borderColor: borderColorProject, backgroundColor: frameColorProject}}>
                    {projectsWithEmployees ? projectsWithEmployees.map(pwe =>
                            <div className="single-card-container" style={{backgroundColor: cardColorProject}}>
                                <h3>Project {pwe.project.name}</h3>
                                {sumUsedHours(pwe) > pwe.project.maxHours ?
                                    <div style={{color: 'red', marginBottom: 15}}>
                                        {sumUsedHours(pwe)} / {pwe.project.maxHours}
                                    </div>
                                    :
                                    <div style={{marginBottom: 15}}>
                                    {sumUsedHours(pwe)} / {pwe.project.maxHours}
                                    </div>
                                }
                                <div>
                                    {pwe.employees.map(e =>
                                        <div className="single-item-card">
                                            {e.firstName} {}
                                            {e.lastName} {}
                                            {e.identificator}

                                            <Tooltip title="Delete"
                                                     onClick={() => {
                                                         if (projectActive)
                                                             handleDeleteSelected(e, pwe.project)
                                                     }}
                                            >
                                                <IconButton aria-label="delete">
                                                    <DeleteIcon/>
                                                </IconButton>
                                            </Tooltip>
                                        </div>)}
                                </div>
                                <MultiselectForEmployeesInProjects projectWithEmployees={pwe}
                                                                   triggerUp={triggerUp}
                                                                   disabled={!projectActive}
                                />
                            </div>)
                        : <div/>}
                </div>
            </div>
            <div className="module-container-employee">
                <button className="module-tab"
                        disabled={!projectActive}
                        onClick={() => {
                            setProjectActive(!projectActive)
                        }}
                >
                    By employee
                </button>
                <div className="module-frame-employee"
                     style={{borderColor: borderColorEmployee, backgroundColor: frameColorEmployee}}>
                    {employeesWithProjects ? employeesWithProjects.map(ewp =>
                            <div className="single-card-container" style={{backgroundColor: cardColorEmployee}}>
                                <h3>{ewp.employee.firstName} {ewp.employee.lastName} {ewp.employee.identificator}</h3>
                                {ewp.projects.map(p =>
                                    <div className="single-item-card">
                                        {p.name}
                                        <MaxHoursInput employee={ewp.employee} project={p}
                                                       maxHours={() => findMaxHours(ewp.employee, p)}
                                                       triggerUp={triggerUp}
                                                       disabled={projectActive}
                                        />
                                        <Tooltip title="Delete"
                                                 onClick={() => {
                                                     if(!projectActive)
                                                     handleDeleteSelected(ewp.employee, p)
                                                 }}>
                                            <IconButton aria-label="delete">
                                                <DeleteIcon/>
                                            </IconButton>
                                        </Tooltip>
                                    </div>)}
                                <MultiselectForProjectsInEmployees employeesWithProjects={ewp}
                                                                   maxHoursCombination={maxHoursCombination}
                                                                   triggerUp={triggerUp}
                                                                   disabled={projectActive}
                                />
                            </div>)
                        : <div/>}
                </div>
            </div>
        </div>
    )

}

export default ProjectEmployeeManagement;
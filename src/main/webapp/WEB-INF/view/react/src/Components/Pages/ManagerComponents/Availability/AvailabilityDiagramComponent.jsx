import axios from "axios";
import AuthenticationService from "Support/LoginService/AuthenticationService";
import React, {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import SupportService from "../../../../Support/SupportService";
import ChartAvailability from "./ChartAvailability";
import {useStateIfMounted} from "use-state-if-mounted";
import TimeIntervalFilter from "../DiagramFilters/TimeIntervalFilter";
import "../Chart.css"


const AvailabilityDiagramComponent = () => {

    const [availabilityData, setAvailabilityData] = useStateIfMounted([]);
    const [processedAvailData, setProcessedAvailData] = useStateIfMounted([]);
    const [trigger, setTrigger] = useStateIfMounted(0);
    const [dateFrom, setDateFrom] = useStateIfMounted(0);
    const [dateTo, setDateTo] = useStateIfMounted(0);
    let history = useHistory();

    // @ts-ignore
    useEffect(() => {
        loadAvailabilityData();
        setProcessedAvailData(createAvailabilityData(availabilityData));

        // return cleanupSub();
    }, [trigger])

    const cleanupSub = () => {
        setAvailabilityData([]);
        setProcessedAvailData([]);
        setDateFrom(0);
        setDateTo(0);
    }

    const createAvailabilityData = (availabilityData) => {
        let processedData;
        if (availabilityData.availabilitySingleEmployeeDsList !== undefined) {
            processedData = Array.from(availabilityData.availabilitySingleEmployeeDsList)
                .map(availDts => createProcessedAvailabilityList(availDts))
            return processedData;
        } else {
            return [];
        }
    }

    const loadAvailabilityData = () => {
        const loggedInUserIdentificator = AuthenticationService.getLoggedInUserName();
        const managerId = SupportService.getManagerId(loggedInUserIdentificator);
        if (managerId == null) {
            console.log("ManagerId = null")
        } else {
            axios.get(`http://localhost:8080/availability/${loggedInUserIdentificator}`)
                .then(response => response.data)
                .then(data => {
                    setAvailabilityData(data)
                })
                .catch((error) => {
                    if (error.response.status === 401) {
                        history.push("/logout=true")
                    }
                })
        }
    }

    const AvailabilityList = (props) => {
        return (
            <div> {(Array.from(props.e[1])).map(a => (
                    <>
                        <div>
                            <h2>{a.event}</h2>
                            W{a.week} Day {a.day}.{a.month}.{a.year}</div>
                    </>
                )
            )}</div>
        )
    }


    const List = () => {
        if (processedAvailData !== undefined) {
            return (
                <div>{(Array.from(processedAvailData)).map((e) => (
                    <>
                        <div/>
                        <div
                            key={e[0].identificator}>{e[0].identificator} {e[0].firstName} {e[0].lastName} availability:
                        </div>
                        <AvailabilityList e={e}/>
                    </>
                ))}</div>
            )
        } else {
            return (
                <div/>
            )
        }
    }


    function createProcessedAvailabilityList(availDts) {
        return [createSingleEmployee(availDts.employee), createAvailability(availDts.availability)];
    }

    const createSingleEmployee = (employee) => {
        return {
            identificator: employee.identificator,
            email: employee.email,
            firstName: employee.firstName,
            lastName: employee.lastName,
        };
    }

    const createAvailability = (availabililty) => {
        return Array.from(availabililty).map(singleAvail =>
            createSingleAvailability(singleAvail)
        );
    }

    function createSingleAvailability(singleAvail) {
        const createSingleAvailability = (singleAvail) => {
            return {
                event: singleAvail.event,
                day: singleAvail.day,
                month: singleAvail.month,
                week: singleAvail.week,
                year: singleAvail.year
            };
        };
        return createSingleAvailability(singleAvail);
    }

    const handleTimeData = (weekFrom, weekTo, yearFrom, yearTo, dateFrom, dateTo) => {
        setDateFrom(dateFrom)
        setDateTo(dateTo)
    }

    const triggerUp = () => {
        setTrigger(trigger + 1)
    }

    return (
        <div className="availability-container">
            <h2>Availability diagram</h2>
            {processedAvailData ?
                <div>
                    <TimeIntervalFilter timeData={handleTimeData}/>
                </div>
                :
                <div/>
            }
            <div>
                <button className="generate-button"
                        onClick={() => {
                            setTrigger(trigger + 1);
                            console.log('trigger: ' + trigger)
                        }}
                >Generate diagram
                </button>
                {processedAvailData? console.log(true) : console.log(false)}
                {processedAvailData ?
                    <div>
                        <ChartAvailability data={processedAvailData} dateFrom={dateFrom} dateTo={dateTo} triggerUp={triggerUp}/>
                    </div>
                    :
                    <div/>
                }
            </div>
        </div>
    )
}


export default AvailabilityDiagramComponent

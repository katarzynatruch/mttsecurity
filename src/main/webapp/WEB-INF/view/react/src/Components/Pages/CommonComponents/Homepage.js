import React from "react";
import {Link, Route, useHistory} from "react-router-dom";
import './Homepage.css'
import projectClipart from '../../../resources/project-management.jpeg'
import teamClipart from '../../../resources/team-management.jpeg'
import analyzeClipart from '../../../resources/analyze.jpeg'
import timeReportClipart from '../../../resources/time-report.jpeg'
import availabilityReportClipart from '../../../resources/availabilitiy-report.jpeg'
import Footer from "../../Footer/Footer";
import {useStateIfMounted} from "use-state-if-mounted";
import axios from "axios";
import supportService from "../../../Support/SupportService";
import AuthenticationService from "../../../Support/LoginService/AuthenticationService";
import NotLoggedHomepage from "./Registration/NotLoggedHomepage";
import AuthenticatedRouteManager from "../../../Navigation/ProtectedRoutings/AuthenticatedRouteManager";
import AuthenticatedRouteLoggedInAll from "../../../Navigation/ProtectedRoutings/AuthenticatedRouteLoggedInAll";

const Homepage = () => {
    const API_URL = 'http://localhost:8080'
    const loggedInUserIdentificator = AuthenticationService.getLoggedInUserName();
    const [isAManager, setIsAManager] = useStateIfMounted(false);
    const [isAnAdmin, setIsAnAdmin] = useStateIfMounted(false);
    let history = useHistory();

    axios.post(`${API_URL}/user/get_roles`, {loggedInUserIdentificator})
        .then(response => response.data)
        .then(data => {
            setIsAManager(supportService.checkIfContainsManagerRole(data.userRoleNameList))
            setIsAnAdmin(supportService.checkIfContainsAdminRole(data.userRoleNameList))
        })

    const AdminAndManagerHomepage = () => {
        return (
            <div className="container">
                <div className="image-background">
                    <div className="column-flex">
                        <div className="home-text">
                            Manage your team and projects
                        </div>
                        <div className="home-text-light">
                            Create your teams, assign employees to projects and explore analytical tools for time spent
                            on
                            work
                        </div>
                        <div className="tools-container">
                            <Link className="link-t" to="/team-management">
                                <div className="tool-item">
                                    Manage your team
                                    <img src={teamClipart}/>
                                </div>
                            </Link>
                            <Link className="link-t" to="/project-management">
                                <div className="tool-item">
                                    Manage projects
                                    <img src={projectClipart}/>
                                </div>
                            </Link>
                            <Link className="link-t" to="/tools-menu">
                                <div className="tool-item">
                                    Analyze your team time consumption
                                    <img src={analyzeClipart}/>
                                </div>
                            </Link>

                        </div>
                    </div>
                    <div className="footer">
                        <Footer/>
                    </div>
                </div>
            </div>
        )
    }

    const EmployeeHomepage = () => {
        return (
            <div className="container">
                <div className="image-background">
                    <div className="column-flex">
                        <div className="home-text">
                            Report your time once a week
                        </div>
                        <div className="home-text-light">
                            Report your hours spent on work and availability
                        </div>
                        <div className="tools-container">
                            <div className="tool-item">
                                Report your time
                                <img src={timeReportClipart}/>
                            </div>
                            <div className="tool-item">
                                Report your availability
                                <img src={availabilityReportClipart}/>
                            </div>
                        </div>
                    </div>
                    <div className="footer">
                        <Footer/>
                    </div>
                </div>
            </div>
        )
    }

    return (
        AuthenticationService.isUserLoggedIn() ?
            isAManager ? (isAnAdmin ?
                    <AuthenticatedRouteManager><AdminAndManagerHomepage/></AuthenticatedRouteManager> :
                    <AuthenticatedRouteManager><AdminAndManagerHomepage/></AuthenticatedRouteManager>) :
                <AuthenticatedRouteLoggedInAll><EmployeeHomepage/></AuthenticatedRouteLoggedInAll>
            :
            <Route><NotLoggedHomepage/></Route>
    )

}
export default Homepage;
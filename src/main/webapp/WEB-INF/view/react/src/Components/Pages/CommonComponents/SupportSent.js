import './Support.css'
import {Link} from "react-router-dom";
const SupportSent = () => {

    return (
        <div className="support-container">
            <div className="question">
               Thank you! We will do our best to explain your issue.
            </div>
            <div className="row-flex">
                <Link to="/homepage">Back to homepage</Link>
            </div>
        </div>
    )
}
export default SupportSent;
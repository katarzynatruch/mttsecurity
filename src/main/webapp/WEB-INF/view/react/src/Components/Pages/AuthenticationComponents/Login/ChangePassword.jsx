import {red} from '@material-ui/core/colors';
import axios from 'axios';
import React, {useState} from 'react'
import {useHistory} from "react-router-dom";
import "./Login.css"
import Footer from "../../../Footer/Footer";


const API_URL = 'http://localhost:8080/account/update-old-password'

const ChangePassword = () => {

    const [oldPassword, setOldPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [matchingPassword, setMatchingPassword] = useState('');
    const [errMessage, setErrMessage] = useState('');
    let history = useHistory();

    function matchExpression(str) {
        const rgularExp = {
            containsSpecial: /^[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/,
            containsNumber: /\d+/,
            containsAlphabet: /[a-zA-Z]/,
        }

        const expMatch = {};
        expMatch.containsNumber = rgularExp.containsNumber.test(str);
        expMatch.containsAlphabet = rgularExp.containsAlphabet.test(str);
        expMatch.alphaNumeric = rgularExp.containsSpecial.test(str);

        return expMatch;
    }

    const passwordValidator = (newPassword) => {
        if (newPassword.length >= 5 && matchExpression(newPassword)) {
            return true;
        } else {
            return false;
        }
    }

    const passwordMatches = (newPassword, matchingPassword) => {
        if (newPassword === matchingPassword) {
            return true;
        } else {
            return false;
        }
    }

    const changePassword = async () => {
        if (passwordValidator(newPassword)) {
            if (passwordMatches(newPassword, matchingPassword)) {
                await axios.post(`${API_URL}`,
                    createJsonDataPassword())
                    .then(
                        response => {
                            console.log('response.data.message: ' + response.data.message);
                            if (response.data.proper) {
                                history.push(`/password-changed?msg=${response.data.message}`);

                            } else {
                                setErrMessage(response.data.message)
                            }
                        })
                    .catch((error) => {
                        if (error.response.status === 401) {
                            history.push("/logout=true")
                        }
                    })
            } else {
                setErrMessage('Passwords do not match')
            }
        } else {
            setErrMessage('New password do not meet requirements')
        }
    }


    const createJsonDataPassword = () => {
        const passwordChangedDts = {
            oldPassword: oldPassword,
            newPassword: newPassword,
            matchingPassword: matchingPassword,
        }
        return passwordChangedDts;
    }

    return (
        <div>
            <div className="login-container">
                <h1>Change Password</h1>
                <div className="login-box">
                    <div className="credentials-alert">{errMessage}</div>
                    <div className="login-single-item">
                        <div className="login-label">Old password</div>
                        <input className="login-input" type="password" name="oldPassword" value={oldPassword}
                               onChange={event => setOldPassword(event.target.value)}/>
                    </div>
                    <div className="login-single-item">
                        <div className="login-label">New password</div>
                        <input className="login-input" type="password" name="newPassword" value={newPassword}
                               onChange={event => setNewPassword(event.target.value)}/>
                    </div>
                    <div className="login-single-item">
                        <div className="login-label">Matching password</div>
                        <input className="login-input" type="password" name="matchingPassword" value={matchingPassword}
                               onChange={event => setMatchingPassword(event.target.value)}/>
                    </div>
                    <button className="login-button" onClick={changePassword}>Change password</button>
                </div>
            </div>
            <div className="login-image"/>
        </div>
    )

}

export default ChangePassword;
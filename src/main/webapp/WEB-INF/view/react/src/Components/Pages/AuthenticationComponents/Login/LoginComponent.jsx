import AuthenticationService from 'Support/LoginService/AuthenticationService';
import React, {Component} from 'react';
import {Link} from "react-router-dom";
import "./Login.css";
import Footer from "../../../Footer/Footer";
import clipLogin from "../../../../resources/loginClip.png"

class LoginComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            hasLoginFailed: false,
            showSuccessMessage: false
        }

        this.handleChange = this.handleChange.bind(this)
        this.loginClicked = this.loginClicked.bind(this)
    }

    handleChange(event) {
        this.setState(
            {
                [event.target.name]: event.target.value
            }
        )
    }

    loginClicked() {
        AuthenticationService
            .executeBasicAuthenticationService(this.state.username, this.state.password)
            .then(() => {
                AuthenticationService.registerSuccessfulLogin(this.state.username, this.state.password);
                this.props.history.push(`/homepage`);
                console.log('USER LOGGED IN acc to CLIENT: ' + AuthenticationService.getLoggedInUserName());
                console.log("User logged in: " + AuthenticationService.isUserLoggedIn());
            })
            .catch(() => {
            console.log("Login failed")
            this.setState({showSuccessMessage: false})
            this.setState({hasLoginFailed: true})
        })

        // AuthenticationService
        //     .executeJwtAuthenticationService(this.state.username, this.state.password)
        //     .then((response) => {
        //         AuthenticationService.registerSuccessfulLoginForJwt(this.state.username, response.data.token)
        //         this.props.history.push(`/courses`)
        //     }).catch(() => {
        //         this.setState({ showSuccessMessage: false })
        //         this.setState({ hasLoginFailed: true })
        //     })

    }

    render() {
        return (
            <div>
                <div className="login-container">
                    <h1>Log in</h1>
                    <h3>New here? <Link className="sign-up-link" to="/sign-up">Sign up</Link> as a manager!</h3>
                    <div className="login-box">
                        {this.state.hasLoginFailed && <div className="credentials-alert">Invalid Credentials</div>}
                        {this.state.showSuccessMessage && <div>Login Successful</div>}
                        <div className="login-single-item">
                            <div className="login-label">Identificator</div>
                            <input className="login-input" type="text" name="username" value={this.state.username}
                                   onChange={this.handleChange}/>
                        </div>
                        <div className="login-single-item">
                            <div className="login-label">Password</div>
                            <input className="login-input" type="password" name="password" value={this.state.password}
                                   onChange={this.handleChange}/>
                            <Link className="login-link" to="/forgot-password">Forgot password</Link>
                        </div>
                        <div className="login-single-item">
                        </div>
                        <button className="login-button" onClick={this.loginClicked}>Log in</button>
                    </div>
                </div>
               <div className="login-image" />
                <Footer/>
            </div>
        )
    }
}

export default LoginComponent
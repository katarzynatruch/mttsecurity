import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
} from "react-router-dom";

const ResetTokenExpired = () => {

    return (
        <div>
            <h2>Your link to reset password has expired. Click the link below to resend the link.</h2>
            <h3><Link to="/forgot-password">Reset password</Link></h3>
            
        </div>
    )
}

export default ResetTokenExpired;
import React, {useEffect, useState} from 'react';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Checkbox from '@mui/material/Checkbox';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import axios from 'axios';
import AuthenticationService from 'Support/LoginService/AuthenticationService';
import {useHistory} from 'react-router-dom';
import PropTypes from 'prop-types';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import ClearIcon from '@material-ui/icons/Clear';


function EnhancedTableHead(props) {

    const [open, setOpen] = React.useState(false);
    let history = useHistory();
    const [trigger, setTrigger] = useState(0);

    useEffect(() => {
    }, [trigger])

    const triggerUp = () => {
        setTrigger(trigger + 1)
    }

    const {classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort} = props;
    const createSortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };
    const numSelectedForTeam = props.employeesForTeam.length;

    const headCells = [
        {id: 'name', numeric: false, disablePadding: true, label: "Team " + props.headTeam},
        {id: 'identificator', numeric: true, disablePadding: false, label: 'Identificator'},
        {id: 'firstName', numeric: true, disablePadding: false, label: 'First name'},
        {id: 'lastName', numeric: true, disablePadding: false, label: 'Last name'},
    ];

    const handleDeleteTeamClick = (event) => {
        const employeeTeamUpdateDsList = [];
        props.teamsData.forEach(teamData => {
            const employees = teamData.employees.map(employee => employee.identificator)
            const singleEmployeeTeamUpdateDts = {
                team: teamData.team,
                employeeIdentificatorList: employees,
                managerIdentificator: AuthenticationService.getLoggedInUserName()
            }
            employeeTeamUpdateDsList.push(singleEmployeeTeamUpdateDts);
        })
        saveUpdatedTeam(employeeTeamUpdateDsList.filter(teamDts => teamDts.team.name !== props.headTeam));
        props.triggerUp();
        triggerUp();
    }

    const saveUpdatedTeam = (employeeTeamUpdateDsList) => {
        axios.post('http://localhost:8080/team/deleteTeams', {employeeTeamUpdateDsList})
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
    }

    return (
        // <TableHead>
        <TableRow>
            <TableCell>
                <IconButton aria-label="expand row" size="small" onClick={() => {
                    setOpen(!open);
                    props.openCollapse(!open, props.headTeam);
                }}>
                    {open ? <KeyboardArrowUpIcon/> : <KeyboardArrowDownIcon/>}
                </IconButton>
            </TableCell>
            <TableCell padding="checkbox">
                <Checkbox
                    indeterminate={numSelectedForTeam > 0 && numSelectedForTeam < rowCount}
                    checked={rowCount > 0 && numSelectedForTeam === rowCount}
                    onChange={onSelectAllClick}
                    inputProps={{'aria-label': props.headTeam}}
                    value={props.headTeam}
                />
            </TableCell>
            {headCells.map((headCell) => (
                <TableCell
                    key={headCell.id}
                    align={headCell.numeric ? 'right' : 'left'}
                    padding={headCell.disablePadding ? 'none' : 'default'}
                    sortDirection={orderBy === headCell.id ? order : false}
                >
                    <TableSortLabel
                        active={orderBy === headCell.id}
                        direction={orderBy === headCell.id ? order : 'asc'}
                        onClick={createSortHandler(headCell.id)}
                    >
                        {headCell.label}
                        {orderBy === headCell.id ? (
                            <span className={classes.visuallyHidden}>
                      {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                    </span>
                        ) : null}
                    </TableSortLabel>
                </TableCell>
            ))}
            <TableCell>
                <Tooltip title="Delete team" onClick={handleDeleteTeamClick}>
                    <IconButton aria-label="delete">
                        <ClearIcon/>
                    </IconButton>
                </Tooltip>
            </TableCell>
        </TableRow>
        // </TableHead>
    );
}

EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};
export default EnhancedTableHead
import React, {useEffect, useState} from "react";
import '../../../../index.css'
import Chart, {Line} from "react-chartjs-2";
import styled from "styled-components";
import {useStateIfMounted} from "use-state-if-mounted";
import ChartDataLabels from 'chartjs-plugin-datalabels';

import ReactDOM from 'react-dom';

const ComboDiagram = (props) => {
    const [mode, setMode] = useStateIfMounted('nearest');

    const chartColorsList = [
        'rgba(255, 99, 132, 1)',
        'rgba(255, 159, 64, 1)',
        'rgba(255, 205, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(231,233,237, 1)']

    let colorIterator = 0;

    const createDataset = (pae, backgroundColor) => {
        colorIterator++;
        if (colorIterator === chartColorsList.length) {
            colorIterator = 0;
        }
        const label = pae.employee.firstName + " " + pae.employee.lastName + " " + pae.employee.identificator;
        let data = [];
        Array.from(pae.projectsWithHours).forEach(p => {
            data = [];
            if (p.hoursUsed > 0) {
                data.push({
                    x: p.project.name + " " + p.project.number,
                    y: p.hoursUsed,
                    tooltipData: [p, pae],
                });
                datasets.push({
                    order: 1,
                    type: 'bar',
                    label: label,
                    xAxisID: 'xAxis1',
                    fill: false,
                    showLine: true,
                    data: data,
                    backgroundColor: backgroundColor,
                    borderColor: backgroundColor,
                    // borderColor: p.hoursLeft < 1 ? 'rgba(255, 0, 0, 1)' : backgroundColor,
                    // borderWidth: p.hoursLeft < 1 ? 1.5 : 0,
                    stack: 'stack' + colorIterator,
                    datalabels: {
                        // align: 'end',
                        // anchor: 'center',
                    }
                })
                data = [];
                data.push({
                    x: p.project.name + " " + p.project.number,
                    y: p.hoursUsed + p.hoursLeft,
                    tooltipData: [p, pae],
                });
                const color = backgroundColor.replace('1)', '0.3)');
                datasets.push({
                    order: 1,
                    type: 'bar',
                    label: label + "-to remove",
                    xAxisID: 'xAxis1',
                    fill: false,
                    showLine: true,
                    data: data,
                    backgroundColor: color,
                    borderColor: color,
                    stack: 'stack' + colorIterator,
                })
            }
        })
    }

    let amountOfProjectsDisplay = props.data.projectsAndTimeForEmployeeDs[0].projectsWithHours.length;
    let amountOfEmployeeDisplay = props.data.projectsAndTimeForEmployeeDs.length
    let widthNumber = amountOfProjectsDisplay * amountOfEmployeeDisplay * 60;
    const widthString = widthNumber + "px";
    let heightNumber = 600;
    const heightString = heightNumber + "px";
    const ratio = widthNumber / heightNumber;
    const xLabels = [];

    let datasets = [];
    colorIterator = 0;
    Array.from(props.data.projectsAndTimeForEmployeeDs).forEach(l => createDataset(l, chartColorsList[colorIterator]));
    colorIterator = 0;

    const labelTooltip = (tooltipItem) => {
        const hoursLeft = tooltipItem.raw.tooltipData[0].hoursLeft + '\n';
        const hoursUsed = tooltipItem.raw.tooltipData[0].hoursUsed + '\n';
        const limit = parseFloat(tooltipItem.raw.tooltipData[0].hoursLeft) + parseFloat(tooltipItem.raw.tooltipData[0].hoursUsed);
        let text = tooltipItem.raw.tooltipData[1].employee.firstName + " " +
            tooltipItem.raw.tooltipData[1].employee.lastName + " " +
            tooltipItem.raw.tooltipData[1].employee.identificator + "\n" +
            "\nHours left: " + hoursLeft +
            "\nHours used: " + hoursUsed +
            "\nHours limit: " + limit;
        return text;
    }

    let legend = [];

    function checkIfShouldBeInLegend(legend, item) {
        if (item.text.includes('to remove')) {
            return false;
        } else if (legend.filter(l => item.text === l).length > props.data.projectsAndTimeForEmployeeDs[0].projectsWithHours.length + 1) {
            return false
        } else {
            legend.push(item.text)
            return true;
        }
    }

    const cfg = {
        id: 'statisticDiagram',
        type: 'line',
        data: {
            labels: xLabels,
            datasets: datasets,
        },
        options: {
            plugins: {
                title: {
                    display: true,
                    text: 'Statistic diagram',
                    font: {
                        // family: 'Times',
                        size: 20,
                        // style: 'normal',
                        // lineHeight: 1.2
                    },
                },
                legend: {
                    display: true,
                    position: 'bottom',
                    labels: {
                        filter: function (item, chart) {
                            return checkIfShouldBeInLegend(legend, item)
                        }
                    },
                },
                tooltip: {
                    callbacks: {
                        label: labelTooltip,
                    },
                },
                beforeDraw: chart => {
                },
                datalabels: {
                    display: function (context) {
                        if (!context.dataset.label.includes('to remove')) {
                            if (context.dataset.data[0].y > 15) {
                                return true
                            }
                        }
                        // return !context.dataset.label.includes('to remove') || context.dataset.data[0].y > 15
                    },
                    formatter: function (value, context) {
                        // if ((value.tooltipData[0].hoursUsed + value.tooltipData[0].hoursLeft) !== 0) {
                        if ((value.tooltipData[0].hoursUsed + value.tooltipData[0].hoursLeft) > 0) {
                            if(context.dataset.data[0].y > 15) {
                                if(!context.dataset.label.includes('to remove')){
                                    const percentage = Math.round(value.tooltipData[0].hoursUsed * 100 / (value.tooltipData[0].hoursUsed + value.tooltipData[0].hoursLeft))
                                    console.log(percentage)
                                    return percentage + "%"
                                }else{
                                    return ''
                                }
                            }else{
                                return ''
                            }
                        } else if (context.dataset.data[0].y > 15){
                            if(!context.dataset.label.includes('to remove')){
                                return 'Limit\nreached'
                            }
                        }else{
                            return ''
                        }
                        // return context.dataIndex + ': ' + Math.round(value*100) + '%';
                    },
                    font: {
                        weight: 'bold'
                    },
                }
            },
            onHover: function (e) {

            },
            interaction: {
                intersect: false,
                mode: mode,
            },
            scales: {
                Axis1:
                    {
                        offset: false,
                        type: "category",
                        stacked: false,
                        distribution: 'series',
                        grid: {
                            display: true,
                            drawBorder: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                        },
                        ticks: {
                            display: false,
                            autoSkip: false,
                            beginAtZero: true,
                        },

                    },
                y: {
                    stacked: false,
                    offset: false,
                    beginAt0: true,
                    grid: {
                        display: true,
                        drawBorder: true,
                        drawOnChartArea: true,
                        drawTicks: true,
                    },
                    ticks: {
                        backdropColor: 'rgba(255, 255, 255, 0)',
                        callback: function (value, index, ticks) {
                            return value;
                        },
                        padding: 5,
                        autoSkip: false,
                        showLabelBackdrop: true,
                        major: {
                            enabled: true,
                        },
                    },
                    title: {
                        display: true,
                        text: 'Hours',
                        padding: {top: 20, left: 0, right: 0, bottom: 10},
                        font: {
                            // family: 'Times',
                            size: 20,
                            // style: 'normal',
                            // lineHeight: 1.2
                        },
                    },

                },
            },
            elements: {
                bar: {
                    borderWidth: 0,
                    borderRadius: 5,
                    inflateAmount: 3,
                },
            },
            responsive: true,
            maintainAspectRatio: false,
            aspectRatio: ratio,
        },
        animation: {
            onProgress: function (animation) {
            },
            onComplete: function (animation) {
            }
        },

    }

    const ChartAreaWrapper = styled.div`
        height: ${heightString};
        width: ${widthString};
    `;

    const ChartWrapper = styled.div`
        position: relative;
        width: ${window.innerWidth + "px"};
        padding: 20px;
        overflow-x: scroll;
    `;


    return (
        <div>
            <div className="option-container">
            {mode === 'nearest' ?
                <button className="option-button" onClick={() => setMode('null')}>Turn off tooltips</button>
                :
                <button className="option-button" onClick={() => setMode('nearest')}>Turn on tooltips</button>
            }
            </div>
            <ChartWrapper>
                <ChartAreaWrapper>
                    <Line type={cfg.type} plugins={[ChartDataLabels]} options={cfg.options} data={cfg.data}/>
                    {/*<canvas id="chartHours" width={400} height={400}/>*/}
                </ChartAreaWrapper>
            </ChartWrapper>
        </div>
    )


}
export default ComboDiagram;
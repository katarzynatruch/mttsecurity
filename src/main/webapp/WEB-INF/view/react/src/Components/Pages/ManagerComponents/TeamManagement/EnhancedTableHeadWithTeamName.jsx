import React, { useEffect, useState } from 'react';
import EnhancedTableHead from './EnhancedTableHead';

const withTeamName = (Component) => {
  return class WithTeamName extends React.Component {
    render() {
      const headTeam = this.props.headTeam;
      return (
        <Component headTeam={headTeam} {...this.props} triggerUp={this.props.triggerUp} 
        employeesForTeam={this.props.employeesForTeam} openCollapse={this.props.openCollapse} 
        teamsData={this.props.teamsData}/>
      );
    }
  }
}
const EnhancedTableHeadWithTeamName = withTeamName(EnhancedTableHead)

export default EnhancedTableHeadWithTeamName
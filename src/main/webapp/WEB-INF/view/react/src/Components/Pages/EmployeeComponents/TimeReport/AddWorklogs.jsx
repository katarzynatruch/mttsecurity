import {FormControl, IconButton, ListItemText, MenuItem, MenuList, Select, Tooltip} from "@mui/material";
import React, {useEffect, useState} from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import Worklog from "./Worklog";
import AuthenticationService from "../../../../Support/LoginService/AuthenticationService";
import axios from "axios";
import {useHistory} from "react-router-dom";
import "./TimeReport.css"
import {useStateIfMounted} from "use-state-if-mounted";


const AddWorklogs = (props) => {
    const [selectedProjects, setSelectedProjects] = useStateIfMounted([]);
    const [worklogs, setWorklogs] = useStateIfMounted([]);
    const [trigger, setTrigger] = useStateIfMounted(0);
    const [displayConfirmedWindow, setDisplayConfirmedWindow] = useStateIfMounted(false);
    const [confirmDisabled, setConfirmDisabled] = useStateIfMounted(true);
    const history = useHistory();


    useEffect(() => {
        loadWorklogs();
        // return cleanupSub();
    }, [trigger])

    const cleanupSub = () => {
        setSelectedProjects([])
        setWorklogs([])
    }

    const triggerUp = () => {
        setTrigger(trigger + 1)
    }

    const ConfirmationSent = () => {
        return (
            <div className="time-popup-confirm-window">
                <h2>Report confirmed</h2>
                <div>Your time report has been sent to your managers for approval</div>
                <button className="time-button" onClick={() => setDisplayConfirmedWindow(false)}>Ok</button>
            </div>
        )
    }


    function loadWorklogs() {
        if (props.worklogs) {
            const loadedWorklogs = Array.from(props.worklogs).map(worklog => {
                return {
                    workedHours: worklog.workedHours,
                    leftHours: worklog.leftHours,
                    week: worklog.week,
                    year: worklog.year,
                    project: worklog.project,
                };
            })

            const tempSelected = [];
            loadedWorklogs.forEach(w => {
                let i = 1;
                for (i; i <= tempSelected.filter(lw => lw.project.number === w.project.number).length; i++) {
                }
                tempSelected.push({
                    project: w.project,
                    iterator: i,
                    workedHours: w.workedHours,
                })
            })
            const mappedWorklogs = [];
            loadedWorklogs.forEach(lw => {
                let i = 1;
                for (i; i <= mappedWorklogs.filter(mw => mw.project.number === lw.project.number).length; i++) {
                }
                mappedWorklogs.push({
                    workedHours: lw.workedHours,
                    leftHours: lw.leftHours,
                    week: lw.week,
                    year: lw.year,
                    project: lw.project,
                    iterator: i,
                })
            })
            setWorklogs(mappedWorklogs);
            setSelectedProjects(tempSelected);
        }
    }

    const handleDeleteWorklog = (selectedProject) => {
        const tempSelProj = selectedProjects.filter(sp => sp.project.number + sp.iterator !== selectedProject.project.number + selectedProject.iterator);
        setSelectedProjects(tempSelProj);
        const tempWorklogs = worklogs.filter(w => w.project.number + w.iterator !== selectedProject.project.number + selectedProject.iterator)
        setWorklogs(tempWorklogs);
        setConfirmDisabled(true)
    }

    function handleProjectSelected(event) {
        let i = 0;
        for (i; i <= selectedProjects.filter(sp => sp.project.number === event.target.value.number).length; i++) {
        }
        setSelectedProjects([...selectedProjects, {project: event.target.value, iterator: i, workedHours: 0}]);
        props.triggerUp();
        setConfirmDisabled(true)
    }

    function handleWorklog(worklog) {
        if (worklog.project) {
            if (worklogs.filter(w => w.project.number + w.iterator === worklog.project.number + worklog.iterator).length > 0) {
                const tempWorklogs = worklogs.filter(w => w.project.number + w.iterator !== worklog.project.number + worklog.iterator)
                tempWorklogs.push(worklog);
                setWorklogs(tempWorklogs);
            } else
                setWorklogs([...worklogs, worklog]);
        } else {
        }
        setConfirmDisabled(true)
    }

    function saveWorklogs() {
        const workLogWeeks = [];
        workLogWeeks.push({
            week: props.week,
            year: props.year,
            workLogs: worklogs
        })

        axios.post('http://localhost:8080/employee/timeReport/save', {
            employeeIdentificator: AuthenticationService.getLoggedInUserName(),
            workLogWeeks: workLogWeeks,
        })
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
        setConfirmDisabled(false)
    }

    const sumAllHours = () => {
        const hours = worklogs.map(w => w.workedHours);
        let sum = 0;
        hours.forEach(h => sum = sum + parseInt(h));
        return sum;
    }
    const sendConfirmation = () => {
        const workLogDs = [];
        workLogDs.push({
            week: props.week,
            year: props.year,
            workLogs: worklogs
        })
        axios.post('http://localhost:8080/employee/timeReport/approved-by-emp', {
            employeeIdentificator: AuthenticationService.getLoggedInUserName(),
            workLogDs: workLogDs,
        })
            .then(
                setDisplayConfirmedWindow(true)
            )
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
        setConfirmDisabled(true)
    }

    const selectStyle = {
        minWidth: 200,
        height: 50,
        p: 0,
        m: 1,
    }
    const menuItemStyle = {
        p: 0,
        m: 0,
    }

    return (
        <div>
            {displayConfirmedWindow ? <div><ConfirmationSent/></div> : <div/>}
            <div className="add-worklog-top-info">
                Time reported this week:
                {sumAllHours() > 39 ?
                    <div style={{color: "green"}}>
                        {sumAllHours()}
                    </div>
                    :
                    sumAllHours() < 1 ?
                        <div style={{color: "red"}}>
                            {sumAllHours()}
                        </div>
                        :
                        <div>
                            {sumAllHours()}
                        </div>
                }
            </div>

            {
                selectedProjects ? selectedProjects.map(selectedProject => (
                    <div className="worklog-row-flex"
                         key={selectedProject.project.number + selectedProject.iterator}>
                        <Worklog project={selectedProject.project} week={props.week} year={props.year}
                                 workedHours={selectedProject.workedHours}
                                 projectIterator={selectedProject.iterator}
                                 sendDataToParent={handleWorklog} triggerUp={triggerUp}/>
                        <Tooltip title="Delete"
                                 onClick={() => handleDeleteWorklog(selectedProject)}>
                            <IconButton aria-label="delete">
                                <DeleteIcon/>
                            </IconButton>
                        </Tooltip>
                    </div>

                )) : <div/>}
            <div>
                <Select
                    sx={{...selectStyle}}
                    variant="outlined"
                    labelId="worklog"
                    id="worklog"
                    label="Select project"
                    onChange={handleProjectSelected}
                    defaultValue={''}
                >
                    {props.projectWithHours.length > 0 ?
                        Array.from(props.projectWithHours).map(pwh => (
                            <MenuItem
                                sx={{...menuItemStyle}}
                                value={pwh.project}>{pwh.project.number + " " + pwh.project.name}</MenuItem>
                        ))
                        :
                            <MenuItem sx={{...menuItemStyle}} disabled={true}>
                                <ListItemText>No projects assigned to you</ListItemText>
                            </MenuItem>
                    }
                </Select>
            </div>
            <button className="time-button" onClick={saveWorklogs}>Save time report</button>
            <button className="time-button" onClick={sendConfirmation} disabled={confirmDisabled}>Send confirmation
            </button>
        </div>
    )
}

export default AddWorklogs;
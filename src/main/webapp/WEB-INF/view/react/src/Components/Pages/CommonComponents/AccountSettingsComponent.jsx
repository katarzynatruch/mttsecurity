import React from "react";
import {Link} from "react-router-dom";
import LockOpenIcon from '@material-ui/icons/LockOpen';
import "./AccountSettings.css"

const AccountSettingsComponent = () => {

    return (
        <div className="settings-container">
            <h1>Account settings</h1>
            <div className="settings-box">
                <Link className="link" to="/change-password">
                    <div className="settings-item">
                        <LockOpenIcon/>
                        <div>
                            Change the password
                        </div>
                    </div>
                </Link>
            </div>
        </div>
    )
}

export default AccountSettingsComponent;
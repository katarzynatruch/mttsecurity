import axios from "axios";
import AuthenticationService from "Support/LoginService/AuthenticationService";
import React, {useEffect, useState} from "react";
import MultiselectForProjects from "./MultiselectForProjects";
import MultiselectForTeam from "./MultiselectForTeam";
import TimeIntervalFilter from "./TimeIntervalFilter";
import '../../../../index.css'
import {useHistory} from "react-router-dom";
import SupportService from "../../../../Support/SupportService";
import {useStateIfMounted} from "use-state-if-mounted";
import "./Filters.css"

const DiagramsFilterComponent = (props) => {

    const [filterData, setFilterData] = useStateIfMounted([]);
    const [processedCapacityDataForTeam, setProcessedCapacityDataForTeam] = useStateIfMounted([]);
    const [processedCapacityDataForProjects, setProcessedCapacityDataForProjects] = useStateIfMounted([]);
    const [trigger, setTrigger] = useStateIfMounted(0);
    const [selectedEmployees, setSelectedEmployees] = useStateIfMounted([]);
    const [selectedProjects, setSelectedProjects] = useStateIfMounted([]);
    const [timeInterval, setTimeInterval] = useStateIfMounted({})
    const [shouldBeDisplayed, setShouldBeDisplayed] = useStateIfMounted(props.shouldBeDisplayed)
    let history = useHistory();


    useEffect(() => {
        loadFilterData()
        if (filterData.length < 1) {
            triggerUp()
        }
        setProcessedCapacityDataForTeam(createCapacityDataByTeams(filterData))
        setProcessedCapacityDataForProjects(createCapacityDataByProjects(filterData))
        sendDataToParent();
        // return cleanupSub();
    }, [trigger])

    const cleanupSub = () => {
        setProcessedCapacityDataForTeam([]);
        setProcessedCapacityDataForProjects([]);
        setSelectedEmployees([]);
        setSelectedProjects([]);
        setTimeInterval({});
    }

    const createCapacityDataByTeams = (filterData) => {
        if (filterData === undefined || filterData.length < 1) {
            return [];
        }
        let teams = Array.from(filterData.employeesInTeams).map(team => createTeamData(team));
        return teams;
    }
    const createCapacityDataByProjects = (filterData) => {
        if (filterData === undefined || filterData.length < 1) {
            return [];
        }
        let projects = filterData.regularProjects;
        filterData.commonProjects.map(cmProject => projects.push(cmProject));
        return projects.map(project => createProjectData(project));
    }
    const createProjectData = (project) => {
        return {
            name: project.name,
            number: project.number,
            type: project.type,
            optionName: project.number + project.name,
            // key: project.number + project.name, value: project.number + project.name, flag: 'af', text: project.name
        }
    }
    const createTeamData = (teamData) => {
        let employees = Array.from(teamData.employees).map(employee => createSingleEmployee(employee))
        const team = teamData.team.name;
        return {team, employees}
    }

    const createSingleEmployee = (employee) => {
        const singleEmployee = {
            identificator: employee.identificator,
            firstName: employee.firstName,
            lastName: employee.lastName,
            name: employee.firstName + " " + employee.lastName + " " + employee.identificator
        }
        return singleEmployee;
    }

    const loadFilterData = () => {
        const loggedInUserIdentificator = AuthenticationService.getLoggedInUserName();
        const managerId = SupportService.getManagerId(loggedInUserIdentificator);

        if (managerId == null) {
            console.log("ManagerId = null")
        } else {
            axios.get(`http://localhost:8080/statistics/filters/${loggedInUserIdentificator}`)
                .then(response => response.data)
                .then(data => {
                    setFilterData(data)
                })
                .catch((error) => {
                    if (error.response.status === 401) {
                        history.push("/logout=true")
                    }
                })
        }
    }

    const handleEmployeeData = (employeeIdentificators) => {
        setSelectedEmployees(employeeIdentificators);
        props.triggerUp();
        triggerUp();
    }

    const handleProjectData = (projectNumbers) => {
        setSelectedProjects(projectNumbers)
        props.triggerUp();
        triggerUp();
    }

    const handleTimeData = (weekFromA, weekToA, yearFromA, yearToA, dateFrom, dateTo) => {
        setTimeInterval({
            weekFrom: weekFromA,
            weekTo: weekToA,
            yearFrom: yearFromA,
            yearTo: yearToA,
            dateFrom: dateFrom,
            dateTo: dateTo,
        })
        props.triggerUp();
        triggerUp();

    }

    const sendDataToParent = () => {
        props.filterData(selectedEmployees, selectedProjects, timeInterval)
        props.triggerUp();
    }

    const triggerUp = () => {
        setTrigger(trigger + 1);
        // props.triggerUp();
    }


    return (
        <div>
            <div className="multiselects-container">
                <div>
                    <div>
                    </div>
                    <div>
                        <div style={{paddingLeft: 20, fontWeight: "bold"}}>
                            Select employees:
                        </div>
                        {processedCapacityDataForTeam ? <MultiselectForTeam team={processedCapacityDataForTeam}
                                                                            employeesData={handleEmployeeData}
                                                                            triggerUp={triggerUp}
                            />
                            : <div/>}

                    </div>
                </div>
                <div>
                    <div>
                        <div style={{paddingLeft: 20, fontWeight: "bold"}}>
                            Select projects:
                        </div>
                        {processedCapacityDataForProjects ?
                            <MultiselectForProjects projects={processedCapacityDataForProjects}
                                                    projectsData={handleProjectData}
                                                    triggerUp={triggerUp}
                            />
                            : <div/>}
                    </div>
                </div>
            </div>
            {shouldBeDisplayed ? <TimeIntervalFilter timeData={handleTimeData}/> : <div/>}
        </div>
    )

}
export default DiagramsFilterComponent;
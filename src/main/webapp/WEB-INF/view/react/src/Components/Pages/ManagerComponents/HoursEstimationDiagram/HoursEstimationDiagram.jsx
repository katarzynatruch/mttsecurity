import React, {useEffect, useState} from "react";
import '../../../../index.css'
import Chart, {Line} from "react-chartjs-2";
import styled from "styled-components";
import {differenceInCalendarDays, differenceInCalendarWeeks, differenceInWeeks, getWeek, getYear} from "date-fns";
import {useStateIfMounted} from "use-state-if-mounted";
import ChartDataLabels from 'chartjs-plugin-datalabels';


import ReactDOM from 'react-dom';

const HoursEstimationDiagram = (props) => {
    const [trigger, setTrigger] = useState(0);
    const [chartType, setChartType] = useStateIfMounted('bar');
    const [limitDataHidden, setLimitDataHidden] = useStateIfMounted(false);
    const [mode, setMode] = useStateIfMounted('nearest');

    useEffect(() => {
    }, [trigger])

    let ctx;
    const chartColorsList = [
        'rgba(255, 99, 132, 1)',
        'rgba(255, 159, 64, 1)',
        'rgba(255, 205, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(231,233,237, 1)']

    let colorIterator = 0;
    let bubbleDataset = [];
    let datasets = [];
    const pointRadius = 3;
    const amountOfWeeks = Math.abs(differenceInCalendarWeeks(props.timeInterval.dateFrom, props.timeInterval.dateTo));
    let widthNumber = amountOfWeeks * 100;
    if (widthNumber < 600) {
        widthNumber = 600;
    }
    const widthString = widthNumber + "px";
    let maxValue = 500;
    Array.from(props.data.listOfHoursConsumptionPerWeekDs).forEach(l => {
        if (l.project.maxHours > maxValue) {
            maxValue = l.project.maxHours;
        }
    });
    // let heightNumber = maxValue * 0.6;
    let heightNumber = 600;
    const heightString = heightNumber + "px";
    const ratio = amountOfWeeks / heightNumber;
    const weekLabels = [];
    if (colorIterator === chartColorsList.length) {
        colorIterator = 0;
    }

    function chooseProperPointStyle(milestone) {
        if (milestone === 'AREL' || milestone === 'BREL' || milestone === 'CREL' || milestone === 'PREL') {
            return 'rectRounded'
        } else if (milestone === 'GA' || milestone === 'GB') {
            return 'circle'
        } else if (milestone === 'CP1' || milestone === 'CP2') {
            return 'triangle'
        }
    }

    const createDataset = (hoursDs, backgroundColor) => {
        colorIterator++;
        const label = hoursDs.project.name + " " + hoursDs.project.number;
        let yValue = limitDataHidden ? 200 : hoursDs.project.maxHours;
        Array.from(hoursDs.project.deadlines).forEach(d => {
            const bubbleData = [];
            if (d.year > props.timeInterval.yearFrom && d.year < props.timeInterval.yearTo) {
                bubbleData.push({
                    x: d.year + " week " + d.week,
                    y: yValue,
                    r: 25,
                    tooltipData: [hoursDs, d],
                });
            } else if (d.year === props.timeInterval.yearFrom) {
                if (d.week >= props.timeInterval.weekFrom) {
                    if (props.timeInterval.yearFrom === props.timeInterval.yearTo) {
                        if (d.week <= props.timeInterval.weekTo) {
                            bubbleData.push({
                                x: d.year + " week " + d.week,
                                y: yValue,
                                r: 25,
                                tooltipData: [hoursDs, d],
                            });
                        }
                    } else {
                        if (d.week >= props.timeInterval.weekFrom) {
                            bubbleData.push({
                                x: d.year + " week " + d.week,
                                y: yValue,
                                r: 25,
                                tooltipData: [hoursDs, d],
                            });
                        }
                    }
                }
            } else if (d.year === props.timeInterval.yearTo) {
                if (d.week <= props.timeInterval.weekTo) {
                    bubbleData.push({
                        x: d.year + " week " + d.week,
                        y: yValue,
                        r: 25,
                        tooltipData: [hoursDs, d],
                    });
                }
            }
            bubbleDataset.push({
                order: 1,
                type: 'bubble',
                label: d.milestone,
                xAxisID: 'xAxis1',
                pointStyle: chooseProperPointStyle(d.milestone),
                data: bubbleData,
                // backgroundColor: backgroundColor,
                // borderColor: backgroundColor,
                datalabels: {}
            })
        })

        let data = [];
        Array.from(hoursDs.hoursConsumptionForEmployees).forEach(h => {
            data.push({
                x: h.year + " week " + h.week,
                y: h.usedHours,
                employees: h.employees,
            });
        })
        return {
            order: 3,
            type: chartType,
            label: label,
            xAxisID: 'xAxis1',
            fill: false,
            showLine: true,
            data: data,
            backgroundColor: backgroundColor,
            borderColor: backgroundColor,
        }
    }


    const createLimitData = (hoursDs) => {
        const data = [];
        Array.from(hoursDs.hoursConsumptionForEmployees).forEach(h =>
            data.push({
                x: h.year + " week " + h.week,
                y: hoursDs.project.maxHours,
                employees: [hoursDs.project.maxHours]
            }))
        return data;
        return [{
            x: getYear(props.timeInterval.dateFrom) + " week " + (getWeek(props.timeInterval.dateFrom)),
            y: hoursDs.project.maxHours,
            employees: [hoursDs.project.maxHours]
        },
            {
                x: getYear(props.timeInterval.dateTo) + " week " + (getWeek(props.timeInterval.dateTo) - 1),
                y: hoursDs.project.maxHours,
                employees: [hoursDs.project.maxHours],
            }]
    }

    if (props.data.listOfHoursConsumptionPerWeekDs) {
        datasets = Array.from(props.data.listOfHoursConsumptionPerWeekDs).map(l => createDataset(l, chartColorsList[colorIterator]));
        colorIterator = 0;
        Array.from(props.data.listOfHoursConsumptionPerWeekDs).map(l => {
            const label = l.project.name + " " + l.project.number;
            const color = chartColorsList[colorIterator].replace('1)', '0.3)');
            datasets.push({
                order: 2,
                hidden: limitDataHidden,
                type: 'line',
                offset: false,
                label: label + "-to remove",
                xAxisID: 'xAxis1',
                fill: false,
                showLine: true,
                data: createLimitData(l),
                lineWidth: 0,
                backgroundColor: color,
                borderColor: color,
                borderWidth: pointRadius * 2,
                pointBorderWidth: 0,
                pointRadius: 0,
                pointHoverRadius: pointRadius,
            })
            colorIterator++;
            if (colorIterator === chartColorsList.length) {
                colorIterator = 0;
            }
        })
        bubbleDataset.forEach(b => datasets.push(b))
    }

    const afterLabelTooltip = (tooltipItem) => {
        if (tooltipItem.raw.employees) {
            let summary = '';
            tooltipItem.raw.employees.forEach(p => {
                let singleEmployee = ''
                if (p.identificator) {
                    singleEmployee = p.firstName + " " + p.lastName + " " + p.identificator + "\n";
                }
                summary = summary + singleEmployee
            })
            return summary;
        } else if (tooltipItem.raw.tooltipData) {
            return tooltipItem.raw.tooltipData[0].project.name + " " + tooltipItem.raw.tooltipData[0].project.number;
        }
    }

    const labelTooltip = (tooltipItem) => {
        let text = 'Hours limit: '
        if (!tooltipItem.dataset.label.includes('to remove')) {
            return tooltipItem.dataset.label;
        } else {
            return text + tooltipItem.raw.y + " h";
        }
    }

    const titleTooltip = (tooltipItems) => {
        let text = '';
        tooltipItems.forEach(t => {
            if (t.dataset.label.includes('to remove')) {
                const tempText = t.dataset.label.split('-')
                text = tempText[0];
            } else {
                text = t.label;
            }
        })
        return text;
    }
    const cfg = {
        id: 'hoursEstimation',
        type: 'line',
        data: {
            labels: weekLabels,
            datasets: datasets,
        },
        options: {
            plugins: {
                title: {
                    display: true,
                    text: 'Hours consumption',
                    font: {
                        // family: 'Times',
                        size: 20,
                        // style: 'normal',
                        // lineHeight: 1.2
                    },
                },
                legend: {
                    display: true,
                    position: 'bottom',
                    labels: {
                        filter: function (item, chart) {
                            return !item.text.includes('to remove')
                                && !item.text.includes('AREL')
                                && !item.text.includes('BREL')
                                && !item.text.includes('CREL')
                                && !item.text.includes('PREL')
                                && !item.text.includes('GA')
                                && !item.text.includes('GB')
                                && !item.text.includes('CP1')
                                && !item.text.includes('CP2')
                        }
                    },
                },
                tooltip: {
                    callbacks: {
                        // footer: footer,
                        title: titleTooltip,
                        label: labelTooltip,
                        afterLabel: afterLabelTooltip,
                    },
                },
                beforeDraw: chart => {
                },
                datalabels: {
                    display: function (context) {
                        return context.dataset.type === 'bubble';
                    },
                    formatter: function (value, context) {
                        if (context.dataset.type === 'bubble') {
                            return value.tooltipData[1].milestone;
                        }
                    },
                    font: {
                        weight: 'bold'
                    },
                }
            },
            onHover: function (e) {

            },
            interaction: {
                intersect: false,
                mode: mode,
            },
            scales: {
                Axis1:
                    {
                        offset: false,
                        type: "time",
                        stacked: false,
                        distribution: 'series',
                        time: {
                            unit: 'week',
                            displayFormats: {
                                week: 'W'
                            }
                        },
                        grid: {
                            display: true,
                            drawBorder: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                        },
                        ticks: {
                            display: true,
                            autoSkip: false,
                            beginAtZero: true,
                        },

                    },
                xAxis3:
                    {
                        type: "time",
                        time: {
                            unit: 'year',
                            displayFormats: {
                                year: 'YYYY'
                            }
                        },
                        grid: {
                            display: false,
                            drawBorder: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                        },
                        ticks: {
                            display: true,
                            autoSkip: false,
                        },
                        offset: false,

                    },
                y: {
                    stacked: false,
                    offset: false,
                    beginAt0: true,
                    labels: [],
                    grid: {
                        display: true,
                        drawBorder: true,
                        drawOnChartArea: true,
                        drawTicks: true,
                    },
                    ticks: {
                        backdropColor: 'rgba(255, 255, 255, 0)',
                        callback: function (value, index, ticks) {
                            ctx = this.chart.ctx;
                            const specialTicks = ticks.filter(t => t.$context.tick.label.toString().includes(' '));
                            const checkIfIsNear = (st, value) => {
                                if ((value + 150) > parseInt(st.value) && (value - 150) < parseInt(st.value) && value !== parseInt(st.value)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                            const hasAnyNearTick = specialTicks.filter(st => checkIfIsNear(st, value)).length > 0;
                            return hasAnyNearTick ? '' : value;
                        },
                        padding: 5,
                        // stepSize: 10,
                        autoSkip: false,
                        showLabelBackdrop: true,
                        major: {
                            enabled: true,
                        },
                        font: function (context) {
                            if (context.tick) {
                                if (context.tick.label.toString().includes(' ')) {
                                    return {
                                        weight: 'bold',
                                        size: '20',
                                        color: chartColorsList[0],
                                    };
                                } else {
                                    return {
                                        transform: 'translateX(-50%)',
                                    }
                                }
                            }
                        }
                    },
                    afterBuildTicks: function (chart) {
                        let common = [];

                        function checkIfContainsAny(listOfHoursConsumptionPerWeekDs, value) {
                            return Array.from(listOfHoursConsumptionPerWeekDs).filter(l => l.project.maxHours === value).length > 0
                        }

                        chart.ticks.forEach(t => {
                            if (!checkIfContainsAny(props.data.listOfHoursConsumptionPerWeekDs, t.value))
                                common.push([t.value, t.value])
                        })
                        Array.from(props.data.listOfHoursConsumptionPerWeekDs).forEach(l => {
                            common.push([l.project.maxHours, l.project.name + " " + l.project.number])
                        })
                        chart.ticks = [];
                        for (let i = 0; i < common.length; i++) {
                            const context = {
                                index: i,
                                tick: {
                                    value: common[i][0],
                                    label: common[i][1],
                                }
                            }
                            chart.ticks.push({
                                value: common[i][0],
                                label: common[i][1],
                                $context: context,
                            })
                        }
                    },
                    title: {
                        display: true,
                        text: 'Hours',
                        padding: {top: 20, left: 0, right: 0, bottom: 10},
                        font: {
                            // family: 'Times',
                            size: 20,
                            // style: 'normal',
                            // lineHeight: 1.2
                        },
                    },

                },
            },
            elements: {
                point: {
                    radius: pointRadius,
                    hoverRadius: pointRadius * 3,
                },
            },
            responsive: true,
            maintainAspectRatio: false,
            aspectRatio: ratio,
        },
        animation: {
            onProgress: function (animation) {
            },
            onComplete: function (animation) {
            }
        },

    }

    const switchChartType = () => {
        if (chartType === 'bar') {
            setChartType('line')
        } else if (chartType === 'line') {
            setChartType('bar')
        }
    }

    const ChartAreaWrapper = styled.div`
        height: ${heightString};
        width: ${widthString};
    `;

    const ChartWrapper = styled.div`
        position: relative;
        width: ${window.innerWidth + "px"};
        padding: 20px;
        overflow-x: scroll;
    `;

    return (
        <div>
            <div className="option-container">
                <button className="option-button" onClick={switchChartType}>Switch chart type</button>
                {limitDataHidden ?
                    <button className="option-button" onClick={() => setLimitDataHidden(!limitDataHidden)}>Show projects
                        limit</button>
                    :
                    <button className="option-button" onClick={() => setLimitDataHidden(!limitDataHidden)}>Hide projects
                        limit</button>
                }
                {mode === 'nearest' ?
                    <button className="option-button" onClick={() => setMode('null')}>Turn off tooltips</button>
                    :
                    <button className="option-button" onClick={() => setMode('nearest')}>Turn on tooltips</button>
                }
            </div>
            <ChartWrapper>
                <ChartAreaWrapper>
                    <Line type={cfg.type} plugins={[ChartDataLabels]} options={cfg.options} data={cfg.data}/>
                    {/*<canvas id="chartHours" width={400} height={400}/>*/}
                </ChartAreaWrapper>
            </ChartWrapper>
        </div>
    )


}
export default HoursEstimationDiagram;
import "./ToolsMenu.css"
import projectManagementIcon from "../../../resources/project-management-icon.png"
import projectEmpIcon from "../../../resources/project-employee-management-icon.png"
import projectDiagramIcon from "../../../resources/project-diagram-icon.png"
import capacityIcon from "../../../resources/capacity-icon.png"
import availabilityDiagramIcon from "../../../resources/availavility-diagram-icon.png"
import empManagementIcon from "../../../resources/employee-management-icon.png"
import hoursEstimationIcon from "../../../resources/hours-estimation-icon.png"
import timeManagementIcon from "../../../resources/time-management-icon.png"
import teamManagementIcon from "../../../resources/team-management-icon.png"
import {Link} from "react-router-dom";

const ToolsMenu = () => {
    return (
        <div className="t-container">
            <div>
                <h2>Management:</h2>
                <Link className="link" to="/project-management">
                    <div className="t-item" style={{backgroundColor: "var(--color-primary)"}}>
                        <div>Project management</div>
                        <img className="icon-image" src={projectManagementIcon} alt=""/>
                    </div>
                </Link>
                <Link className="link" to="/employee-management">
                    <div className="t-item" style={{backgroundColor: "var(--color-primary-light)"}}>
                        <div>Employees management</div>
                        <img className="icon-image" src={empManagementIcon} alt=""/>
                    </div>
                </Link>
                <Link className="link" to="/project-employee-management">
                    <div className="t-item" style={{backgroundColor: "var(--color-primary)"}}>
                        <div>Project-employee management</div>
                        <img className="icon-image" src={projectEmpIcon} alt=""/>
                    </div>
                </Link>
                <Link className="link" to="/team-management">
                    <div className="t-item" style={{backgroundColor: "var(--color-primary-light)"}}>
                        <div>Team management</div>
                        <img className="icon-image" src={teamManagementIcon} alt=""/>
                    </div>
                </Link>
                <Link className="link" to="/time-report-management">
                    <div className="t-item" style={{backgroundColor: "var(--color-primary)"}}>
                        <div>Time report management</div>
                        <img className="icon-image" src={timeManagementIcon} alt=""/>
                    </div>
                </Link>
                <h2>Analytical tools:</h2>
                <Link className="link" to="/availability-diagram">
                    <div className="t-item" style={{backgroundColor: "var(--color-primary-light)"}}>
                        <div>Availability diagram</div>
                        <img className="icon-image" src={availabilityDiagramIcon} alt=""/>
                    </div>
                </Link>
                <Link className="link" to="/hours-estimation-diagram">
                    <div className="t-item" style={{backgroundColor: "var(--color-primary)"}}>
                        <div>Hours estimation diagram</div>
                        <img className="icon-image" src={hoursEstimationIcon} alt=""/>
                    </div>
                </Link>
                <Link className="link" to="/capacity-diagram">
                    <div className="t-item" style={{backgroundColor: "var(--color-primary-light)"}}>
                        <div>Capacity diagram</div>
                        <img className="icon-image" src={capacityIcon} alt=""/>
                    </div>
                </Link>
                <Link className="link" to="/combo-diagram">
                    <div className="t-item" style={{backgroundColor: "var(--color-primary)"}}>
                        <div>Project statistics diagram</div>
                        <img className="icon-image" src={projectDiagramIcon} alt=""/>
                    </div>
                </Link>
            </div>
        </div>
    )
}
export default ToolsMenu;
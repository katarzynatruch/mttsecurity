
import { red } from '@material-ui/core/colors';
import axios from 'axios';
import React, { Component, useState } from 'react'
import { useHistory } from 'react-router';

const API_URL = 'http://localhost:8080/account/unauthorized/registration-new-password'
const API_URL_TOKEN_USED = 'http://localhost:8080/account/token-used'

const AssignPassword = () => {

    const [password, setPassword] = useState('');
    const [matchingPassword, setMatchingPassword] = useState('');
    const [errMessage, setErrMessage] = useState('');
    let history = useHistory();

    function matchExpression(str) {
        const rgularExp = {
            containsSpecial: /^[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/,
            containsNumber: /\d+/,
            containsAlphabet: /[a-zA-Z]/,

        }

        const expMatch = {};
        expMatch.containsNumber = rgularExp.containsNumber.test(str);
        expMatch.containsAlphabet = rgularExp.containsAlphabet.test(str);
        expMatch.alphaNumeric = rgularExp.containsSpecial.test(str);

        return expMatch;
    }

    const passwordValidator = (newPassword) => {
        if (newPassword.length >= 5 && matchExpression(newPassword)) {
            return true;
        } else {
            return false;
        }
    }

    const passwordMatches = (newPassword, matchingPassword) => {
        if (newPassword === matchingPassword) {
            return true;
        } else {
            return false;
        }
    }

const resetPassword = () => {
    if (passwordValidator(password)) {
        if (passwordMatches(password, matchingPassword)){
            const token = new URLSearchParams(window.location.search).get("token");
            const id = new URLSearchParams(window.location.search).get("id");
            axios.post(`${API_URL}?id=${id}&token=${token}`,
            createJSONdataPassword())
            .then(response => {
                console.log('response.data.message: ' + response.data.message);
                if (response.data.proper){
                    history.push(`/password-changed?msg=${response.data.message}`);
                    axios.post(`${API_URL_TOKEN_USED}?id=${id}&token=${token}`)
                    .catch((error) => {
                        if(error.response.status === 401){
                            history.push("/logout=true")
                        }
                    })
                }else{
                    setErrMessage(response.data.message);
                }
            })
            .catch((error) => {
                if(error.response.status === 401){
                    history.push("/logout=true")
                }
            })
        }else{
            setErrMessage('Passwords do not match')
        }
    }else{
        setErrMessage('New password do not meet requirements')
    }
}

const createJSONdataPassword = () => {
    
    const passwordDts = {

        password : password,
        matchingPassword : matchingPassword
    }

    return passwordDts;
}

    return (
        // <div>
        //     <h1>Reset Password</h1>
        //     <div className="container">
        //     <div style={{ color: red[500]}}>{errMessage}</div>
        //         Password: <input type="password" name="password" value={password} onChange={event => setPassword(event.target.value)} />
        //         Matching password: <input type="password" name="matchingPassword" value={matchingPassword} onChange={event => setmatchingPassword(event.target.value)} />
        //         <button className="btn btn-success" onClick={resetPassword}>Reset password</button>
        //     </div>
        // </div>

    <div>
        <div className="login-container">
            <h1>Change Password</h1>
            <div className="login-box">
                <div className="credentials-alert">{errMessage}</div>
                <div className="login-single-item">
                    <div className="login-label">Password</div>
                    <input className="login-input" type="password" name="password" value={password}
                           onChange={event => setPassword(event.target.value)}/>
                </div>
                <div className="login-single-item">
                    <div className="login-label">Matching password</div>
                    <input className="login-input" type="password" name="matchingPassword" value={matchingPassword}
                           onChange={event => setMatchingPassword(event.target.value)}/>
                </div>
                <button className="login-button" onClick={resetPassword}>Reset password</button>
            </div>
        </div>
        <div className="login-image"/>
    </div>
    )

}

export default AssignPassword;
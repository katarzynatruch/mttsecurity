import {useHistory} from "react-router-dom";
import React, {useEffect, useState} from "react";
import AuthenticationService from "../../../../Support/LoginService/AuthenticationService";
import axios from "axios";
import "./TimeReport.css"
import AddWorklogs from "./AddWorklogs";
import {red} from "@mui/material/colors";
import smudge1 from "../../../../resources/plama1.png"

const TimeReport = () => {

    const history = useHistory();
    const [data, setData] = useState([]);
    const [trigger, setTrigger] = useState(0);

    useEffect(() => {
        loadView();
        // return cleanupSub();
    }, [trigger])

    const cleanupSub = () => {
        setData([]);
    }
    const triggerUp = () => {
        setTrigger(trigger + 1)
    }

    const loadView = () => {
        const identificatorLoggedInUser = AuthenticationService.getLoggedInUserName();
        const userData = {
            identificator: identificatorLoggedInUser,
        }

        axios.post('http://localhost:8080/employee/timeReport/score', userData)
            .then(response => response.data)
            .then(data => {
                setData(data)
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
    }
    const compareWorklogs = (a, b) => {
        const aa = parseInt(a.year + a.week);
        const bb = parseInt(b.year + b.week);
        if(aa === bb){
            return 0;
        }
        return aa < bb ? 1 : -1
    }

    let sortedWorklogs = [];
    if(data.workLogWeeks){
        sortedWorklogs = Array.from(data.workLogWeeks).sort(compareWorklogs);
    }

    return (
        data.employee ?
            <div className="time-container">
                <img className="smudge1-img" src={smudge1} alt={"ïmg"}/>
                <div className="time-top-info">
                    <div className="time-top-info-item">Identificator: {data.employee.identificator}</div>
                    <div className="time-top-info-item">Today is Week {data.currentWeek} Year {data.currentYear}</div>
                    <div className="time-top-info-item">
                        Your managers:
                    </div>
                </div>
                <div className="time-top-info-item">
                    Report your time:
                </div>
                <div className="worklog-container">
                    {data.workLogWeeks ?
                        Array.from(sortedWorklogs).map(wls =>
                            <div>
                                {wls.week === data.currentWeek && wls.year === data.currentYear ?
                                    <div className="current-worklog">
                                        <div>
                                            <div>
                                                <AddWorklogs week={data.currentWeek} year={data.currentYear}
                                                             projectWithHours={data.projectsAndTimeForEmployeeDs.projectsWithHours}
                                                             worklogs={wls.workLogs} triggerUp={triggerUp}/>
                                            </div>
                                        </div>
                                    </div>
                                    : <div/>}
                                {wls.week < data.currentWeek && wls.year === data.currentYear ?
                                    <div className="past-worklog">
                                        <div className="add-worklog-top-info">
                                            W{wls.week} Y{wls.year}
                                        </div>
                                        <div>
                                            {Array.from(wls.workLogs).map(worklog =>
                                                <div className="past-worklog-data">
                                                    <div>
                                                        {worklog.project.name} {worklog.project.number}
                                                    </div>
                                                    <div>
                                                        {worklog.workedHours} [h]
                                                    </div>
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                    : <div/>}
                                {
                                    wls.week > data.currentWeek && wls.year < data.currentYear ?
                                        <div>
                                            W{wls.week} Y{wls.year}
                                            <div>
                                                {Array.from(wls.workLogs).map(worklog =>
                                                    <div key={worklog.project.number + worklog.workedHours}>
                                                        {worklog.project.name} {worklog.project.number} and {worklog.workedHours}
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                        : <div/>}
                            </div>)
                        :
                        <AddWorklogs week={data.currentWeek} year={data.currentYear}
                                     projectWithHours={data.projectsAndTimeForEmployeeDs.projectsWithHours}
                                     worklogs={{}} triggerUp={triggerUp}/>
                    }
                </div>
                <div className="time-bottom-project-info">
                    <div>

                    </div>
                    Your projects:
                    {Array.from(data.projectsAndTimeForEmployeeDs.projectsWithHours).map(pwh =>
                        <div className="project-info-container">
                            <div className="project-name" key={pwh.project.number}>
                                Project {pwh.project.name} {pwh.project.number}
                            </div>
                            <div className="hours-info">
                                Hours used: {pwh.hoursUsed}
                            </div>
                            {pwh.hoursLeft < 1 ?
                                <div className="hours-info" style={{color: red[500]}}>
                                    Hours left: {pwh.hoursLeft}
                                </div>
                                :
                                <div className="hours-info">
                                    Hours left: {pwh.hoursLeft}
                                </div>
                            }
                        </div>
                    )}
                </div>
            </div>
            : <div></div>
    )

}
export default TimeReport;
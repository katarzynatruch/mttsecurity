import {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import axios from "axios";
import {TextField} from "@mui/material";

const MaxHoursInput = (props) => {
    const [maxHoursState, setMaxHoursState] = useState(0);
    let history = useHistory();

    useEffect(() => {
        loadData();


    }, [])

    const loadData = () => {
        axios.get(`http://localhost:8080/project-management/loadMaxHours/${props.employee.identificator}&${props.project.number}`)
            .then((response) => {
                    setMaxHoursState(response.data);
                    props.triggerUp();
                }
            )
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
    }
    const handleMaxHours = (maxHoursSingle, employee, project) => {
        const singleEmployeeProjectMaxHours = {
            employeeIdentificator: employee.identificator,
            projectNumber: project.number,
            maxHours: maxHoursSingle,
        }
        saveEmployeeWithMaxHours(singleEmployeeProjectMaxHours);
    }

    const saveEmployeeWithMaxHours = (singleEmployeeProjectMaxHours) => {
        console.log(singleEmployeeProjectMaxHours.maxHours);
        axios.post(`http://localhost:8080/project-management/list/saveSingleEmployee`, {
            employeeIdentificator: singleEmployeeProjectMaxHours.employeeIdentificator,
            projectNumber: singleEmployeeProjectMaxHours.projectNumber,
            maxHours: singleEmployeeProjectMaxHours.maxHours,
        })
            .then(() => {
                    props.triggerUp();
                }
            )
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
    }

    const handleInputKeyDown = (event) => {
        if (event.key === 'Enter') {
            handleMaxHours(event.target.value, props.employee, props.project)
        }
    }

    return (
        <div style={{maxWidth: 100, minWidth: 70}}>
            <TextField
                disabled={props.disabled}
                type="number"
                value={maxHoursState}
                onChange={(event) => setMaxHoursState(event.target.value)}
                onKeyDown={handleInputKeyDown}
            />
        </div>
    )
}
export default MaxHoursInput;

import React, {useEffect, useState} from 'react';
import Checkbox from '@mui/material/Checkbox';
import axios from 'axios';
import AuthenticationService from 'Support/LoginService/AuthenticationService';
import {Link, useHistory} from 'react-router-dom';
import EnhancedTableHeadWithTeamName from './EnhancedTableHeadWithTeamName';
import {FormControl, InputLabel, MenuItem, Select} from '@mui/material';
import EnhancedTableToolbarWithSelected from './EnhancedTableToolbarWithSelected';
import './TeamManagement.css'
import SupportService from "../../../../Support/SupportService";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import AddCardRoundedIcon from "@mui/icons-material/AddCardRounded";
import teamManagementBackground from "../../../../resources/team-management-background.png"
import {useStateIfMounted} from "use-state-if-mounted";

function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function getComparator(order, orderBy) {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

function EmployeesInTeamsManagement() {
    const [order, setOrder] = useStateIfMounted('asc');
    const [orderBy, setOrderBy] = useStateIfMounted('calories');
    const [selected, setSelected] = useStateIfMounted([]);
    const [dense, setDense] = useStateIfMounted(true);
    const [heads, setHeads] = useStateIfMounted([]);
    const [teamsData, setTeamsData] = useStateIfMounted([]);
    const [allUsers, setAllUsers] = useStateIfMounted([]);
    const [trigger, setTrigger] = useStateIfMounted(0);
    const [openCollapse, setOpenCollapse] = useStateIfMounted([]);
    let history = useHistory();

    useEffect(() => {
        createHeads();
        getAllUsers();
        getManagerTeams();
        return cleanupSub();
    }, [trigger])

    const cleanupSub = () => {
        setTeamsData([]);
        setAllUsers([]);
    }

    function getManagerTeams() {
        const loggedInUserIdentificator = AuthenticationService.getLoggedInUserName();
        const managerId = SupportService.getManagerId(loggedInUserIdentificator);

        if (managerId == null) {
        } else {
            axios.get(`http://localhost:8080/team/management/${loggedInUserIdentificator}`)
                .then(response => response.data)
                .then(data => {
                    setTeamsData(createDataWithKey(data.employeeTeamLoadDts))
                })
                .catch((error) => {
                    if (error.response.status === 401) {
                        history.push("/logout=true")
                    }
                })
        }
    }

    function getAllUsers() {
        axios.get('http://localhost:8080/user/load-list-all')
            .then(response => response.data)
            .then(data => {
                setAllUsers(createUsersData(data.users))
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
    }

    function createUsersData(users) {
        return users.map(user => {
            return {
                firstName: user.firstName,
                lastName: user.lastName,
                identificator: user.identificator,
                email: user.email,
            }
        })
    }

    function createDataWithKey(employeeTeamLoadDts) {
        if (!employeeTeamLoadDts) {
            return []
        }
        return Array.from(employeeTeamLoadDts).map(empTeamDts => createNewEmpTeamDts(empTeamDts))
    }

    function createNewEmpTeamDts(empTeamDts) {
        const listOfEmployees = empTeamDts.employees.map(employee => {
            return {
                firstName: employee.firstName,
                lastName: employee.lastName,
                identificator: employee.identificator,
                email: employee.email,
                team: empTeamDts.team,
                key: employee.identificator + empTeamDts.team.name
            }
        });

        let newEmpTeamDts = {
            team: empTeamDts.team,
            employees: listOfEmployees,
            managerId: empTeamDts.managerId,
        }
        return newEmpTeamDts;
    }

    function createHeads() {
        getManagerTeams();
        const tempHeads = Array.from(teamsData).map(employeeTeamLoadDts => employeeTeamLoadDts.team.name)
        setHeads(tempHeads);
        // triggerUp();
    }

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleSelectAllClick = (event) => {
        if (event.target.checked) {
            const selectedTeam = teamsData.filter(teamData => teamData.team.name === event.target.value)
            const newSelecteds = selectedTeam[0].employees.map(employee => employee.key)
            setSelected(newSelecteds);
            return;
        }
        setSelected([]);
    };

    const handleSelectedFromListEmployee = (event, teamDS) => {
        const employeeTeamUpdateDsList = [];
        const employeeIdentificatorList = teamDS.employees.map(employee => employee.identificator)
        employeeIdentificatorList.push(event.target.value)
        const singleEmployeeTeamUpdateDts = {
            team: teamDS.team,
            employeeIdentificatorList: employeeIdentificatorList,
            managerIdentificator: AuthenticationService.getLoggedInUserName()
        }

        employeeTeamUpdateDsList.push(singleEmployeeTeamUpdateDts);

        saveUpdatedTeam(employeeTeamUpdateDsList);
        setTrigger(trigger + 1)
    }

    const saveUpdatedTeam = (employeeTeamUpdateDsList) => {
        axios.post('http://localhost:8080/team/manageTeams', {employeeTeamUpdateDsList})
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
    }

    const handleClick = (event, key) => {
        const selectedIndex = selected.indexOf(key);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, key);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }
        setSelected(newSelected);
    };
    const isSelected = (key) => selected.indexOf(key) !== -1;

    const checkIfselected = (employee, selecteds) => {
        return selecteds.filter(sel => sel === employee.key).length > 0
    }

    const filterEmployeesNotSelected = (selecteds) => {

        const allListedPositionsOfEmployees = []
        teamsData.forEach(teamsData => teamsData.employees.forEach(employee => {
            allListedPositionsOfEmployees.push(employee)
        }));
        const filtered = allListedPositionsOfEmployees.filter(employee => !checkIfselected(employee, selecteds))
        return filtered;
    }
    const getAllNotBelongingToTeamUser = (teamDS, allUsers) => {
        return allUsers.filter(user => !checkIfTeamContainsUser(user, teamDS))
    }

    const checkIfTeamContainsUser = (user, teamDS) => {
        if (teamDS.employees.filter(employee => employee.identificator === user.identificator).length > 0) {
            return true;
        }
        return false;
    }
    const openCollapseTeam = (isOpen, teamName) => {
        const tempOpenCollapse = openCollapse.filter(oc => oc.teamName !== teamName)
        setOpenCollapse([...tempOpenCollapse, {isOpen: isOpen, teamName: teamName}])
    }

    const checkIfShouldBeOpen = (teamDS) => {
        const collapseState = openCollapse.filter(item => item.teamName === teamDS.team.name)
        if (collapseState.length > 0) {
            return collapseState[0].isOpen
        }
        return false;
    }

    const triggerUp = () => {
        setTrigger(trigger + 1);
        console.log("trigger " + trigger)
    }

    const growStyle = {
        // -moz-transition: 'height .5s',
        // -ms-transition: 'height .5s',
        // -o-transition: 'height .5s',
        // -webkit-transition: 'height .5s',
        transition: ' max-height .5s',
        height: 0,
        overflow: 'hidden',
    }

    return (
        <div className="t-m-container">
            {teamsData ? (
                    <Paper>
                        <EnhancedTableToolbarWithSelected numSelected={selected.length}
                                                          notSelected={filterEmployeesNotSelected(selected)}
                                                          triggerUp={triggerUp} updateSelected={() => {
                            setSelected([])
                        }} teamsData={teamsData}/>
                        <TableContainer>
                            <Table
                                stickyHeader
                                className="table-container"
                                aria-labelledby="tableTitle"
                                size={dense ? 'small' : 'medium'}
                                aria-label="enhanced table"
                            >
                                {Array.from(teamsData).map(teamDS =>
                                    <>
                                        <EnhancedTableHeadWithTeamName
                                            numSelected={selected.length}
                                            order={order}
                                            orderBy={orderBy}
                                            onSelectAllClick={handleSelectAllClick}
                                            onRequestSort={handleRequestSort}
                                            rowCount={teamDS.employees.length}
                                            headTeam={teamDS.team.name}
                                            employeesForTeam={teamDS.employees.filter(employee => checkIfselected(employee, selected))}
                                            openCollapse={openCollapseTeam}
                                            teamsData={teamsData}
                                            triggerUp={triggerUp}
                                        />
                                        {/*<Collapse in={checkIfShouldBeOpen(teamDS)} timeout="auto" unmountOnExit>*/}
                                        {checkIfShouldBeOpen(teamDS) ?
                                            <TableBody style={{...growStyle}}>
                                                {stableSort(teamDS.employees, getComparator(order, orderBy))
                                                    .map((row, index) => {
                                                        const isItemSelected = isSelected(row.key);
                                                        const labelId = `enhanced-table-checkbox-${index}`;
                                                        return (

                                                            <TableRow
                                                                style={{padding: 0}}
                                                                hover
                                                                onClick={(event) => handleClick(event, row.key)}
                                                                role="checkbox"
                                                                aria-checked={isItemSelected}
                                                                tabIndex={-1}
                                                                key={row.key}
                                                                selected={isItemSelected}>
                                                                <TableCell/>
                                                                {/*<TableCell padding="checkbox">*/}
                                                                <TableCell>
                                                                    <Checkbox
                                                                        checked={isItemSelected}
                                                                        inputProps={{'aria-labelledby': labelId}}/>
                                                                </TableCell>
                                                                <TableCell
                                                                    component="th" id={labelId} scope="row"
                                                                    padding="none">
                                                                    {row.name}
                                                                </TableCell>
                                                                <TableCell align="right">{row.identificator}</TableCell>
                                                                <TableCell align="right">{row.firstName}</TableCell>
                                                                <TableCell align="right">{row.lastName}</TableCell>
                                                                <TableCell/>
                                                            </TableRow>
                                                        );
                                                    })}
                                                <TableRow>
                                                    <TableCell colSpan={6}>
                                                        <FormControl fullWidth>
                                                            <InputLabel id="employee">Choose employee</InputLabel>
                                                            <Select
                                                                labelId="employee"
                                                                id="identificator"
                                                                label="Choose employee"
                                                                onChange={(event) => {
                                                                    return handleSelectedFromListEmployee(event, teamDS);
                                                                }}>
                                                                {Array.from(getAllNotBelongingToTeamUser(teamDS, allUsers)).map(user => (
                                                                    <MenuItem
                                                                        value={user.identificator}>{user.firstName + " " + user.lastName + " "
                                                                        + user.identificator}</MenuItem>
                                                                ))}
                                                            </Select>
                                                        </FormControl>
                                                    </TableCell>
                                                </TableRow>
                                            </TableBody>
                                            : <TableBody style={{...growStyle}}/>}
                                        {/*</Collapse>*/}

                                    </>
                                )}
                            </Table>
                        </TableContainer>
                        <Link className="link-p-management" to="add-employee">
                            <div className="add-p-container">
                                <AddCardRoundedIcon/>
                                <div>Add employee to database</div>
                            </div>
                        </Link>
                    </Paper>
                ) :
                (<div/>)
            }
            <img src={teamManagementBackground} alt="img" className="t-m-image"/>
        </div>
    );
}

export default EmployeesInTeamsManagement;

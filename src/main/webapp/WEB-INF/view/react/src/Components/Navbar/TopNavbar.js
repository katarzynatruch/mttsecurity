import AuthenticationService from "../../Support/LoginService/AuthenticationService";
import axios from "axios";
import {useStateIfMounted} from "use-state-if-mounted";
import supportService from "../../Support/SupportService";
import {Link, useHistory} from "react-router-dom";
import HomeRoundedIcon from '@material-ui/icons/HomeRounded';
import SettingsIcon from "@material-ui/icons/Settings";
import MenuRoundedIcon from '@material-ui/icons/MenuRounded';
import ArrowDropDownRoundedIcon from '@material-ui/icons/ArrowDropDownRounded';
import ArrowDropUpRoundedIcon from '@mui/icons-material/ArrowDropUpRounded';
import LoginRoundedIcon from '@mui/icons-material/LoginRounded';
import LogoutIcon from '@mui/icons-material/Logout';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import React from "react";
import "./Navbar.css"
import logo from "../../resources/logo2.png"


const TopNavbar = () => {
    const API_URL = 'http://localhost:8080'
    const loggedInUserIdentificator = AuthenticationService.getLoggedInUserName();
    const [isAManager, setIsAManager] = useStateIfMounted(false);
    const [isAnAdmin, setIsAnAdmin] = useStateIfMounted(false);
    const [firstName, setFirstName] = useStateIfMounted('');
    const [lastName, setLastName] = useStateIfMounted('');
    const [identificator, setIdentificator] = useStateIfMounted('');
    const [showMenu, setShowMenu] = useStateIfMounted(false);
    let history = useHistory();


    axios.post(`${API_URL}/user/get_roles`, {loggedInUserIdentificator})
        .then(response => response.data)
        .then(data => {
            setIsAManager(supportService.checkIfContainsManagerRole(data.userRoleNameList))
            setIsAnAdmin(supportService.checkIfContainsAdminRole(data.userRoleNameList))
        })
        // .catch((error) => {
        //     if (error.response.status === 401) {
        //         history.push("/logout=true")
        //     } else {
        //     }
        // })
    const getUserData = () => {
        axios.post(`${API_URL}/user/get_user_data`, {loggedInUserIdentificator})
            .then(response => response.data)
            .then(data => {
                setFirstName(data.firstName);
                setLastName(data.lastName);
                setIdentificator(data.identificator);
            })
            // .catch((error) => {
            //     if (error.response.status === 401) {
            //         history.push("/logout=true")
            //     } else {
            //     }
            // })
    }
    const UserMenu = () => {
        return (
            <nav className="sidebar-container">
                <div className="sidebar-item" onClick={() => history.push('/change-password')}>
                    <div><LockOpenIcon/></div>
                    <div className="sidebar-text">Change password</div>
                </div>
                <span className="sidebar-separator"/>
                <div className="sidebar-item" onClick={() => history.push('/account-settings')}>
                    <div>< SettingsIcon/></div>
                    <div className="sidebar-text">Settings</div>
                </div>
                <span className="sidebar-separator"/>
                <div className="sidebar-item" onClick={() => history.push('/logout=true')}>
                    <div><LogoutIcon/></div>
                    <div className="sidebar-text">Sign out</div>
                </div>
            </nav>
        )
    }

    const AdminAndManagerNavbar = () => {
        getUserData();
        return (
            <div>
                <div className="navbar-container">
                    <div className="left-navbar">
                        <Link className="link-nav" to="/homepage">
                            <HomeRoundedIcon style={{fontSize: 40}}/>
                        </Link>
                        <img className="logo" src={logo}/>
                    </div>
                    <div className="right-navbar">
                        <div className="item">
                            <Link className="link-nav" to="/about">About</Link>
                        </div>
                        <div className="item">
                            <Link className="link-nav" to="/tools-menu">Manager tools</Link>
                        </div>
                        <div className="item">
                            <Link className="link-nav" to="/time-report-menu">Time report</Link>
                        </div>
                        <div className="item">
                            <Link className="link-nav" to="/support">Support</Link>
                        </div>
                    </div>
                    {showMenu ?
                        <div className="logging" onClick={() => setShowMenu(!showMenu)}>
                            {firstName} {lastName} <ArrowDropUpRoundedIcon/>
                        </div>
                        :
                        <div className="logging" onClick={() => setShowMenu(!showMenu)}>
                            {firstName} {lastName} <ArrowDropDownRoundedIcon/>
                        </div>
                    }
                </div>
                <div>
                    {showMenu ? <UserMenu/> : <div/>}
                </div>
            </div>
        )
    }

    const EmployeeNavbar = () => {
        getUserData();

        return (
            <div>
                <div className="navbar-container">
                    <div className="left-navbar">
                        <Link className="link-nav" to="/homepage">
                            <HomeRoundedIcon style={{fontSize: 40}}/>
                        </Link>
                        <img className="logo" src={logo}/>
                    </div>
                    <div className="right-navbar">

                        <div className="item">
                            <Link className="link-nav" to="/about">About</Link>
                        </div>
                        <div className="item">
                            <Link className="link-nav" to="/time-report-menu">Time report</Link>
                        </div>
                        <div className="item">
                            <Link className="link-nav" to="/support">Support</Link>
                        </div>
                    </div>
                    {showMenu ?
                        <div className="logging" onClick={() => setShowMenu(!showMenu)}>
                            {firstName} {lastName} <ArrowDropUpRoundedIcon/>
                        </div>
                        :
                        <div className="logging" onClick={() => setShowMenu(!showMenu)}>
                            {firstName} {lastName} <ArrowDropDownRoundedIcon/>
                        </div>
                    }
                </div>
                <div>
                    {showMenu ? <UserMenu/> : <div/>}
                </div>
            </div>
        )
    }
    const NotLoggedNavbar = () => {
        return (
            <div>
                <div className="navbar-container">
                    <div className="left-navbar">
                        <Link className="link-nav" to="/homepage">
                            <HomeRoundedIcon style={{fontSize: 40}}/>
                        </Link>
                        <img className="logo" src={logo}/>
                    </div>
                    <div className="right-navbar">
                        <div className="item">
                            <Link className="link-nav" to="/about">About</Link>
                        </div>
                        <div className="item">
                            <Link className="link-nav" to="/support">Support</Link>
                        </div>
                    </div>
                    <Link className="link-nav" to="/login">
                        <div className="logging">
                            Log in
                            <LoginRoundedIcon style={{fontSize: 40}}/>
                        </div>
                    </Link>
                </div>
            </div>
        )
    }

    return (
        AuthenticationService.isUserLoggedIn() ?
            isAManager ?
                isAnAdmin ?
                    <AdminAndManagerNavbar/>
                    :
                    <AdminAndManagerNavbar/>
                :
                <EmployeeNavbar/>
            :
            <NotLoggedNavbar/>
    )

}
export default TopNavbar
;
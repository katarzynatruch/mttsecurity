import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import "./Login.css"

const API_URL = 'http://localhost:8080/account/unauthorized/resetPassword'


const ForgotPassword = () => {

    const [email, setEmail] = useState('');
    const [isEmailValid, setEmailValid] = useState(false);
    const [errMessage, setErrMessage] = useState('');
    const [trigger, setTrigger] = useState(false);
    let history = useHistory();

    const sendLinkToReset = () => {
        axios.post(`${API_URL}`, createJsonDataEmail())
            .then(response => {
                console.log(response.data.message)
                console.log(response.data.proper)
                setErrMessage(response.data.message);
                setEmailValid(response.data.proper);
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })

    }

    const createJsonDataEmail = () => {
        return {
            email: email
        };
    }

    if (isEmailValid) {
        history.push(`/password-reset-token`)
    }
    return (
        <div>
            <div className="login-container">
                <h1>Email:</h1>
                <div className="login-box" style={{minHeight : 250}}>
                    <div className="credentials-alert">{errMessage}</div>
                    <div className="login-single-item">
                        <div className="login-label">Email</div>
                        <input className="login-input" type="text" name="email" value={email}
                               onChange={event => setEmail(event.target.value.trim())}/>
                    </div>
                    <button className="login-button" onClick={sendLinkToReset}>Reset password</button>
                </div>
            </div>
        </div>
)

}


export default ForgotPassword;
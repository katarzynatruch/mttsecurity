import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
} from "react-router-dom";

const PasswordChangedComponent = () => {

    const message = new URLSearchParams(window.location.search).get("msg");

    return (
        <div className="reg-container">
            <h2>{message}</h2>
            <h3><Link to="/login">Login</Link></h3>
            
        </div>
    )
}

export default PasswordChangedComponent;
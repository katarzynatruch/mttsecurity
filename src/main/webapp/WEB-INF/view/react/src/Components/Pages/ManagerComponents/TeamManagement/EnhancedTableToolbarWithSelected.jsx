import React from "react";
import EnhancedTableToolbar from "./EnhancedTableToolbar";

const withSelected = (Component) => {
  return class WithSelected extends React.Component {
    render() {
      const notSelected = this.props.notSelected;
      const triggerUp = () => { this.props.triggerUp() }
      return (
        <Component notSelected={notSelected} triggerUp={triggerUp} teamsData={this.props.teamsData} updateSelected = {this.props.updateSelected} {...this.props} />
      );
    }
  }
}
const EnhancedTableHeadWithTeamName = withSelected(EnhancedTableToolbar)

export default EnhancedTableHeadWithTeamName
import React, {useEffect, useRef} from "react";
import {useStateIfMounted} from "use-state-if-mounted";
import {Multiselect} from "multiselect-react-dropdown";
import "./Filters.css"
import {Checkbox} from "@mui/material";

const MultiselectForProjects = (props) => {

    const [allProjectTypes, setAllProjectTypes] = useStateIfMounted([]);
    const [selected, setSelected] = useStateIfMounted([]);
    const [trigger, setTrigger] = useStateIfMounted(0);
    const [options, setOptions] = useStateIfMounted([]);
    const multiselectRef = useRef();

    let startTimerInterval = useRef();
    if(trigger<1){
        startTimerInterval.current = setTimeout(() => {
            setTrigger(trigger+1)
        }, (2000));
    }


    useEffect(() => {
        sendDataToParent();
        loadData();
        // return cleanupSub();
    }, [trigger])
    // }, [trigger])

    const cleanupSub = () => {
        setAllProjectTypes([]);
        setSelected([]);
        setOptions([]);
    }
    const loadData = () => {
        setOptions(props.projects)
        // if (props.projects.length < 1)
        //     setTrigger(trigger + 1)
        // props.triggerUp();
    }

    const sendDataToParent = () => {
        const projectNumbers = selected.map(selected => selected.number)
        props.projectsData(projectNumbers);
    }

    const onSelect = (selectedList, selectedItem) => {
        updateProjectTypeCheckboxOnAdd(selectedItem);
        setSelected(selectedList);
        // props.triggerUp();
        setTrigger(trigger + 1)
    }
    const onRemove = (selectedList, removedItem) => {
        updateProjectTypeCheckboxOnRemove(removedItem);
        setSelected(selectedList);
        // props.triggerUp();
        setTrigger(trigger + 1)
    }

    const updateProjectTypeCheckboxOnAdd = (selectedItem) => {
        const targetSelectedProjectList = [...selected, selectedItem]
        const projectCheckboxesUnchecked = options.filter(project => !checkIfProjectTypeCheckboxShouldBeChecked(project.type)).map(project => project.type)
        projectCheckboxesUnchecked.forEach(projectType => updateCheckboxStateOnAdd(targetSelectedProjectList, projectType))
    }
    const updateCheckboxStateOnAdd = (targetSelectedProjectList, projectType) => {
        const allProjectsForType = options.filter(project => project.type === projectType)
        if (checkIfSelectedProjectsCoverAllProjectTypeList(targetSelectedProjectList, allProjectsForType)) {
            setAllProjectTypes([...allProjectTypes, projectType])
        }
    }
    const checkIfSelectedProjectsCoverAllProjectTypeList = (targetSelectedProjectList, allProjectsForType) => {
        return allProjectsForType.filter(projectByType => checkIfProjectIsOnTheList(projectByType, targetSelectedProjectList)).length === allProjectsForType.length
    }

    const updateProjectTypeCheckboxOnRemove = (removedItem) => {
        const targetSelectedProjectList = selected.filter(project => project !== removedItem)
        allProjectTypes.forEach(projectType => updateCheckboxStateOnRemove(targetSelectedProjectList, projectType))
    }
    const updateCheckboxStateOnRemove = (targetSelectedProjectList, projectType) => {
        const allProjectsForType = options.filter(project => project.type === projectType)
        if (!checkIfSelectedProjectsCoverAllProjectTypeList(targetSelectedProjectList, allProjectsForType)) {
            const tempProjectTypesList = allProjectTypes.filter(selectedProjectType => selectedProjectType !== projectType)
            setAllProjectTypes(tempProjectTypesList)
        }
    }
    const handleResetClick = (event) => {
        setSelected([]);
        setAllProjectTypes([]);
        setTrigger(trigger+1)
    }

    function checkIfProjectTypeCheckboxShouldBeChecked(projectType) {
        const checkingProjects = allProjectTypes.filter(allProjectType => allProjectType === projectType)
        if (checkingProjects.length > 0) {
            return true;
        }
        return false;
    }

    const onChangeAllProjects = (event) => {
        // setTrigger(trigger + 1)
        const isChecked = event.target.checked;
        if (isChecked) {
            setAllProjectTypes([...allProjectTypes, event.target.value])
            updateAddSelection(event.target.value);
        } else {
            const selectedProjects = allProjectTypes.filter(item => item !== event.target.value)
            setAllProjectTypes(selectedProjects);
            updateRemoveSelection(event.target.value);
        }
        setTrigger(trigger + 1)
        // props.triggerUp();

    }

    function updateAddSelection(checkedProjectType) {
        const projectsThatShouldBeSelected = options.filter(project => project.type === checkedProjectType)
        const projectsNotSelectedYet = projectsThatShouldBeSelected.filter(project => !checkIfProjectIsSelected(project))
        const finalProjectList = [];
        selected.forEach(project => finalProjectList.push(project))
        projectsNotSelectedYet.forEach(project => finalProjectList.push(project))
        setSelected(finalProjectList);
    }

    function updateRemoveSelection(uncheckedProjectType) {
        const projectsThatShouldNotBeSelected = options.filter(project => project.type === uncheckedProjectType)
        const projectsThatShouldBeRemoved = projectsThatShouldNotBeSelected.filter(project => checkIfProjectIsSelected(project))
        const projectsThatShouldStaySelected = selected.filter(project => !checkIfProjectIsOnTheList(project, projectsThatShouldBeRemoved))
        setSelected(projectsThatShouldStaySelected);
    }

    const checkIfProjectIsSelected = (project) => {
        return selected.filter(selectedProject => selectedProject.number === project.number).length > 0
    }
    const checkIfProjectIsOnTheList = (project, list) => {
        return list.filter(listProject => listProject.number === project.number).length > 0
    }
    const projectTypes = [];
    props.projects.map(project => {
        if (projectTypes.indexOf(project.type) === -1) {
            projectTypes.push(project.type)
        }
    })
    const multiselectStyle = {
        multiselectContainer: {
            fontSize: 15,
            display: "flex",

        },
        searchBox: {
            minHeight: 40,
            width: "-webkit-fill-available",
            minWidth: 220,

        },
        inputField: {
            // margin: 12,
            // display : "none",
        },
        chips: {backgroundColor: "#85a2a6",},
        optionContainer: {},
        option: {
            whitespace: "nowrap",
        },
    }

    return (
        <div className="multiselect-container">
            <div>
                {projectTypes.map(projectType => (
                    <div style={{display: "flex", flexWrap: "nowrap", alignItems: "center"}}>
                        <Checkbox sx={{
                            color: "#85a2a6",
                            '&.Mui-checked': {
                                color: "#85a2a6",
                            },
                        }}
                                  checked={checkIfProjectTypeCheckboxShouldBeChecked(projectType)}
                                  type="checkbox"
                                  onChange={onChangeAllProjects}
                                  value={projectType}
                        />
                        <label style={{whiteSpace: "nowrap"}}>All {projectType} projects</label>
                    </div>
                ))}
            </div>
            {console.log(options)}
            <Multiselect
                style={multiselectStyle}
                options={options.length > 0 ? options : [{
                    name: "No options available",
                    number: "No options available",
                    type: "No options available",
                    optionName: "No options available",
                }]}
                displayValue="optionName"
                groupBy="type"
                onRemove={onRemove}
                onSearch={function noRefCheck() {
                }}
                onSelect={onSelect}
                selectedValues={selected}
                ref={multiselectRef}
                closeOnSelect={false}
                showCheckbox={true}
                placeholder={options.length < 1? "No options available" : "Select"}
                disable = {options.length < 1}
            />
            <button className="reset-button"
                    onClick={handleResetClick}>Reset
            </button>
        </div>
    )
}

export default MultiselectForProjects;




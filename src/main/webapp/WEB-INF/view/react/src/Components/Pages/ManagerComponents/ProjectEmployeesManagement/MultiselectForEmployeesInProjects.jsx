import React, {useEffect, useState} from "react";
import {FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import axios from "axios";
import {useHistory} from "react-router-dom";
import {useStateIfMounted} from "use-state-if-mounted";
import AuthenticationService from "../../../../Support/LoginService/AuthenticationService";


const MultiselectForEmployeesInProjects = (props) => {

    const [allUsers, setAllUsers] = useStateIfMounted([]);
    const [trigger, setTrigger] = useStateIfMounted(0);
    let history = useHistory();

    useEffect(() => {
        getAllUsers();
        // return cleanupSub();
    }, [trigger])

    const cleanupSub = () => {
        setAllUsers([]);
    }

    const triggerUp = () => {
        setTrigger(trigger + 1);
    }

    function getAllUsers() {
        //to change by manager!
        console.log(props)
        // axios.get('http://localhost:8080/admin/loadUsers')
        const userLoggedIn = AuthenticationService.getLoggedInUserName();
        axios.get(`http://localhost:8080/employee/load-list/${userLoggedIn}`)
            .then(response => response.data)
            .then(data => {
                setAllUsers(createUsersData(data.employees))
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
    }

    function createUsersData(users) {
        return users.map(user => {
            return {
                firstName: user.firstName,
                lastName: user.lastName,
                identificator: user.identificator,
                email: user.email,
            }
        })
    }

    function handleSelectedFromListEmployee(event) {
        const selectedEmployeesTempList = Array.from(props.projectWithEmployees.employees).map(selectedEmployee => selectedEmployee.identificator);
        selectedEmployeesTempList.push(event.target.value)
        console.log(selectedEmployeesTempList);
        const projectEmployeesList = [];
        const singleProjectEmployeeElement = {
            employeeIdentificators: selectedEmployeesTempList,
            projectNumber: props.projectWithEmployees.project.number,
        }
        projectEmployeesList.push(singleProjectEmployeeElement);
        saveProjectWithEmployeeList(projectEmployeesList);
    }

    const saveProjectWithEmployeeList = (projectEmployeesList) => {
        axios.post('http://localhost:8080/project-management/list/saveProject', {projectEmployeesList})
            .then(() => {
                props.triggerUp();
                }
            )
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
    }

    const getAllNotBelongingToProjectUser = (projectWithEmployees, allUsers) => {
        return allUsers.filter(user => !checkIfProjectContainsUser(user, projectWithEmployees))
    }

    const checkIfProjectContainsUser = (user, projectWithEmployees) => {
        return projectWithEmployees.employees.filter(employee => employee.identificator === user.identificator).length > 0;

    }

    return (
        <div style={{paddingTop:15}}>
            <FormControl fullWidth>
                <InputLabel id="employee">Select employee</InputLabel>
                <Select
                    disabled={props.disabled}
                    labelId="employee"
                    id="identificator"
                    label="Select employee"
                    onChange={(event) => handleSelectedFromListEmployee(event, props.projectWithEmployees)}
                    defaultValue=""
                >
                    {Array.from(getAllNotBelongingToProjectUser(props.projectWithEmployees, allUsers)).map(user => (
                        <MenuItem key={user}
                                  value={user.identificator}>{user.firstName + " " + user.lastName + " "
                            + user.identificator}</MenuItem>
                    ))}
                </Select>
            </FormControl>
        </div>
    )
}
export default MultiselectForEmployeesInProjects;
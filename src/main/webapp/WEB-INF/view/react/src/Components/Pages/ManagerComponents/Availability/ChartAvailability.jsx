import {Line} from 'react-chartjs-2';
import '../Chart.css'
import styled from 'styled-components';
import 'chartjs-adapter-moment';
import {differenceInCalendarDays, format} from "date-fns";


const ChartAvailability = (props) => {


    let yLabelsEmployees;
    if (props.data === undefined) {
        props.triggerUp();
    } else {
        console.log(props.data)
        yLabelsEmployees = Array.from(props.data).map(a => a[0].identificator + " " + a[0].firstName + " " + a[0].lastName);
    }
    const yearLabels = [format(props.dateFrom, "MM-dd-yyyy"), format(props.dateTo, "MM-dd-yyyy")];


    const rectSize = 25;
    const rectSizeOnHover = rectSize - 0.2 * rectSize;
    const amountOfDays = Math.abs(differenceInCalendarDays(props.dateFrom, props.dateTo));
    const widthNumber = (amountOfDays * rectSize * 2) - 15;
    const widthString = widthNumber + "px";
    const heightNumber = yLabelsEmployees.length * rectSize * 2 + 80;
    const heightString = heightNumber + "px";
    const ratio = amountOfDays / heightNumber;

    const chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(231,233,237)'
    };
    const events = [
        {label: "Sick leave", event: "SL", backgroundColor: chartColors.red},
        {label: "Delegation", event: "DL", backgroundColor: chartColors.orange},
        {label: "Holiday", event: "HOL", backgroundColor: chartColors.yellow},
        {label: "Pickup overtime", event: "POT", backgroundColor: chartColors.green},
        {label: "Training", event: "T", backgroundColor: chartColors.blue},
        {label: "Overtime", event: "OT", backgroundColor: chartColors.purple},
        {label: "Home office", event: "HO", backgroundColor: chartColors.grey},
        {label: "Private leave", event: "PL", backgroundColor: chartColors.red},
        {label: "Work", event: "W", backgroundColor: chartColors.yellow},
    ]
    const compareDates = (date, dateFrom, dateTo) => {
        return date > dateFrom
            && date < dateTo;
    }

    const createDataset = (label, event, backgroundColor) => {
        const filtered = [];
        Array.from(props.data).forEach(d => d[1].forEach(e => {
            if (e.event === event) {
                filtered.push(d);
            }
        }));
        let data = [];
        filtered.forEach(f =>
            f[1].forEach(ff => {
                    if (compareDates(new Date(ff.year, ff.month - 1, ff.day), props.dateFrom, props.dateTo)) {
                        data.push({
                            x: ff.month + "-" + ff.day + "-" + ff.year,
                            y: f[0].identificator + " " + f[0].firstName + " " + f[0].lastName,
                        })
                    }
                }
            )
        )

        return {
            label: label,
            xAxisID: 'xAxis0',
            fill: false,
            showLine: false,
            data: data,
            backgroundColor: backgroundColor,
        }
    }

    const datasets = events.map(e => createDataset(e.label, e.event, e.backgroundColor))

    const cfg = {
        id: 'availability',
        type: 'line',
        data: {
            labels: yearLabels,
            datasets: datasets,
        },
        options: {
            plugins: {
                title: {
                    display: true,
                    text: 'Availability',
                },
                legend: {
                    position: 'bottom',
                },
            },
            scales: {
                xAxis0:
                    {
                        offset: true,
                        type: "time",
                        stacked: true,
                        distribution: 'series',
                        time: {
                            unit: 'day',
                            tooltipFormat: 'DD-MM-YYYY',
                            // minUnit: 'day',
                            // isoWeekday: true,
                        },
                        grid: {
                            display: true,
                            drawBorder: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                        },
                        ticks: {
                            display: true,
                            autoSkip: false,
                            align: 'center',
                            // source: 'data',
                        },
                    },
                xAxis2:
                    {
                        type: "time",
                        time: {
                            unit: 'month',
                            tooltipFormat: 'MM'
                        },
                        grid: {
                            lineWidth: 1.5,
                            tickWidth: 1.5,
                            display: false,
                            drawBorder: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                            offset: true,
                        },
                        ticks: {
                            display: true,
                            autoSkip: false,
                        }
                    },
                xAxis3:
                    {
                        type: "time",
                        time: {
                            unit: 'year',
                            tooltipFormat: 'YYYY'
                        },
                        grid: {
                            display: false,
                            drawBorder: true,
                            drawOnChartArea: true,
                            drawTicks: true,
                        },
                        ticks: {
                            display: true,
                            autoSkip: false,
                        },
                        offset: true,

                    },
                y: {
                    backdropColor: 'rgba(255, 255, 255, 0)',
                    type: 'category',
                    labels: yLabelsEmployees,
                    stacked: false,
                    offset: true,
                    grid: {
                        display: true,
                        drawBorder: true,
                        drawOnChartArea: true,
                        drawTicks: true,
                    },
                    ticks: {},
                }
            },
            elements: {
                point: {
                    pointStyle: 'rect',
                    radius: rectSize,
                    hoverRadius: rectSizeOnHover,
                },
            },
            responsive: true,
            maintainAspectRatio: false,
            aspectRatio: ratio,
        },
        animation: {
            onComplete: function () {
            }
        }
    }

    const ChartAreaWrapper = styled.div`
        height: ${heightString};
        width: ${widthString};
    `;

    const ChartWrapper = styled.div`
        position: relative;
        width: ${window.innerWidth - 35 + "px"};
        padding: 20px;
        overflow-x: scroll;
    `;

    return (
        <div>
            <ChartWrapper>
                <ChartAreaWrapper>
                    {props.data.length > 0 ?
                        <Line type={cfg.type} options={cfg.options} data={cfg.data}/>
                        : <div/>}

                </ChartAreaWrapper>
            </ChartWrapper>
        </div>

    )
}

export default ChartAvailability;
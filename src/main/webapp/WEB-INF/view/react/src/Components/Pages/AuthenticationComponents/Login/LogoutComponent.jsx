import AuthenticationService from 'Support/LoginService/AuthenticationService';
import React, { Component } from 'react'
import {Link} from "react-router-dom";
import "./Login.css"
import Footer from "../../../Footer/Footer";

class LogoutComponent extends Component {
    
    render() {
        AuthenticationService.logout();
        return (
                <div>
                    <div className="logout-container">
                        <h1>You have been logged out</h1>
                        <div>
                            Thank You for Using Our Application.

                        </div>
                        <Link to="/login">Login</Link>

                    </div>
                    <div className="login-image" />
                    <Footer/>
                </div>
        )
    }
}
export default LogoutComponent
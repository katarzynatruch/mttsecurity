import React, {useEffect, useState} from "react";
import {Input, InputLabel, TextField} from "@mui/material";
import PropTypes from "prop-types";
import "./TimeReport.css"

const Worklog = (props) => {

    const [hours, setHours] = useState(parseInt(props.workedHours));
    const [comment, setComment] = useState('');
    const [worklog, setWorklog] = useState({})
    const [trigger, setTrigger] = useState(0);
    const project = props.project;

    useEffect(() => {
        const updatedWorklog = {
            workedHours: hours,
            week: props.week,
            year: props.year,
            isConfirmedByEmployee: 0,
            isConfirmedByManager: 0,
            project: props.project,
            iterator: props.projectIterator,
        }
        props.sendDataToParent(updatedWorklog);

    }, [trigger])

    const triggerUp = () => {
        setTrigger(trigger + 1)
    }

    const textStyle = {
        width: 80,
        background: 'rgb(255,255,255)',
        borderRadius: 1,
    }

    return (
        project ?
            <div className="worklog-single-line">
                <div className="worklog-project-name">
                    Project {project.number}  {project.name}
                </div>
                <div>
                    <TextField
                        // variant="filled"
                        sx={{...textStyle}}
                        label="Hours"
                        type="number"
                        id="hours"
                        value={hours}
                        onChange={(event) => {
                            setHours(event.target.value);
                            triggerUp();
                        }}
                    />
                </div>
                {/*<div>*/}
                {/*    <InputLabel id="year">Comment</InputLabel>*/}
                {/*    <input type={"textArea"}*/}
                {/*           id={"comment"}*/}
                {/*           value={comment}*/}
                {/*           onChange={(event) => {*/}
                {/*               setComment(event.target.value);*/}
                {/*               triggerUp();*/}
                {/*           }}*/}
                {/*    />*/}
                {/*</div>*/}
            </div>
            : <div/>
    )

}
Worklog.propTypes = {
    hours: PropTypes.number,
};
export default Worklog;
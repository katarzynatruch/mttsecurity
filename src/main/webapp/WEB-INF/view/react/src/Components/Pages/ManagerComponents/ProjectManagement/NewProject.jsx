import React, {useEffect, useRef, useState} from "react";
import {useHistory, useLocation} from "react-router-dom";
import axios from "axios";
import {FormControl, IconButton, Input, InputLabel, inputLabelClasses, MenuItem, Select, Tooltip} from "@mui/material";
import "./NewProject.css"
import Deadline from "./Deadline";
import DeleteIcon from "@material-ui/icons/Delete";
import {useStateIfMounted} from "use-state-if-mounted";
import {TextField} from "@mui/material";
import AddRoundedIcon from '@mui/icons-material/AddRounded';
import RemoveRoundedIcon from '@mui/icons-material/RemoveRounded';
import newProject from "../../../../resources/new-project.png"


const NewProject = (props) => {

    const [trigger, setTrigger] = useStateIfMounted(0);
    let history = useHistory();
    const [data, setData] = useStateIfMounted([]);
    const [projectName, setProjectName] = useStateIfMounted('');
    const [projectNumber, setProjectNumber] = useStateIfMounted('');
    const [costCenters, setCostCenters] = useStateIfMounted([]);
    const [newCostCenter, setNewCostCenter] = useStateIfMounted([]);
    const [selectedCostCenter, setSelectedCostCenter] = useStateIfMounted('');
    const [maxHours, setMaxHours] = useStateIfMounted(0);
    const [costCenterPopUp, setCostCenterPopUp] = useStateIfMounted(false);
    const [selectedProjectType, setSelectedProjectType] = useStateIfMounted('');
    const [deadlines, setDeadlines] = useStateIfMounted([]);
    const [milestonesChosen, setMilestonesChosen] = useStateIfMounted([]);
    const [milestonesLeft, setMilestonesLeft] = useStateIfMounted([]);
    const [anyError, setAnyError] = useStateIfMounted(true)
    const [errorMessage, setErrorMessage] = useStateIfMounted('');
    const [projectNumberDisabled, setProjectNumberDisabled] = useStateIfMounted(false)
    let location = useLocation();



    useEffect(() => {
        loadDataToNewProject();
        if (location?.state) {
            setProjectNumberDisabled(true);
            setProjectName(location.state.projectName);
            setProjectNumber(location.state.projectNumber);
            setSelectedCostCenter(location.state.selectedCostCenter);
            setMaxHours(location.state.maxHours);
            setSelectedProjectType(location.state.selectedProjectType);
            setDeadlines(location.state.deadlines);
            setMilestonesChosen(location.state.milestonesChosen);
            setMilestonesLeft(location.state.milestonesLeft);
        }
        // return cleanupSub();
    }, [trigger])

    const cleanupSub = () => {
        setData([]);
        setCostCenters([])
        setNewCostCenter([])
        setDeadlines([])
        setMilestonesChosen([])
        setMilestonesLeft([])
    }

    const triggerUp = () => {
        setTrigger(trigger + 1);
    }

    function saveProject() {
        axios.post(`http://localhost:8080/new-project/add`,
            {
                name: projectName,
                number: projectNumber,
                costCenter: selectedCostCenter,
                maxHours: maxHours,
                type: selectedProjectType,
                deadlines: deadlines
            })
            .then(() => {
                // window.location.reload();
                history.push("/project-management")
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                } else {
                }
            })
    }

    function saveEditedProject() {
        axios.post(`http://localhost:8080/new-project/override`,
            {
                name: projectName,
                number: projectNumber,
                costCenter: selectedCostCenter,
                maxHours: maxHours,
                type: selectedProjectType,
                deadlines: deadlines
            })
            .then(() => {
                if (location.state)
                    history.push("/project-management")
                window.location.reload();
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                } else {
                }
            })
    }

    const validation = () => {
        if (projectNumber?.length > 6) {
            setAnyError(true);
            setErrorMessage("Project number must have min. 6 characters");
        } else if (projectNumber === '') {
            setAnyError(true);
            setErrorMessage("All fields must be fulfilled");
        } else if (selectedCostCenter === '') {
            setAnyError(true);
            setErrorMessage("All fields must be fulfilled");
        } else if (maxHours < 1) {
            setAnyError(true);
            setErrorMessage("All fields must be fulfilled");
        } else if (selectedProjectType === '') {
            setAnyError(true);
            setErrorMessage("All fields must be fulfilled");
        } else {
            setAnyError(false);
            setErrorMessage("")
            if (location.state) {
                saveEditedProject()
            } else {
                saveProject();
            }
        }
    }
    const loadDataToNewProject = () => {
        axios.get('http://localhost:8080/new-project/loadData')
            .then(response => response.data)
            .then(data => {
                setData(data);
                if (data.costCenters) {
                    setCostCenters(data.costCenters);
                }
                if (!location.state)
                    setMilestonesLeft(data.milestones)
            })
    }

    const handleSelectedCostCenter = (event) => {
        setSelectedCostCenter(event.target.value);
    }
    const handleSelectedType = (event) => {
        setSelectedProjectType(event.target.value);
    }
    const handleInputKeyDown = (event) => {
        if (event.key === 'Enter') {
            if (!costCenters.filter(cc => cc === event.target.value).length > 0)
                setCostCenters([...costCenters, event.target.value]);
        }
    }

    const handleAddCostCenter = (event) => {
        if (!costCenters.filter(cc => cc === newCostCenter).length > 0)
            setCostCenters([...costCenters, newCostCenter]);
    }

    function checkIfExists(deadline) {
        return deadlines.filter(d => d.milestone === deadline.milestone).length > 0
    }

    const handleDeadlines = (deadline) => {
        if (checkIfExists(deadline)) {
            const tempDeadlines = deadlines.filter(d => d.milestone !== deadline.milestone);
            tempDeadlines.push(deadline);
            setDeadlines(tempDeadlines);
        } else {
            setDeadlines([...deadlines, deadline]);
        }
    }

    const handleSelectedMilestone = (event) => {
        setMilestonesChosen([...milestonesChosen, event.target.value]);
        const milestonesFiltered = milestonesLeft.filter(m => m !== event.target.value);
        setMilestonesLeft(milestonesFiltered);
    }

    const handleSaveProjectClick = () => {
        validation();
    }

    const handleDelete = (milestone) => {
        const tempMilestonesChosen = milestonesChosen.filter(mc => mc !== milestone);
        setMilestonesChosen(tempMilestonesChosen);
        setMilestonesLeft([...milestonesLeft, milestone]);
        const tempDeadlines = deadlines.filter(d => d.milestone !== milestone);
        setDeadlines(tempDeadlines);
    }

    const textStyle = {
        maxWidth: 350,
        // width: 300,
        minWidth: 120,
        background: 'rgb(255,255,255)',
        borderRadius: 1,
        color: '#292929',
    }

    const selectStyle = {
        maxWidth: 200,
        // width: 180,
        minWidth: 140,
        background: 'rgb(255,255,255)',
        borderRadius: 1,
        color: '#292929',
    }

//     const StyledTextField = styled(TextField)(`
//   .${inputLabelClasses.root} {
//     font-size: 20px;
//     &.${inputLabelClasses.focused} {
//       color: black;
//     }
//   }
// `);


    return (
        data && data.milestones && data.types ?
            <div className="new-project-form-container">
                <img src={newProject} alt="img" className="new-project-image"/>
                <h2 style={{color: "darkgray"}}>Add project</h2>
                <div className="new-project-form">
                    <div className="new-project-error">{anyError ? errorMessage : <div/>}</div>
                    <InputLabel style={{fontSize: 15, marginLeft: 30}}>Project number</InputLabel>
                    <div className="new-project-single-item">
                        <TextField sx={{...textStyle}}
                                   type="text"
                                   placeholder="Project number"
                                   value={projectNumber}
                                   onChange={(event) => setProjectNumber(event.target.value)}
                                   disabled={projectNumberDisabled}
                        />
                    </div>
                    <InputLabel style={{fontSize: 15, marginLeft: 30}}>Project name</InputLabel>
                    <div className="new-project-single-item">
                        <TextField sx={{...textStyle}}
                                   placeholder="Project name"
                                   type="text"
                                   value={projectName}
                                   onChange={(event) => setProjectName(event.target.value)}/>
                    </div>
                    <div className="new-project-single-item">
                        <InputLabel style={{fontSize: 15, marginLeft: 15}}>Cost center</InputLabel>
                        <div className="new-project-row-flex">
                            <Select sx={{...selectStyle}}
                                    labelId="costCenter"
                                    id="cost"
                                    label="Cost center"
                                    onChange={handleSelectedCostCenter}
                                    value={selectedCostCenter}
                                    autoWidth
                                    defaultValue=""
                                    placeholder={costCenters.length > 0 ? "Select" : "No options available"}
                            >
                                {costCenters.length > 0 ?
                                    costCenters.map(cc => (
                                        <MenuItem key={cc} value={cc}>{cc}</MenuItem>
                                    ))
                                    :
                                    <MenuItem key={"No cost center yet"} value={"No cost center yet"} disabled>No cost
                                        center yet</MenuItem>
                                }
                            </Select>
                            <Tooltip title={"Add new"}
                                     onClick={() => setCostCenterPopUp(!costCenterPopUp)}
                                     style={{height: 40, backgroundColor: "rgba(255, 255, 255, 0.5)"}}>
                                <IconButton aria-label="add">
                                    {costCenterPopUp ? <RemoveRoundedIcon/> : <AddRoundedIcon/>}
                                </IconButton>
                            </Tooltip>
                            {costCenterPopUp ?
                                <div style={{marginLeft: 15}}>
                                    <Tooltip title={"Enter to confirm"}>
                                        <TextField sx={{...textStyle, width: 120}}
                                                   label="New cost center"
                                                   placeholder="New cost center"
                                                   type="text"
                                                   value={newCostCenter}
                                                   onChange={(event) => setNewCostCenter(event.target.value)}
                                                   onKeyDown={handleInputKeyDown}
                                        />
                                    </Tooltip>
                                    <button onClick={(event) => handleAddCostCenter(event)}
                                            className="add-button">Add
                                    </button>
                                </div>
                                : <div/>
                            }
                        </div>
                    </div>
                    <InputLabel style={{fontSize: 15, marginLeft: 30}}>Hours Limitation</InputLabel>
                    <div className="new-project-single-item">
                        <TextField sx={{...textStyle}}
                                   type="number"
                                   placeholder="Hours Limitation"
                                   value={maxHours}
                                   onChange={(event) => setMaxHours(event.target.value)}/>
                    </div>
                    <InputLabel style={{fontSize: 15, marginLeft: 30}} id="ptype">Project type</InputLabel>
                    <div className="new-project-single-item">
                        <Select
                            sx={{...selectStyle}}
                            labelId="type"
                            id="ptype"
                            label="Project type"
                            onChange={handleSelectedType}
                            value={selectedProjectType}
                            autoWidth
                            defaultValue=""
                        >
                            {Array.from(data.types).map(type => (
                                <MenuItem key={type} value={type}>{type}</MenuItem>
                            ))}
                        </Select>
                    </div>
                    <InputLabel style={{fontSize: 15, marginLeft: 30}} id="deadlines">Deadlines</InputLabel>
                    <div className="deadline-frame">
                        <div className="new-project-single-item" style={{marginTop: 15}}>
                            <Select
                                sx={{...selectStyle}}
                                labelId="deadlines"
                                id="deadline"
                                label="Deadline"
                                placeholder="Choose milestone"
                                onChange={handleSelectedMilestone}
                                autoWidth
                                defaultValue=""
                            >
                                {Array.from(milestonesLeft).map(milestone => (
                                    <MenuItem key={milestone} value={milestone}>{milestone}</MenuItem>
                                ))}
                            </Select>
                        </div>
                        {milestonesChosen ?
                            Array.from(milestonesChosen).map(m => (
                                <div className="new-project-single-item">
                                    <div className="new-project-row-flex-nowrap">
                                        <Deadline milestone={m} deadlines={deadlines} triggerUp={triggerUp}
                                                  handleDeadlines={handleDeadlines}
                                                  setErrorMessage={setErrorMessage} setAnyError={setAnyError}
                                        />
                                        <Tooltip title="Delete" onClick={() => handleDelete(m)}>
                                            <IconButton aria-label="delete">
                                                <DeleteIcon/>
                                            </IconButton>
                                        </Tooltip>
                                    </div>
                                </div>

                            ))
                            : <div/>}
                    </div>
                    <div>
                        <button className="time-button" onClick={handleSaveProjectClick}
                                disabled={errorMessage.length > 0}>Save project
                        </button>
                    </div>
                </div>
            </div>
            : <div/>


    )
}
export default NewProject;
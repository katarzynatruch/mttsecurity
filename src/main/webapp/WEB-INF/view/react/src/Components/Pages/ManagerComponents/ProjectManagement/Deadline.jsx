import {InputLabel, MenuItem, Select} from "@material-ui/core";
import React, {useEffect, useRef, useState} from "react";
import "./NewProject.css"
import {TextField} from "@mui/material";
import {useStateIfMounted} from "use-state-if-mounted";

const Deadline = (props) => {

    const [weekNumber, setWeekNumber] = useStateIfMounted(0);
    const [yearNumber, setYearNumber] = useStateIfMounted(0);
    const [deadline, setDeadline] = useStateIfMounted({});
    const [comment, setComment] = useStateIfMounted('');
    const [trigger, setTrigger] = useStateIfMounted(0);
    const [saveDisabled, setSaveDisabled] = useStateIfMounted(false);
    const [shouldCheckIfDeadlineExists, setShouldCheckIfDeadlineExists] = useStateIfMounted(true);
    const milestone = props.milestone;

    let startTimerInterval = useRef();
    if (trigger < 1) {
        startTimerInterval.current = setTimeout(() => {
            setTrigger(trigger + 1)
        }, (2000));
    }

    const checkIfDeadlineIsTheSame = () => {
        const deadline = props.deadlines.filter(d => d.milestone === milestone)[0]
        if(deadline){
            if (deadline.milestone === milestone) {
                if (weekNumber === deadline.week) {

                    if (yearNumber === deadline.year) {
                        setSaveDisabled(true);
                    } else {
                        setSaveDisabled(false);
                    }
                } else {
                    setSaveDisabled(false);
                }
            } else {
                setSaveDisabled(false);
            }
        }
    }

    useEffect(() => {
        if (props.deadlines && shouldCheckIfDeadlineExists) {
            const deadline = props.deadlines.filter(d => d.milestone === milestone)[0]
            if (deadline) {
                setWeekNumber(deadline.week)
                setYearNumber(deadline.year)
                setComment(deadline.comment)
            }
            setShouldCheckIfDeadlineExists(false);
        }
        setDeadline({
            milestone: milestone,
            week: weekNumber,
            year: yearNumber,
            comment: comment
        })
        checkIfDeadlineIsTheSame();

        return cleanupSub();
    }, [trigger])

    const cleanupSub = () => {

    }


    const triggerUp = () => {
        setTrigger(trigger + 1)
    }

    const validation = () => {
        if (weekNumber > 53) {
            props.setAnyError(true);
            props.setErrorMessage("Year have only 53 weeks")
        } else if (weekNumber < 1) {
            props.setAnyError(true);
            props.setErrorMessage("Min. week is 1")
        } else if (yearNumber < 2022) {
            props.setAnyError(true);
            props.setErrorMessage("You cannot create project in the past. Min year is 2022")
        } else if (yearNumber > 2035) {
            props.setAnyError(true);
            props.setErrorMessage("Max year is 2035")
        } else {
            props.setAnyError(false);
            props.setErrorMessage("")
        }
        checkIfDeadlineIsTheSame();
    }
    const textStyle = {
        maxWidth: 350,
        // width: 300,
        minWidth: 120,
        background: 'rgb(255,255,255)',
        borderRadius: 1,
        color: '#292929',
    }

    const selectStyle = {
        maxWidth: 200,
        // width: 180,
        minWidth: 140,
        background: 'rgb(255,255,255)',
        borderRadius: 1,
        color: '#292929',
    }

    return (
        milestone ?
            <div className="new-project-row-flex-nowrap">
                <div className="new-project-row-flex">
                    <div className="milestone">{milestone}</div>
                    <TextField sx={{...textStyle, width: 100}}
                               label="Week"
                               placeholder={"Week"}
                               type={"number"}
                               id={"week"}
                               value={weekNumber}
                               onChange={(event) => {
                                   setWeekNumber(event.target.value);
                                   triggerUp();
                               }}
                    />
                    <TextField sx={{...textStyle, width: 100}}
                               label="Year"
                               type={"number"}
                               placeholder={"Year"}
                               id={"year"}
                               value={yearNumber}
                               onChange={(event) => {
                                   setYearNumber(event.target.value);
                                   triggerUp();
                               }}
                    />
                </div>

                {/*<InputLabel id="year">Comment</InputLabel>*/}
                {/*<input type={"textArea"}*/}
                {/*       id={"comment"}*/}
                {/*       value={comment}*/}
                {/*       onChange={(event) => {*/}
                {/*           setComment(event.target.value);*/}
                {/*           triggerUp();*/}
                {/*       }}*/}
                {/*/>*/}
                <button className="time-button"
                        onClick={() => {
                            props.handleDeadlines(deadline);
                            validation();
                            setTrigger(0)
                        }}
                        disabled={saveDisabled}
                >
                    Save
                </button>
            </div>
            : <div/>
    )
}
export default Deadline;
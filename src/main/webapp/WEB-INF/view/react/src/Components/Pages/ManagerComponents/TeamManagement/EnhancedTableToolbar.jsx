import {IconButton, lighten, makeStyles, Toolbar, Tooltip, Typography} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import AddBoxIcon from '@material-ui/icons/AddBox';
import axios from 'axios';
import clsx from 'clsx';
import AuthenticationService from 'Support/LoginService/AuthenticationService';

import PropTypes from 'prop-types';
import React, {useEffect, useRef, useState} from 'react';
import {useHistory} from 'react-router-dom';

const EnhancedTableToolbar = (props) => {
    const classes = useToolbarStyles();
    const {numSelected} = props;
    let history = useHistory();
    const [trigger, setTrigger] = useState(0);
    const [isAddTeamVisible, setIsAddTeamVisible] = useState(false);
    const [newTeamName, setNewTeamName] = useState('')

    let startTimerInterval = useRef();
    console.log(trigger)
    if(trigger<1){
        console.log("timeout")
        startTimerInterval.current = setTimeout(() => {
            setTrigger(trigger+1)
            props.triggerUp();
        }, (1000));
    }

    useEffect(() => {
    }, [trigger])

    const triggerUp = () => {
        setTrigger(trigger + 1)
    }

    function handleDeleteSelectedEmployees() {
        const employeeTeamUpdateDsList = [];
        const notSelectedList = Array.from(props.notSelected)
        props.teamsData.forEach(teamData => {
            const employeesByTeam = notSelectedList.filter(employee => employee.team.name === teamData.team.name)
            const employees = employeesByTeam.map(employee => employee.identificator)
            const singleEmployeeTeamUpdateDts = {
                team: teamData.team,
                employeeIdentificatorList: employees,
                managerIdentificator: AuthenticationService.getLoggedInUserName()
            }
            employeeTeamUpdateDsList.push(singleEmployeeTeamUpdateDts);
        })
        saveUpdatedTeam(employeeTeamUpdateDsList);
        props.triggerUp();
        props.updateSelected();
    }

    const saveUpdatedTeam = (employeeTeamUpdateDsList) => {
        axios.post('http://localhost:8080/team/manageTeams', {employeeTeamUpdateDsList})
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
        setTrigger(0);
    }

    const handleAddTeamButton = (event) => {
        setIsAddTeamVisible(!isAddTeamVisible);
    }

    function saveTeam() {
        const employeeTeamUpdateDsList = [];
        props.teamsData.forEach(teamData => {
            const employees = teamData.employees.map(employee => employee.identificator)
            const singleEmployeeTeamUpdateDts = {
                team: teamData.team,
                employeeIdentificatorList: employees,
                managerIdentificator: AuthenticationService.getLoggedInUserName()
            }
            employeeTeamUpdateDsList.push(singleEmployeeTeamUpdateDts);
        })
        employeeTeamUpdateDsList.push(
            {
                team: {name: newTeamName},
                employeeIdentificatorList: [],
                managerIdentificator: AuthenticationService.getLoggedInUserName()
            }
        )
        saveUpdatedTeam(employeeTeamUpdateDsList);
        setIsAddTeamVisible(false)
        // props.triggerUp();
        // triggerUp();
    }

    const handleSaveAddTeamButton = (event) => {
        if (props.teamsData) {
            let teamExists = Array.from(props.teamsData).filter(td => td.team.name === newTeamName).length > 0;
            if(!teamExists){
                saveTeam();
            }
        }else{
            saveTeam();
        }
    }

    return (
        <Toolbar
            className={clsx(classes.root, {
                [classes.highlight]: numSelected > 0,
            })}
        >
            {numSelected > 0 ? (
                <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
                    {numSelected} selected
                </Typography>
            ) : (
                <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                    Team management
                </Typography>
            )}

            {numSelected > 0 ? (
                <Tooltip title="Delete" onClick={handleDeleteSelectedEmployees}>
                    <IconButton aria-label="delete">
                        <DeleteIcon/>
                    </IconButton>
                </Tooltip>
            ) : (
                <Tooltip title="Click to create a new team" onClick={handleAddTeamButton}>
                    <IconButton aria-label="add team">
                        Add new team <div style={{width: 30}}></div>
                        <AddBoxIcon/>
                    </IconButton>
                </Tooltip>
            )}
            {isAddTeamVisible ? (
                <>
                    <div>
                        <input type="textArea" placeholder="Type a team name" name="teamName"
                               value={newTeamName} onChange={(event) => setNewTeamName(event.target.value)}>
                        </input>
                        <button className="save-team-button" type="submit" onClick={handleSaveAddTeamButton}>Save
                        </button>
                    </div>
                </>
            ) : <div></div>}
        </Toolbar>
    );
};

const useToolbarStyles = makeStyles((theme) => ({
    root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1),
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    title: {
        flex: '1 1 100%',
    },
}));

EnhancedTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
};


export default EnhancedTableToolbar;
import {useEffect, useMemo, useRef, useState} from "react";
import axios from "axios";
import React from "react";
import DiagramsFilterComponent from "Components/Pages/ManagerComponents/DiagramFilters/DiagramsFilterComponent";
import ChartCapacity from "./ChartCapacity";
import '../../../../index.css'
import {useHistory} from "react-router-dom";
import {useStateIfMounted} from "use-state-if-mounted";
import "../Chart.css"

const CapacityDiagramComponent = () => {

    const [trigger, setTrigger] = useStateIfMounted(0);
    const [diagramData, setDiagramData] = useStateIfMounted([]);
    const [timeInterval, setTimeInterval] = useStateIfMounted({});
    let history = useHistory();

    // useEffect(() => {
    //     // return cleanupSub();
    // }, [trigger])


    const cleanupSub = () => {
        setDiagramData([]);
        setTimeInterval({})
    }

    const handleGenerateDiag = (selectedEmployees, selectedProjects, timeInterval) => {
        setTrigger(trigger + 1)
        if (selectedEmployees.length < 1) {
            // throw new Error("No employees selected!")
            setDiagramData([])
        }
        if (selectedProjects.length < 1) {
            // throw new Error("No projects selected!")
            setDiagramData([])
        }
        const capacityRetrieveDts = {
            employeeIdentificators: selectedEmployees,
            projectNumbers: selectedProjects,
        }
        // console.log("Capacity data: \n" + selectedEmployees + "\n \n " +  selectedProjects)
        if (selectedEmployees.length > 0 && selectedProjects.length > 0) {
            setTimeInterval(timeInterval)

            axios.post(`http://localhost:8080/statistics/capacityGraph`, capacityRetrieveDts)
                .then(response => response.data)
                .then(data => {
                    setDiagramData(data)
                })
                .catch((error) => {
                    if (error.response.status === 401) {
                        history.push("/logout=true")
                    }
                })
        }
        setTrigger(trigger + 1)
    }
    const triggerUp = () => {
        setTrigger(trigger + 1);
    }
    return (
        <div className="availability-container">
            <h2>Capacity diagram</h2>
            <DiagramsFilterComponent filterData={handleGenerateDiag} triggerUp={triggerUp} shouldBeDisplayed={false}/>
            {diagramData.capacityOfEmployees ?
                <ChartCapacity data={diagramData} triggerUp={triggerUp} timeInterval={timeInterval}/>
                : <div/>}
        </div>
    )
}

export default CapacityDiagramComponent;
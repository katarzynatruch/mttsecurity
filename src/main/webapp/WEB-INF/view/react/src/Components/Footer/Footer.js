import './Footer.css'
import logo4 from "../../resources/logo4.png"
import {Link} from "react-router-dom";

const Footer = () => {

    return (
        <div className="footer-container">
            <div className="block-container">
                <img className="logo1" src={logo4} alt={'not show up'}/>
            </div>
            <div className="separator"/>
            <div className="map-container">
                <Link className="link-r" to="/about">
                    <div className="map-item">
                        ABOUT
                    </div>
                </Link>
                <Link className="link-r" to="/support">
                    <div className="map-item">
                        SUPPORT
                    </div>
                </Link>
                <Link className="link-r" to="/homepage">
                    <div className="map-item">
                        HOME
                    </div>
                </Link>
                <Link className="link-r" to="/">
                    <div className="map-item">
                        MYTEAMSTIME@GMAIL.COM
                    </div>
                </Link>
            </div>
        </div>
    )
}
export default Footer;
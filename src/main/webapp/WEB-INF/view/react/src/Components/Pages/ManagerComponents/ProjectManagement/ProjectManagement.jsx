import React, {useEffect, useState} from "react";
import {Link, useHistory} from "react-router-dom";
import axios from "axios";
import {IconButton, Tooltip} from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import AddCardRoundedIcon from '@mui/icons-material/AddCardRounded';
import "./ProjectManagement.css"
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, {tableCellClasses} from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {styled} from '@mui/material/styles';
import {useStateIfMounted} from "use-state-if-mounted";

const ProjectManagement = () => {

    const [trigger, setTrigger] = useStateIfMounted(0);
    let history = useHistory();
    const [data, setData] = useStateIfMounted([]);
    const [allMilestones, setAllMilestones] = useStateIfMounted([]);

    useEffect(() => {
        loadDataOfProjectList();
        return cleanupSub();
    }, [trigger])

    const cleanupSub = () => {
        setAllMilestones([]);
        setData([]);
    }

    const loadDataOfProjectList = () => {
        axios.get('http://localhost:8080/project/plist')
            .then(response => response.data)
            .then(data => {
                setData(data);
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                } else {
                }
            })
        axios.get('http://localhost:8080/new-project/loadData')
            .then(response => response.data)
            .then(data => {
                setAllMilestones(data.milestones);
            })
    }

    function getMilestonesChosen(deadlines) {
        return deadlines.map(deadline => deadline.milestone)
    }

    function getMilestonesLeft(milestones, projectDeadlines) {
        const milestonesChoosen = getMilestonesChosen(projectDeadlines);
        return milestones.filter(milestone => !checkIfMilestonesChoosenContains(milestone, milestonesChoosen));
    }

    function checkIfMilestonesChoosenContains(milestone, milestonesChoosen) {
        return milestonesChoosen.filter(mc => mc === milestone).length > 0;
    }

    const StyledTableCell = styled(TableCell)(({theme}) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: "#5f7375",
            color: theme.palette.common.white,
            fontSize: 15,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({theme}) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: 'rgba(200, 200, 200, 0.5)',

        },
        '&:nth-of-type(even)': {
            backgroundColor: 'rgba(225, 225, 225, 0.5)',
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));

    return (
        data.projects ?
            <div className="p-m-container">
                <Link className="link-p-management" to="/project-add">
                    <div className="add-p-container">
                        <AddCardRoundedIcon/>
                        <div>New project</div>
                    </div>
                </Link>
                <div className="p-list-container">
                    <TableContainer component={Paper} sx={{minWidth: 300, maxWidth: 800}}>
                        <Table sx={{minWidth: 300}} aria-label="simple table">
                            <TableHead>
                                <StyledTableRow>
                                    <StyledTableCell>Project name</StyledTableCell>
                                    <StyledTableCell align="center">Project number</StyledTableCell>
                                    <StyledTableCell align="center">Project type</StyledTableCell>
                                    <StyledTableCell align="center">Cost center</StyledTableCell>
                                    <StyledTableCell align="center">Edit</StyledTableCell>
                                </StyledTableRow>
                            </TableHead>
                            <TableBody>
                                {Array.from(data.projects).map(project => (
                                    <StyledTableRow
                                        key={project.name}
                                        sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                    >
                                        <StyledTableCell component="th" scope="row">
                                            {project.name}
                                        </StyledTableCell>
                                        <StyledTableCell align="center">{project.number}</StyledTableCell>
                                        <StyledTableCell align="center">{project.type}</StyledTableCell>
                                        <StyledTableCell align="center">{project.costCenter}</StyledTableCell>
                                        <StyledTableCell align="center">
                                            <Link to={{
                                                pathname: "/project-add",
                                                state: {
                                                    projectName: project.name,
                                                    projectNumber: project.number,
                                                    selectedCostCenter: project.costCenter,
                                                    maxHours: project.maxHours,
                                                    selectedProjectType: project.type,
                                                    deadlines: project.deadlines,
                                                    milestonesChosen: getMilestonesChosen(project.deadlines),
                                                    milestonesLeft: getMilestonesLeft(allMilestones, project.deadlines)
                                                }
                                            }}
                                            >
                                                <Tooltip title="Edit">
                                                    <IconButton aria-label="edit">
                                                        <EditIcon/>
                                                    </IconButton>
                                                </Tooltip>
                                            </Link>
                                        </StyledTableCell>
                                    </StyledTableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>
            </div>
            : <div/>
    )

}
export default ProjectManagement;
import "../ManagerComponents/ToolsMenu.css"
import {Link} from "react-router-dom";
import timeReportIcon from "../../../resources/time-report-icon.png";
import availIcon from "../../../resources/availavility-diagram-icon.png";

const TimeReportMenu = () => {

    return(
        <div className="t-container">
            <div>
                <h2>Time report:</h2>
                <Link className="link" to="/time-report">
                    <div className="t-item" style={{backgroundColor: "var(--color-primary)"}}>
                        <div>Weekly report</div>
                        <img className="icon-image" src={timeReportIcon} alt=""/>
                    </div>
                </Link>
                <Link className="link" to="/">
                    <div className="t-item" style={{backgroundColor: "var(--color-primary-light)"}}>
                        <div>Availability</div>
                        <img className="icon-image" src={availIcon} alt=""/>
                    </div>
                </Link>
            </div>
        </div>
    )
}
export default TimeReportMenu;
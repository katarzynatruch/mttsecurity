// import 'semantic-ui-react'
import select from "bootstrap-select"
import {Multiselect} from "multiselect-react-dropdown";
import "./Filters.css"


import React, {useEffect, useRef, useState} from "react";
import {useStateIfMounted} from "use-state-if-mounted";
import {Checkbox} from "@mui/material";

const MultiselectForTeam = (props) => {

    const [allTeam, setAllTeam] = useStateIfMounted([]);
    const [selected, setSelected] = useStateIfMounted([]);
    const [trigger, setTrigger] = useStateIfMounted(0);
    const [options, setOptions] = useStateIfMounted([]);
    const multiselectRef = useRef();

    let startTimerInterval = useRef();
    if(trigger<1){
        startTimerInterval.current = setTimeout(() => {
            setTrigger(trigger+1)
        }, (2000));
    }

    useEffect(() => {
        loadData();
        // if (options.length < 1) {
        //     setTrigger(trigger + 1)
        // }
        sendDataToParent();
        // return cleanupSub();
    }, [trigger])

    const cleanupSub = () => {
        setAllTeam([]);
        setSelected([]);
        setOptions([]);
    }

    const loadData = () => {
        const tempOptions = []
        Array.from(props.team).map(team =>
            Array.from(team.employees).map(employee => tempOptions.push(createEmployeeWithTeamOption(employee, team)))
        )
        setOptions(tempOptions);
    }
    const createEmployeeWithTeamOption = (employee, team) => {
        return {
            name: employee.firstName + " " + employee.lastName + " " + employee.identificator,
            identificator: employee.identificator,
            team: team.team,
        }
    }


    const onChangeAllTeam = (event) => {
        const isChecked = event.target.checked;
        if (isChecked) {
            setAllTeam([...allTeam, event.target.value])
            updateAddSelection(event.target.value);
        } else {
            const selectedTeams = allTeam.filter(item => item !== event.target.value)
            setAllTeam(selectedTeams);
            updateRemoveSelection(event.target.value);
        }
        // props.triggerUp();
        setTrigger(trigger + 1)
    }

    const updateTeamsRemoveSelection = (uncheckedEmployee) => {
        allTeam.forEach(checkTeam => {
            Array.from(props.team).forEach(team => {
                if (checkTeam === team.team) {
                    if (checkIfAllTeamEmployeesAreSelected(team.employees, uncheckedEmployee)) {
                        const selectedTeams = allTeam.filter(item => item !== checkTeam)
                        setAllTeam(selectedTeams);
                    }
                }
            })
        })
    }

    const updateTeamsAddSelection = (checkedEmployee) => {
        const uncheckedTeams = Array.from(props.team).filter(team => !checkIfTeamIsInAllTeamState(team))
        uncheckedTeams.forEach(uncheckedTeam =>
            updateTeamChecked(uncheckedTeam, checkedEmployee)
        )
    }

    function checkIfAllTeamEmployeesAreSelected(employees, uncheckedEmployee) {
        const uncheckedEmployeeInTeam = Array.from(employees).filter(employee => employee.identificator === uncheckedEmployee.identificator)
        if (uncheckedEmployeeInTeam.length > 0) {
            return true
        }
        return false;
    }


    const updateAddSelection = (currentTeam) => {
        const currentTeamEmployees = options.filter(item => item.team === currentTeam)
        const notYetSelectedEmployees = currentTeamEmployees
            .filter(item => checkIfSelectedTeamEmployeesAreSelected(selected, item))
        const combinedEmployees = [];
        selected.map(item => combinedEmployees.push(item))
        notYetSelectedEmployees.map(item => combinedEmployees.push(item))
        setSelected(combinedEmployees);
    }
    const updateRemoveSelection = (currentTeam) => {
        const currentTeamEmployees = options.filter(item => item.team === currentTeam)
        const selectedEmployeesToBeRemoved = currentTeamEmployees
            .filter(employee => checkIfSelectedTeamEmployeesShouldBeRemoved(employee, currentTeam))
        const shouldBeSelectedEmployees = selected.filter(employee => !checkIfContainsAny(selectedEmployeesToBeRemoved, employee))
        setSelected(shouldBeSelectedEmployees);
    }

    const onSelect = (selectedList, selectedItem) => {
        updateTeamsAddSelection(selectedItem);
        setSelected(selectedList);
        // props.triggerUp();
        setTrigger(trigger + 1)
    }
    const onRemove = (selectedList, removedItem) => {
        updateTeamsRemoveSelection(removedItem);
        setSelected(selectedList);
        // props.triggerUp();
        setTrigger(trigger + 1)
    }
    const handleResetClick = (event) => {
        setSelected([]);
        setAllTeam([]);
        setTrigger(trigger+1)
        // props.triggerUp();
    }

    const sendDataToParent = () => {
        const employeesIdentificators = selected.map(selected => selected.identificator)
        props.employeesData(employeesIdentificators);
        // props.triggerUp();
    }

    function checkIfContainsAny(employeeList, employee) {
        const arrayOfBooleans = employeeList.map(item => item.name === employee.name)
        const arrayOfTrue = arrayOfBooleans.filter(b => b === true)
        if (arrayOfTrue.length > 0) {
            return true;
        }
        return false;
    }

    function checkIfSelectedTeamEmployeesShouldBeRemoved(employee, currentTeam) {
        return selected.filter(selectedEmployee => selectedEmployee.name === employee.name
            && chceckIfEmployeeIsNotInAnotherCheckedTeam(employee, currentTeam)).length > 0;
    }

    function checkIfSelectedTeamEmployeesAreSelected(selected, item) {
        if (selected.length < 1) {
            return true;
        }
        return !checkIfContainsAny(selected, item)
    }

    function chceckIfEmployeeIsNotInAnotherCheckedTeam(employee, currentTeam) {
        return allTeam.filter(team => team !== currentTeam).filter(team => checkIfEmployeeIsInTheTeam(team, employee)).length === 0;
    }

    function checkIfEmployeeIsInTheTeam(team, employee) {
        return props.team.filter(propsTeam => propsTeam.team === team && checkIfEmployeeIsInTheList(propsTeam.employees, employee)).length > 0;
    }

    function checkIfEmployeeIsInTheList(employees, employee) {
        return employees.filter(employeeFromList => employeeFromList.name === employee.name).length > 0;
    }

    function checkIfTeamIsInAllTeamState(renderedTeam) {
        const checkingTeams = allTeam.filter(team => team === renderedTeam.team)
        if (checkingTeams.length > 0) {
            return true;
        }
        return false;
    }

    function chceckIfSelectedEmployeesCoverUncheckedTeamEmployees(employees, checkedEmployee) {
        const updatedSelectedList = selected;
        updatedSelectedList.push(checkedEmployee);
        return employees.filter(employee => checkIfEmployeeIsInTheList(updatedSelectedList, employee)).length === employees.length;
    }

    function updateTeamChecked(uncheckedTeam, checkedEmployee) {
        if (chceckIfSelectedEmployeesCoverUncheckedTeamEmployees(uncheckedTeam.employees, checkedEmployee)) {
            setAllTeam([...allTeam, uncheckedTeam.team]);
        }
    }

    const multiselectStyle = {
        multiselectContainer: {
            fontSize: 15,
            display: "flex",

        },
        searchBox: {
            minHeight: 40,
            width: "-webkit-fill-available",
            minWidth: 220,

        },
        inputField: {
            // margin: 12,
            // display : "none",
        },
        chips: {backgroundColor: "#85a2a6",},
        optionContainer: {},
        option: {
            whitespace: "nowrap",
        },
    }

    return (
        <div className="multiselect-container">
            <div>
                {Array.from(props.team).map(team => (
                    <div style={{display: "flex", flexWrap: "nowrap", alignItems:"center"}}>
                        <Checkbox sx={{color: "#85a2a6",
                            '&.Mui-checked': {
                                color: "#85a2a6",
                            },}}
                                  checked={checkIfTeamIsInAllTeamState(team)}
                                  type="checkbox"
                                  onChange={onChangeAllTeam}
                                  value={team.team}
                        />
                        <label style={{whiteSpace: "nowrap"}}>All team {team.team}</label>
                    </div>
                ))}
            </div>
            {options ?
                <Multiselect
                    style={multiselectStyle}
                    displayValue="name"
                    groupBy="team"
                    onRemove={onRemove}
                    onSearch={function noRefCheck() {
                    }}
                    onSelect={onSelect}
                    selectedValues={selected}
                    ref={multiselectRef}
                    closeOnSelect={false}
                    options={options.length > 0 ? options : [{
                        name: "No options available",
                        number: "No options available",
                        type: "No options available",
                        optionName: "No options available",

                    }]}
                    placeholder={options.length < 1? "No options available" : "Select"}
                    disable = {options.length < 1}
                    showCheckbox={true}
                />

                :
                <div/>}
            <button className="reset-button"
                    onClick={handleResetClick}>Reset
            </button>
        </div>
    )
}

export default MultiselectForTeam;








import {useEffect, useMemo, useRef, useState} from "react";
import axios from "axios";
import React from "react";
import DiagramsFilterComponent from "Components/Pages/ManagerComponents/DiagramFilters/DiagramsFilterComponent";
import '../../../../index.css'
import HoursEstimationDiagram from "./HoursEstimationDiagram";
import {useHistory} from "react-router-dom";
import {useStateIfMounted} from "use-state-if-mounted";
import "../Chart.css"


const HoursEstimationComponent = () => {

    const [trigger, setTrigger] = useStateIfMounted(0);
    const [diagramData, setDiagramData] = useStateIfMounted([]);
    const [timeInterval, setTimeInterval] = useStateIfMounted({});

    let history = useHistory();

    useEffect(() => {
        // return cleanupSub();
    }, [trigger])

    const cleanupSub = () => {
        setDiagramData([]);
        setTimeInterval({});
    }

    const handleGenerateDiag = (selectedEmployees, selectedProjects, timeInterval) => {
        setTrigger(trigger + 1)
        if (selectedEmployees.length < 1) {
            setDiagramData([])
        }
        if (selectedProjects.length < 1) {
            setDiagramData([])

        }
        const hoursEstimationRetrieveDts = {
            employeeIdentificators: selectedEmployees,
            projectNumbers: selectedProjects,
            weekFrom: timeInterval.weekFrom,
            yearFrom: timeInterval.yearFrom,
            weekTo: timeInterval.weekTo,
            yearTo: timeInterval.yearTo,
        }
        if (selectedEmployees.length > 0 && selectedProjects.length > 0) {
            axios.post(`http://localhost:8080/statistics/hours-estimation-diagram`, hoursEstimationRetrieveDts)
                .then(response => response.data)
                .then(data => {
                    setDiagramData(data)
                })
                .catch((error) => {
                    if (error.response.status === 401) {
                        history.push("/logout=true")
                    }
                })
            setTimeInterval(timeInterval)
        }
        // setTrigger(trigger + 1)
    }
    const triggerUp = () => {
        setTrigger(trigger + 1);
    }
    return (
        <div className="availability-container">
            <h2>Hours Estimation diagram</h2>
            <DiagramsFilterComponent filterData={handleGenerateDiag} triggerUp={triggerUp} shouldBeDisplayed={true}/>
            {/*{console.log(diagramData.listOfHoursConsumptionPerWeekDs)}*/}
            {diagramData.listOfHoursConsumptionPerWeekDs ?
                <HoursEstimationDiagram data={diagramData} triggerUp={triggerUp} timeInterval={timeInterval}/>
                : <div/>}
        </div>
    )
}

export default HoursEstimationComponent;
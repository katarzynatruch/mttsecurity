import {useHistory} from "react-router-dom";
import {useEffect, useRef, useState} from "react";
import AuthenticationService from "../../../../Support/LoginService/AuthenticationService";
import axios from "axios";
import "./TimeReportManagement.css"
import timeReportManagementBackground from "../../../../resources/time-report-management-background.png"
import {FormControl, InputLabel, TextField} from "@mui/material";
import {useStateIfMounted} from "use-state-if-mounted";

const TimeReportManagement = () => {

    const history = useHistory();
    const [dataHardWorking, setDataHardWorking] = useStateIfMounted([]);
    const [dataLazy, setDataLazy] = useStateIfMounted([]);
    const [trigger, setTrigger] = useStateIfMounted(0);
    const [checkedEmployees, setCheckedEmployees] = useStateIfMounted([]);
    const [message, setMessage] = useStateIfMounted("");
    const [showConfirmWindow, setShowConfirmWindow] = useStateIfMounted(false);

    let startTimerInterval = useRef();
    if(trigger<1){
        startTimerInterval.current = setTimeout(() => {
            console.log("timeout")
            setTrigger(trigger+1)
        }, (2000));
    }

    useEffect(() => {
        loadView();
        return cleanupSub();
    }, [trigger])

    const triggerUp = () => {
        setTrigger(trigger + 1)
    }

    const cleanupSub = () => {
        setDataHardWorking([]);
        setDataLazy([]);
        setCheckedEmployees([]);
    }

    const loadView = () => {
        const identificatorLoggedInUser = AuthenticationService.getLoggedInUserName();
        axios.get(`http://localhost:8080/lazy-employee/employee-workLog-confirmed/${identificatorLoggedInUser}`)
            .then(response => response.data)
            .then(data => {
                setDataHardWorking(data);
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
        axios.get(`http://localhost:8080/lazy-employee/employee-workLog-not-confirmed/${identificatorLoggedInUser}`)
            .then(response => response.data)
            .then(data => {
                setDataLazy(data);
                !data.reminderMessageDTO ?
                    setMessage("Reminder to report your work time W" + dataLazy.currentWeek + "Y" + dataLazy.currentYear)
                    : setMessage(data.reminderMessageDTO.defaultReminderMessage);

            })

    }

    const sum = (employeesWithWorklogs) => {
        let sum = 0;
        employeesWithWorklogs.worklogs.forEach(w => sum = sum + parseInt(w.workedHours))
        return sum;
    }

    const whatColorSum = (sum) => {
        return sum < 40 ? '#c90a0a' : 'inherit';
    }

    const handleCheckbox = (identificator, event) => {
        const isChecked = event.target.checked;
        if (isChecked) {
            setCheckedEmployees([...checkedEmployees, identificator]);
        } else {
            const tempCheckedEmployees = checkedEmployees.filter(ce => ce !== identificator);
            setCheckedEmployees(tempCheckedEmployees)
        }
    }

    const sendMessage = () => {
        const loggedInUser = AuthenticationService.getLoggedInUserName();
        axios.post("http://localhost:8080/lazy-employee/sending/", {
            employeeIdentificators: checkedEmployees,
            message: message,
        })
            .then(axios.post("http://localhost:8080/lazy-employee/change-default/", {
                managerIdentificator: loggedInUser,
                reminderMessageDTO: {
                    defaultReminderMessage: message,
                }
            }))
        setShowConfirmWindow(true);
    }

    const sendConfirmation = (identificator) => {
        const workLogDs = [];
        const employeeList = Array.from(dataHardWorking.employeesWithWorklogs).filter(eww => eww.employee.identificator === identificator);
        const worklogs = employeeList[0].worklogs;
        workLogDs.push({
            week: dataHardWorking.currentWeek,
            year: dataHardWorking.currentYear,
            workLogs: worklogs
        })
        axios.post("http://localhost:8080/lazy-employee/timeReport/approved-by-manager", {
            employeeIdentificator: identificator,
            workLogDs: workLogDs,
        })
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
        window.location.reload();
    }

    const setDefaultMessage = () => {
        setMessage("Reminder to report your work time W" + dataLazy.currentWeek + "Y" + dataLazy.currentYear)
    }

    function okClicked() {
        setShowConfirmWindow(false);
        if (checkedEmployees.length > 0)
            window.location.reload();
    }

    const checkIfShouldBeDisabled = (eww) => {
        return eww.worklogs.filter(w => w.isConfirmedByManager === true).length !== eww.worklogs.length;
    }

    return (
        <div className="report-m-container">
            <img className="report-m-image"
                 src={timeReportManagementBackground}
                 alt="img"
            />
            {showConfirmWindow ?
                <div className="confirmation-window">
                    {checkedEmployees.length > 0 ? <div>Mail with reminder has been sent to chosen users.</div>
                        : <div>Choose employees to send a reminder.</div>}
                    <button className="report-button" onClick={() => okClicked()}>Ok</button>
                </div>
                : <div/>}
            <div className="report-top-info">
                Today is W{dataHardWorking.currentWeek} Y{dataHardWorking.currentYear}
            </div>
            <div className="report-to-confirm-container">
                {dataHardWorking.employeesWithWorklogs ?
                    Array.from(dataHardWorking.employeesWithWorklogs).map(eww =>
                        <div className="report-single-card"
                             key={eww.employee.identificator}
                        >
                            {eww.employee.firstName} {eww.employee.lastName} {eww.employee.identificator}
                            <div>
                                <div className="report-row-flex">
                                    <div> Sum:</div>
                                    <div style={{color: whatColorSum(sum(eww))}}> {sum(eww)} </div>
                                </div>
                                <div> Projects:</div>
                                {eww.worklogs ? eww.worklogs.map(w =>
                                        <div key={eww.employee.identificator + w.project.number + w.workedHours}>
                                            <div> {w.project.number} {w.project.name}
                                            </div>
                                        </div>
                                    )
                                    : <div/>}
                            </div>
                            <div className="report-justify-center">
                                {checkIfShouldBeDisabled(eww) ?
                                    <button className="confirm-button"
                                            onClick={() => sendConfirmation(eww.employee.identificator)}
                                    >Confirm</button>
                                    :
                                    <button className="confirm-button" disabled={true}>Confirmed</button>}
                            </div>
                        </div>)
                    : <div/>}
            </div>
            <div className="report-to-remind-container">
                Users who not reported this week:
                {dataLazy.employees ? Array.from(dataLazy.employees).map(dl =>
                        <div key={dl.identificator}>
                            <div>
                                <input type="checkbox" onChange={(event) => handleCheckbox(dl.identificator, event)}/>
                                {dl.firstName} {dl.lastName} {dl.identificator}
                            </div>

                        </div>
                    )

                    : <div/>}
                <div>
                    {dataLazy.currentWeek ?
                        <div className="report-to-remind-container">
                            <FormControl>
                                <label style={{fontSize: 13, color: "grey"}}>Reminder message</label>
                                <TextField sx={{backgroundColor: 'rgb(255,255,255)', minWidth: 250, maxWidth: 600}}
                                           value={message}
                                           onChange={(event) => setMessage(event.target.value)}
                                           defaultValue={"Reminder to report your work time W" + dataLazy.currentWeek + "Y" + dataLazy.currentYear}/>
                            </FormControl>
                            <button className="default-message-button" onClick={setDefaultMessage}>Set default message
                            </button>
                            <button className="report-button" onClick={sendMessage}>Send reminder</button>
                        </div>
                        : <div/>
                    }
                </div>
            </div>

        </div>
    )
}
export default TimeReportManagement;
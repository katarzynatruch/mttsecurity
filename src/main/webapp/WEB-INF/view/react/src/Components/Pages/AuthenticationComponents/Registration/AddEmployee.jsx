import {red} from '@material-ui/core/colors';
import axios from 'axios';
import React, {Component, useEffect, useState} from 'react'
import {useHistory, useLocation} from 'react-router-dom';
import isEmail from "validator/es/lib/isEmail";
import newProject from "../../../../resources/new-project.png";
import {InputLabel, TextField} from "@mui/material";
import "./AddEmployee.css"

const API_URL = 'http://localhost:8080'

const AddEmployee = () => {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [identificator, setIdentificator] = useState(0);
    const [email, setEmail] = useState('');
    const [errMessage, setErrMessage] = useState('');
    let history = useHistory();
    let location = useLocation();

    useEffect(() => {
        if (location?.state) {
            setFirstName(location.state.firstName);
            setLastName(location.state.lastName);
            setEmail(location.state.email);
            setIdentificator(location.state.identificator);
        }
    }, [])

    function makeRandomPassword(length) {
        const result = [];
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        const numbers = '0123456789';
        const specials = '!@#$%^&*()';
        const charactersLength = characters.length;
        const numbersLength = numbers.length;
        const specialLength = specials.length;
        for (let i = 0; i < length; i++) {
            result.push(characters.charAt(Math.floor(Math.random() *
                charactersLength)));
        }
        for (let i = 0; i < 3; i++) {
            result.push(numbers.charAt(Math.floor(Math.random() *
                numbersLength)));
        }
        for (let i = 0; i < 3; i++) {
            result.push(specials.charAt(Math.floor(Math.random() *
                specialLength)));
        }
        return result.join('');
    }

    const saveEmployeeClicked = () => {

        if (location?.state) {
            if (isEmail(email)) {
                axios.post(`${API_URL}/account/user/save-edited`, createJsonDataUserDTO())
                    .then(response => {
                        if (response.data.proper) {
                            // zmienic
                            console.log(response.data)
                            history.push(`/employee-management?msg=${response.data.message}`);
                        } else {
                            console.log(response.data)
                            setErrMessage(response.data.message)
                        }
                    })
            } else {
                setErrMessage('Invalid email pattern')
            }
        } else {
            if (isEmail(email)) {
                axios.post(`${API_URL}/account/user/registration`, createJsonDataUserDTO())
                    .then(response => {
                        if (response.data.proper) {
                            // zmienic
                            console.log(response.data)
                            history.push(`/employee-management?msg=${response.data.message}`);
                        } else {
                            console.log(response.data)
                            setErrMessage(response.data.message)
                        }
                    })
            } else {
                setErrMessage('Invalid email pattern')
            }
        }
    }

    const createJsonDataUserDTO = () => {
        const randomPassword = makeRandomPassword(11);
        const userDTO =
            {
                firstName: firstName,
                lastName: lastName,
                password: randomPassword,
                matchingPassword: randomPassword,
                identificator: identificator,
                email: email,
                role1: "ROLE_USER",
                role2: "",
                role3: "",
                role4: "",
                role5: "",
                enabled: false,
                activate: false,

            }
        return userDTO;
    }

    const textStyle = {
        maxWidth: 350,
        // width: 300,
        minWidth: 120,
        background: 'rgb(255,255,255)',
        borderRadius: 1,
        color: '#292929',
    }

    return (
        <div className="add-employee-form-container ">
            <img src={newProject} alt="img" className="add-employee-image"/>
            <h2 style={{color: "darkgray"}}>Add employee</h2>
            <div className="add-employee-form">
                <div className="add-employee-error">{errMessage ? errMessage : <div/>}</div>
                <div className="add-employee-single-item ">
                    {/*<InputLabel>First name</InputLabel>*/}
                    <TextField label="First name"
                               sx={{...textStyle}}
                               value={firstName}
                               onChange={event => setFirstName(event.target.value)}/>
                </div>
                <div className="add-employee-single-item ">
                    {/*<InputLabel>Last name</InputLabel>*/}
                    <TextField type="text"
                               sx={{...textStyle}}
                               label="Last name"
                               value={lastName}
                               onChange={event => setLastName(event.target.value)}/>
                </div>
                {/*<div className="add-employee-single-item ">*/}
                {/*    <InputLabel>Identificator</InputLabel>*/}
                {/*    <TextField type="text"*/}
                {/*               sx={{...textStyle}}*/}
                {/*               label="Identificator"*/}
                {/*               value={identificator}*/}
                {/*               onChange={event => setIdentificator(event.target.value)}/>*/}
                {/*</div>*/}
                <div className="add-employee-single-item ">
                    {/*<InputLabel>Email</InputLabel>*/}
                    <TextField type="text"
                               sx={{...textStyle}}
                               label="Email"
                               value={email}
                               onChange={event => setEmail(event.target.value.trim())}/>
                </div>
                <div className="add-employee-single-item ">
                    <button className="time-button" onClick={saveEmployeeClicked}>Save employee</button>
                </div>
            </div>
        </div>
    )

}


export default AddEmployee;
import React, {useEffect, useRef, useState} from "react";
import '../../../../index.css'
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import {addDays, getWeek, getYear} from 'date-fns';
import moment from 'moment';
import {useStateIfMounted} from "use-state-if-mounted";
import useComponentVisible from "./useComponentVisible";
import "./Filters.css"
import {TextField} from "@mui/material";

const TimeIntervalFilter = (props) => {

    const [dateFrom, setDateFrom] = useStateIfMounted(new Date());
    const [dateTo, setDateTo] = useStateIfMounted(new Date());
    const [trigger, setTrigger] = useState(0);
    const [hoverRangeFrom, setHoverRangeFrom] = useStateIfMounted();
    const [selectedDaysFrom, setSelectedDaysFrom] = useStateIfMounted([]);
    const [selectedDaysTo, setSelectedDaysTo] = useStateIfMounted([]);
    const {ref, isComponentVisible, setIsComponentVisible} = useComponentVisible(false);

    useEffect(() => {
        updateParentData();
        return cleanupSub();
    }, [trigger])

    const cleanupSub = () => {

    }

    const updateParentData = () => {
        const weekFrom = getWeek(dateFrom);
        const yearFrom = getYear(dateFrom);
        const weekTo = getWeek(dateTo);
        const yearTo = getYear(dateTo);
        props.timeData(weekFrom, weekTo, yearFrom, yearTo, dateFrom, dateTo)
    }

    function getWeekDays(weekStart) {
        const days = [weekStart];
        for (let i = 1; i < 7; i += 1) {
            days.push(
                moment(weekStart)
                    .add(i, 'days')
                    .toDate()
            );
        }
        return days;
    }

    const handleDayChangeFrom = date => {
        setSelectedDaysFrom(getWeekDays(getWeekRange(date).from));
        setDateFrom(getWeekDays(getWeekRange(date).from)[0]);
        setTrigger(trigger + 1);

    };
    const handleDayChangeTo = date => {
        setSelectedDaysTo(getWeekDays(getWeekRange(date).from));
        setDateTo(getWeekDays(getWeekRange(date).from)[6]);
        setTrigger(trigger + 1);
    };

    const handleDayEnterFrom = date => {
        setHoverRangeFrom(getWeekRange(date));
    };
    const handleDayEnterTo = date => {
    };

    const handleDayLeaveFrom = () => {
        setHoverRangeFrom(undefined)
    };
    const handleDayLeaveTo = () => {
    };

    const handleWeekClickFrom = (weekNumber, days, e) => {
        setSelectedDaysFrom(days)
    };

    const handleWeekClickTo = (weekNumber, days, e) => {
        setSelectedDaysTo(days)
    };


    function getWeekRange(date) {
        return {
            from: moment(date)
                .startOf('week')
                .toDate(),
            to: moment(date)
                .endOf('week')
                .toDate(),
        };
    }

    const daysAreSelected = selectedDaysFrom.length > 0;

    const modifiers = {
        hoverRange: hoverRangeFrom,
        selectedRange: daysAreSelected && {
            from: selectedDaysFrom[0],
            to: selectedDaysFrom[6],
        },
        hoverRangeStart: hoverRangeFrom && hoverRangeFrom.from,
        hoverRangeEnd: hoverRangeFrom && hoverRangeFrom.to,
        selectedRangeStart: daysAreSelected && selectedDaysFrom[0],
        selectedRangeEnd: daysAreSelected && selectedDaysFrom[6],
    };

    return (
        <div className="time-interval-container" ref={ref}>
            <div className="rowFlex">
                <div className="single-input">
                    <TextField type="text"
                               label="From"
                               value={moment(selectedDaysFrom[0]).format('LL') + " week " + getWeek(dateFrom)}
                               onClick={() => setIsComponentVisible(!isComponentVisible)}
                    />
                </div>
                <div className="single-input">
                <TextField type="text"
                               label="To"
                               value={moment(selectedDaysTo[6]).format('LL') + " week " + getWeek(dateTo)}
                               onClick={() => setIsComponentVisible(!isComponentVisible)}
                    />
                </div>
            </div>
            <div>
                {isComponentVisible ?
                    <div className="rowFlex">
                        <div className="SelectedWeekExample">
                            <DayPicker
                                selectedDays={selectedDaysFrom}
                                showWeekNumbers
                                showOutsideDays
                                modifiers={modifiers}
                                onDayClick={handleDayChangeFrom}
                                onDayMouseEnter={handleDayEnterFrom}
                                onDayMouseLeave={handleDayLeaveFrom}
                                onWeekClick={handleWeekClickFrom}
                            />

                        </div>
                        <div className="SelectedWeekExample">
                            <DayPicker
                                selectedDays={selectedDaysTo}
                                showWeekNumbers
                                showOutsideDays
                                modifiers={modifiers}
                                onDayClick={handleDayChangeTo}
                                onDayMouseEnter={handleDayEnterTo}
                                onDayMouseLeave={handleDayLeaveTo}
                                onWeekClick={handleWeekClickTo}
                            />
                        </div>
                    </div>

                    :
                    <div/>}
            </div>
        </div>
    )

}

export default TimeIntervalFilter;
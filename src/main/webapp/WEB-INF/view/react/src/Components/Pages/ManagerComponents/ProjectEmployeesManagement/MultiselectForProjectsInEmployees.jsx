import Multiselect from "multiselect-react-dropdown";
import React, {useEffect, useRef, useState} from "react";
import {FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import axios from "axios";
import {useHistory} from "react-router-dom";
import {useStateIfMounted} from "use-state-if-mounted";


const MultiselectForEmployeesInProjects = (props) => {

    const [allProjects, setAllProjects] = useStateIfMounted([]);
    const [trigger, setTrigger] = useStateIfMounted(0);
    let history = useHistory();

    useEffect(() => {
        getAllProjects();
        // return cleanupSub();
    }, [trigger])

    const cleanupSub = () => {
        setAllProjects([]);
    }

    const triggerUp = () => {
        setTrigger(trigger + 1);
    }

    function getAllProjects() {
        axios.get('http://localhost:8080/project/plist')
            .then(response => response.data)
            .then(data => {
                setAllProjects(createProjectData(data.projects))
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
    }

    function createProjectData(projects) {
        return projects.map(projects => {
            return {
                name: projects.name,
                number: projects.number,
                costCenter: projects.costCenter,
                maxHours: projects.maxHours,
                type: projects.type
            }
        })
    }

    function handleSelectedFromListProject(event) {
        const singleEmployee = {
            projectNumber: event.target.value,
            employeeIdentificator: props.employeesWithProjects.employee.identificator,
            maxHours: 0,
        }

        saveSingleEmployee(singleEmployee);
    }

    const saveSingleEmployee = (singleEmployee) => {
        console.log(singleEmployee);
        axios.post(`http://localhost:8080/project-management/list/saveSingleEmployee`, {
            employeeIdentificator: singleEmployee.employeeIdentificator,
            projectNumber: singleEmployee.projectNumber,
            maxHours: singleEmployee.maxHours,
        })
            .then(() => {
                    props.triggerUp();
                }
            )
            .catch((error) => {
                if (error.response.status === 401) {
                    history.push("/logout=true")
                }
            })
    }


    const getAllNotBelongingToEmployeeProjects = (employeesWithProjects) => {
        return allProjects.filter(project => !checkIfProjectContainsUser(project, employeesWithProjects))
    }

    const checkIfProjectContainsUser = (project, employeesWithProjects) => {
        if (employeesWithProjects.projects.filter(p => p.number === project.number).length > 0) {
            return true;
        }
        return false;
    }

    return (
        <div style={{paddingTop: 15}}>
            <FormControl fullWidth>
                <InputLabel id="employee">Select project</InputLabel>
                <Select
                    disabled={props.disabled}
                    labelId="project"
                    id="number"
                    label="Select project"
                    onChange={(event) => handleSelectedFromListProject(event, props.employeesWithProjects)}
                    autoWidth
                    defaultValue=""
                >
                    {Array.from(getAllNotBelongingToEmployeeProjects(props.employeesWithProjects)).map(project => (
                        <MenuItem key={project} value={project.number}>{project.name + " " + project.number}</MenuItem>
                    ))}
                </Select>
            </FormControl>
        </div>
    )
}
export default MultiselectForEmployeesInProjects;
import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
} from "react-router-dom";

const ResetTokenSentComponent = () => {

    return (
        <div className="reg-container">
            <h2>Your password reset link has been sent on your email</h2>
            <h3><Link to="/login">Login</Link></h3>
            
        </div>
    )
}

export default ResetTokenSentComponent;
import Footer from "../../../Footer/Footer";
import "../Homepage.css"
import {useStateIfMounted} from "use-state-if-mounted";
import React, {useState} from "react";
import {useHistory} from "react-router-dom";
import axios from "axios";
import {red} from "@material-ui/core/colors";
import validator from 'validator/lib/isEmail';
import isEmail from "validator/es/lib/isEmail";

const API_URL = 'http://localhost:8080'


const NotLoggedHomepage = () => {

    const [firstName, setFirstName] = useStateIfMounted('');
    const [lastName, setLastName] = useStateIfMounted('');
    const [email, setEmail] = useStateIfMounted('');
    const [errMessage, setErrMessage] = useStateIfMounted('');
    let history = useHistory();

    function makeRandomPassword(length) {
        const result = [];
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        const numbers = '0123456789';
        const specials = '!@#$%^&*()';
        const charactersLength = characters.length;
        const numbersLength = numbers.length;
        const specialLength = specials.length;
        for (let i = 0; i < length; i++) {
            result.push(characters.charAt(Math.floor(Math.random() *
                charactersLength)));
        }
        for (let i = 0; i < 3; i++) {
            result.push(numbers.charAt(Math.floor(Math.random() *
                numbersLength)));
        }
        for (let i = 0; i < 3; i++) {
            result.push(specials.charAt(Math.floor(Math.random() *
                specialLength)));
        }
        return result.join('');
    }

    const createJsonDataUserDTO = () => {
        const randomPassword = makeRandomPassword(11);
        const userDTO =
            {
                firstName: firstName,
                lastName: lastName,
                password: randomPassword,
                matchingPassword: randomPassword,
                identificator: 0,
                email: email,
                role1: "ROLE_USER",
                role2: "ROLE_MANAGER",
                role3: "",
                role4: "",
                role5: "",
                enabled: false,
                activate: false
            }
        return userDTO;
    }

    const validation = () => {
        if (firstName.length < 3 || lastName.length < 3) {
            setErrMessage("Min 3 characters needed for name")
            return false;
        }
        return true;
    }
    const handleSubmit = () => {
        const user = createJsonDataUserDTO();
        if (validation()) {
            if (isEmail(email)) {
                axios.post('http://localhost:8080/account/user/registration', user)
                    .then(response => response.data)
                    .then(data => {
                        if (data.proper) {
                            history.push(`/registration-ongoing?msg=${data.message}&proper=${data.proper}`)
                        } else {
                            setErrMessage(data.message)
                        }
                    })
                    // .then(response =>
                    //     {
                    //         console.log("before axios to send token")
                    //         axios.post('http://localhost:8080/account/user/sendRegistrationToken', user)
                    //     }
                    //
                    // )
            } else {
                setErrMessage('Invalid email pattern')
            }
        }
    }

    return (
        <div className="container">
            <div className="image-background">
                <div className="column-flex">
                    <div className="home-text">
                        Create an account as manager
                    </div>
                    <div className="home-text-light">
                        Create your teams, assign employees to projects and explore analytical tools for time spent
                        on work
                    </div>
                    <div className="tools-container">
                        <div className="sign-in-box">
                            {errMessage ? <div style={{color: red[500]}}>{errMessage}</div> : <div/>}
                            <label>Email address</label>
                            <input type="text" className="input-account" value={email}
                                   onChange={(event) => setEmail(event.target.value.trim())}/>
                            <label>First name</label>
                            <input type="text" className="input-account" value={firstName}
                                   onChange={(event) => setFirstName(event.target.value.trim())}/>
                            <label>Last name</label>
                            <input type="text" className="input-account" value={lastName}
                                   onChange={(event) => setLastName(event.target.value.trim())}/>
                            <button className="home-button" onClick={handleSubmit}>Submit</button>
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <Footer/>
                </div>
            </div>
        </div>
    )
}
export default NotLoggedHomepage;
import axios from "axios";
import AuthenticationService from "Support/LoginService/AuthenticationService";
import {Component, useState} from "react";

const API_URL = 'http://localhost:8080'
export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'

class SupportService extends Component {

    constructor(props) {
        super(props)

        this.state = {
            manId: '',
            isAnAdmin: false,
            isAnManager: false,
            roles: [],
        }
    }

    getManagerId = async (loggedInUserIdentificator) => {
        if (this.checkIfLoggedUserIsManager(loggedInUserIdentificator)) {
            const data = await axios.post(`${API_URL}/user/get_id`, {
                loggedInUserIdentificator
            })
                .then(result => result.data)
                .then(data => {
                    this.setState({manId: data});
                    return data;
                })

        } else {
            return null
        }
    }

    checkRolesOfLoggedUser = (loggedInUserIdentificator) => {
        this.checkIfLoggedUserIsManager(loggedInUserIdentificator);
        return {
            roles: this.state.roles,
            isAnManager: this.state.isAManager,
            isAnAdmin: this.state.isAnAdmin,
        }
    }

    checkIfContainsManagerRole = (roles) => {
        if (roles.find(role => role == "ROLE_MANAGER") != null) {
            return true
        } else {
            return false
        }
    }
    checkIfLoggedUserIsManager = (loggedInUserIdentificator) => {
        if (AuthenticationService.isUserLoggedIn === null) {
            return false;
        }
        axios.post(`${API_URL}/user/get_roles`, {loggedInUserIdentificator})
            .then(response => response.data)
            .then(data => {
                this.setState({
                    isAManager: this.checkIfContainsManagerRole(data.userRoleNameList),
                    isAnAdmin: this.checkIfContainsAdminRole(data.userRoleNameList),
                    roles: data.userRoleNameList,
                });
            });
        return this.state.isAManager;
    }

    checkIfContainsAdminRole = (roles) => {
        if (roles.find(role => role == "ROLE_ADMIN") != null) {
            return true
        } else {
            return false
        }
    }

}

export default new SupportService;
import AuthenticationService from "./LoginService/AuthenticationService";
import axios from "axios";
import {USER_NAME_SESSION_ATTRIBUTE_NAME} from "../Navigation/ProtectedRoutings/AuthenticatedRouteAdmin";
import {useHistory} from "react-router-dom";
import {useEffect} from "react";

const API_URL = 'http://localhost:8080'

const AuthChecker = () => {

    let history = useHistory();

    useEffect(() => {
        window.addEventListener('unload', alertUser);
        return () => {
            window.addEventListener('unload', alertUser);
        };
    }, []);

    const alertUser = (e) => {
        e.preventDefault();
        checkIfUserLoggedInFromServer();
        e.returnValue = "";
    };

    const checkIfUserLoggedInFromServer = () => {
        const loggedInUserIdentificator = localStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        if(AuthenticationService.isUserLoggedIn()){
            axios.post(`${API_URL}/user/get_roles`, { loggedInUserIdentificator })
                .catch((error) => {
                    if (error.response.status === 401) {
                        AuthenticationService.logout();
                        // history.push("/logout=true")
                    }
                })
        }
    }


    return <div/>
}
export default AuthChecker;
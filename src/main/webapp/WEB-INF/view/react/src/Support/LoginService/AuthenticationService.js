import axios from 'axios'
import React from 'react'
import {Component} from 'react'
import {Redirect} from 'react-router-dom'

const API_URL = 'http://localhost:8080'

export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'

class AuthenticationService extends Component {

    constructor(props) {
        super(props)
        this.state = {
            token: '',
        }
    }

    executeBasicAuthenticationService(username, password) {
        return axios.get(`${API_URL}`,
            {
                headers: {
                    authorization: this.createBasicAuthToken(username, password)
                }
            })
    }

    executeJwtAuthenticationService(username, password) {
        console.log(username);
        return axios.post(`${API_URL}/authenticate`, {
            username,
            password
        })
    }

    createBasicAuthToken(username, password) {
        // this.setState({token : 'Basic ' + window.btoa(username + ":" + password)})
        return 'Basic ' + window.btoa(username + ":" + password)
    }

    registerSuccessfulLogin(username, password) {
        localStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username)
        this.setupAxiosInterceptors(this.createBasicAuthToken(username, password))
    }

    registerSuccessfulLoginForJwt(username, token) {
        localStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username)
        this.setupAxiosInterceptors(this.createJWTToken(token))
    }

    createJWTToken(token) {
        return 'Bearer ' + token
    }

    logout() {
        console.log("logout")
        axios.post(`${API_URL}/perform_logout`)
        localStorage.clear();
        return localStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
    }

    isUserLoggedIn() {
        let user = localStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        return user !== null;
    }

    getLoggedInUserName() {
        let user = localStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        if (user === null) return null
        return user
    }

    setupAxiosInterceptors(token) {
        const axios = require('axios');
        const axiosApiInstance = axios.create();
        axiosApiInstance.interceptors.request.use(
            (config) => {
                console.log("Token: " + token);
                if (this.isUserLoggedIn()) {
                    config.headers.authorization = token;
                    console.log("Token: " + token);
                    return config
                } else {
                    return config
                }

            }
        )
    }
}

export default new AuthenticationService()
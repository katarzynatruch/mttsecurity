import React, {
    useState,
    useEffect,
    useCallback,
    useRef,
    Fragment,
} from 'react';
import moment from 'moment';
import AuthenticationService from "./LoginService/AuthenticationService";
import {useHistory} from "react-router-dom";
import "./SessionTimeout.css"
import {useStateIfMounted} from "use-state-if-mounted";

const SessionTimeout = () => {
    const [events, setEvents] = useStateIfMounted(['click', 'load', 'scroll']);
    const [second, setSecond] = useStateIfMounted(0);
    const [isOpen, setOpen] = useStateIfMounted(false);

    let history = useHistory();
    let timeStamp;
    let warningInactiveInterval = useRef();
    let startTimerInterval = useRef();

    // start inactive check
    let timeChecker = () => {
        startTimerInterval.current = setTimeout(() => {
            let storedTimeStamp = sessionStorage.getItem('lastTimeStamp');
            warningInactive(storedTimeStamp);
            }, (60000*13));
        // }, (10000));
    };

    // warning timer
    let warningInactive = (timeString) => {
        clearTimeout(startTimerInterval.current);
        warningInactiveInterval.current = setInterval(() => {
            const maxTime = 2;
            const popTime = 1;
            const diff = moment.duration(moment().diff(moment(timeString)));
            const minPast = diff.minutes();
            const leftSecond = 60 - diff.seconds();

            if (minPast === popTime) {
                //inactive time has start
                setSecond(leftSecond);
                setOpen(true);
            }

            if (minPast === maxTime) {
                clearInterval(warningInactiveInterval.current);
                setOpen(false);
                sessionStorage.removeItem('lastTimeStamp');
                history.push("/logout=true")
                AuthenticationService.logout();
            }
        }, 1000);
    };

    // reset interval timer
    let resetTimer = useCallback(() => {
        clearTimeout(startTimerInterval.current);
        clearInterval(warningInactiveInterval.current);
        if (AuthenticationService.isUserLoggedIn()) {
            timeStamp = moment();
            sessionStorage.setItem('lastTimeStamp', timeStamp);
        } else {
            clearInterval(warningInactiveInterval.current);
            sessionStorage.removeItem('lastTimeStamp');
        }
        timeChecker();
        setOpen(false);
    }, [AuthenticationService.isUserLoggedIn()]);


    // handle close popup
    const handleClose = () => {
        setOpen(false);
        resetTimer();
    };

    useEffect(() => {
        events.forEach((event) => {
            window.addEventListener(event, resetTimer);
        });
        timeChecker();
        return () => {
            clearTimeout(startTimerInterval.current);
            // resetTimer();
        };
    }, [resetTimer, events, timeChecker]);

    if (!isOpen) {
        return null;
    }
    console.log(second)
    // change fragment to modal and handleclose func to close
    return (
        <div>
            {/*<Fragment/>*/}
            <div className="timeout-window">
                Your session is about to expire
                <div style={{fontSize: 20, fontWeight: "bold"}}>
                    {second}
                </div>
                <button className="time-button" onClick={handleClose}>Refresh session</button>
            </div>
        </div>
    )
}
export default SessionTimeout;
import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import './resources/font/stylesheet.css'
import './fonts/RobotoCondensed-BoldItalic.ttf'
import './fonts/RobotoCondensed-Bold.ttf'
import './fonts/RobotoCondensed-Italic.ttf'
import './fonts/RobotoCondensed-Light.ttf'
import './fonts/RobotoCondensed-Regular.ttf'
import './fonts/RobotoCondensed-LightItalic.ttf'

import Navigation from "./Navigation/Navigation";
import $ from 'jquery';
import SessionTimeout from "./Support/SessionTimeout";
window.jQuery = $;
window.$ = $;
global.jQuery = $;

ReactDOM.render(<Navigation />,

 document.getElementById('container'))
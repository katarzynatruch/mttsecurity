package pl.kt.myteamtime.secController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.kt.myteamtime.controller.util.GenericResponse;
import pl.kt.myteamtime.dto.PasswordDTO;
import pl.kt.myteamtime.dts.EmailDts;
import pl.kt.myteamtime.dts.PasswordDts;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.secEntity.VerificationToken;
import pl.kt.myteamtime.secRepository.TokenRepository;
import pl.kt.myteamtime.secService.SecurityService;
import pl.kt.myteamtime.secService.UserService;
import pl.kt.myteamtime.util.GlobalVariables;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/security")
public class TestAdminController {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	@Autowired
	private TokenRepository tokenRepository;
	@Autowired
	private Environment env;
	@Autowired
	private SecurityService securityService;
	@Autowired
	private UserService userService;
	@Autowired
	private MessageSource messages;
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@GetMapping("/log-in-data")
	@ResponseBody
	public String whatUserIsLoggingIn(Locale locale, @AuthenticationPrincipal UserDetails userDetails) {
		User user = userService.findUserByIdentificator(userDetails.getUsername());
		UserDetails userDet2 =  (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user2 = userService.findUserByIdentificator(userDet2.getUsername());

		String userString = "@ ID: " + user.getIdentificator() + " getContext ID: " + user2.getIdentificator();

		return userString;
	}
}

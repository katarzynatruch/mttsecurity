package pl.kt.myteamtime.secController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import pl.kt.myteamtime.authExceptions.EmailExistsException;
import pl.kt.myteamtime.controller.util.GenericResponse;
import pl.kt.myteamtime.dto.UserDTO;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.Role;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.secEntity.VerificationToken;
import pl.kt.myteamtime.secRepository.TokenRepository;
import pl.kt.myteamtime.secService.SecurityService;
import pl.kt.myteamtime.secService.UserService;
import pl.kt.myteamtime.service.EmployeeService;
import pl.kt.myteamtime.validation.OnRegistrationCompleteEvent;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/account")
public class UserManagementController {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private TokenRepository tokenRepository;
    @Autowired
    private Environment env;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messages;
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private ApplicationEventPublisher eventPublisher;
    @Autowired
    private EmployeeService employeeService;


    // Adding user to database, not enabled, not ready for use
    @PostMapping("/user/registration")
    @Transactional
    public GenericResponse registerUserAccount(@RequestBody @Valid UserDTO accountDto, HttpServletRequest request) {
            logger.debug("Registering value = user account with information: {}", accountDto);
        if (userService.findUserByEmail(accountDto.getEmail()) != null) {
            return new GenericResponse(messages.getMessage("message.userRegisteredEmailFail", null, request.getLocale()), false);
        }
        if (userService.findUserByIdentificator(accountDto.getIdentificator()) != null) {
            return new GenericResponse(messages.getMessage("message.userRegisteredIdentificatorFail", null, request.getLocale()), false);
        }
        String newIdentificator = createIdentificator(accountDto);
        accountDto.setIdentificator(newIdentificator);
        User registered = createUserAccount(accountDto);
        if (registered == null) {
            return new GenericResponse(messages.getMessage("message.userRegisteredFail", null, request.getLocale()), false);
        }
        if (checkIfIsManager(accountDto)) {
            sendRegistrationToken(request, registered);
            return new GenericResponse(messages.getMessage("message.managerRegistered", null, request.getLocale()), true);
        } else {
            return new GenericResponse(messages.getMessage("message.userRegistered", null, request.getLocale()), true);
        }
    }

    @PostMapping("/user/save-edited")
    public GenericResponse saveEditedUserAccount(@RequestBody @Valid UserDTO accountDto, HttpServletRequest request) {
        if (userService.saveEditedUser(accountDto)) {
            return new GenericResponse(messages.getMessage("message.userEditedSucc", null, request.getLocale()), true);
        } else {
            return new GenericResponse(messages.getMessage("message.userEditedFail", null, request.getLocale()), false);
        }
    }

    private String createIdentificator(UserDTO accountDto) {
        String newIdentificator;
        //TODO if list is null then error
        List<UserDTO> all = userService.findAll();
        List<String> cutIdentificators = all.stream().map(user -> user.getIdentificator().substring(1)).sorted().collect(Collectors.toList());
        long newIdentificatorNumber = Long.parseLong(cutIdentificators.get(cutIdentificators.size() - 1)) + 1;
        if (checkIfIsManager(accountDto)) {
            newIdentificator = "M" + newIdentificatorNumber;
        } else {
            newIdentificator = "E" + newIdentificatorNumber;
        }
        return newIdentificator;
    }

    private boolean checkIfIsManager(UserDTO accountDto) {
        return !accountDto.getRole2().isEmpty();
//        return accountDto.getRole2().equals("ROLE_MANAGER");
    }

    private boolean checkIfIsManager(User user) {
        for (Role role : user.getRoles()) {
            if (role.getName().equals("ROLE_MANAGER")) return true;
        }
        return false;
    }

    //Give to client existing token of user
    @GetMapping("/user/getExpiredToken")
    public VerificationToken giveExpiredVerificationTokenOfUser(HttpServletRequest request, Locale locale, @RequestBody UserDTO userDTO, @RequestParam("token") String expiredToken) {

        User user = userService.findUserByIdentificator(userDTO.getIdentificator());
        VerificationToken token = tokenRepository.findByUser(user);
        if (token == null) {
            return null;
        }
        if (checkIfTokenExpired(token)) {
            return null;
        }
        return token;
    }

    private boolean checkIfTokenExpired(VerificationToken token) {
        Calendar cal = Calendar.getInstance();
        if ((token.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            return true;
        }
        return false;
    }

    // Send user an activation link with email, generating verification token
    @PostMapping("/user/sendRegistrationTokenManager")
    public GenericResponse sendRegistrationToken(HttpServletRequest request, @RequestBody User user) {
//        User user = userService.findUserByIdentificator(user.getIdentificator());
        String token = UUID.randomUUID().toString();
        userService.createVerificationToken(user, token);
        SimpleMailMessage email = constructNewTokenEmail(getAppUrl(request), request.getLocale(), token, user);
        mailSender.send(email);
        return new GenericResponse(messages.getMessage("message.sendToken", null, request.getLocale()));
    }

    @PostMapping("/user/sendRegistrationToken")
    public GenericResponse sendRegistrationTokenByActivate(HttpServletRequest request, @RequestBody User userRetreived) {
        User user = userService.findUserByIdentificator(userRetreived.getIdentificator());
        String token = UUID.randomUUID().toString();
        userService.createVerificationToken(user, token);
        SimpleMailMessage email = constructNewTokenEmail(getAppUrl(request), request.getLocale(), token, user);
        mailSender.send(email);
        return new GenericResponse(messages.getMessage("message.sendToken", null, request.getLocale()));
    }


    // Resend user an activation link with email, replacing verification token
    @GetMapping("/user/resendRegistrationToken/{existingToken}")
    public GenericResponse resendRegistrationToken(HttpServletRequest request, @PathVariable String existingToken) {
        VerificationToken newToken = userService.generateNewVerificationToken(existingToken);
        User user = userService.getUser(newToken.getToken());
        String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        SimpleMailMessage email = constructResendVerificationTokenEmail(appUrl, request.getLocale(), newToken, user);
        mailSender.send(email);

        return new GenericResponse(messages.getMessage("message.resendToken", null, request.getLocale()));
    }

    @PostMapping("/user/remove/{userIdentificator}")
    public GenericResponse deactivateUser(HttpServletRequest request, @PathVariable String userIdentificator) {
        userService.deactivateUser(userIdentificator);
        employeeService.deactivateEmployee(userIdentificator);
        return new GenericResponse(messages.getMessage("message.userDeleted", null, request.getLocale()));
    }

    // ???
    @PostMapping("/giveAccess")
    public void giveAccess(@RequestParam String userIdentificator, WebRequest request) {
        //,	BindingResult result, Errors errors
        User registered = userService.findByIdentificator(userIdentificator);
        try {
            String appUrl = request.getContextPath();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), appUrl));
        } catch (Exception me) {
        }
    }

    private SimpleMailMessage constructNewTokenEmail(String contextPath, Locale locale, String token, User user) {
        SimpleMailMessage email = new SimpleMailMessage();
        String url = contextPath + "/account/unauthorized/assignPassword?id=" + user.getId() + "&token=" + token;
        String message;
        if (checkIfIsManager(user)) {
            message = messages.getMessage("message.welcomeRegistrationLinkForManager", null, locale);
            message = message + "\n" + user.getIdentificator() + "\n";
        } else {
            message = messages.getMessage("message.welcomeRegistrationLink", null, locale);
        }
        email.setSubject("Welcome to MyTeamTime - registration");
        email.setText(message + " \r\n" + url);
        email.setTo(user.getEmail());
        email.setFrom(Objects.requireNonNull(env.getProperty("spring.mail.username")));
        return email;
    }


    private SimpleMailMessage constructResendVerificationTokenEmail(String contextPath, Locale locale, VerificationToken newToken, User user) {
        String confirmationUrl = contextPath + "/account/unauthorized/assignPassword?id=" + user.getId() + "&token=" + newToken.getToken();
        String message = messages.getMessage("message.resendToken", null, locale);
        SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject("MyTeamTime - Resend Registration Token");
        email.setText(message + " \r\n" + confirmationUrl);
        email.setFrom(Objects.requireNonNull(env.getProperty("spring.mail.username")));
        email.setTo(user.getEmail());
        return email;
    }


    private User createUserAccount(UserDTO accountDto) {
        User registered = null;

        try {
            Employee employee = employeeService.saveEmployee(accountDto);
            registered = userService.registerNewUserAccount(accountDto, employee);
        } catch (EmailExistsException e) {
            return null;
        }
        return registered;
    }

    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
}

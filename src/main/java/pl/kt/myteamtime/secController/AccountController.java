package pl.kt.myteamtime.secController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.kt.myteamtime.controller.util.GenericResponse;
import pl.kt.myteamtime.dts.EmailDts;
import pl.kt.myteamtime.dts.PasswordChangedDts;
import pl.kt.myteamtime.dts.PasswordDts;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.secEntity.ResetToken;
import pl.kt.myteamtime.secEntity.VerificationToken;
import pl.kt.myteamtime.secRepository.ResetTokenRepository;
import pl.kt.myteamtime.secRepository.TokenRepository;
import pl.kt.myteamtime.secService.SecurityService;
import pl.kt.myteamtime.secService.UserService;
import pl.kt.myteamtime.service.EmployeeService;
import pl.kt.myteamtime.util.GlobalVariables;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/account")
public class AccountController {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	@Autowired
	private TokenRepository tokenRepository;
	@Autowired
	private ResetTokenRepository resetTokenRepository;
	@Autowired
	private Environment env;
	@Autowired
	private SecurityService securityService;
	@Autowired
	private UserService userService;
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private MessageSource messages;
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private ApplicationEventPublisher eventPublisher;
//	@Autowired
//	private AuthenticationManager authenticationManager;


// no. 1 User gives email and receive link to reset password
	@PostMapping("/unauthorized/resetPassword")
	public GenericResponse resetPassword(HttpServletRequest request, @RequestBody EmailDts emailDts) {
		User user = userService.findUserByEmail(emailDts.getEmail());
		if (user == null) {
			return new GenericResponse(messages.getMessage("message.resetPasswordEmailNotValid", null, request.getLocale()),false);
		}
		if(!user.isEnabled()){
			return new GenericResponse(messages.getMessage("message.accountNotEnabled", null, request.getLocale()), false);
		}
		String token = UUID.randomUUID().toString();
		userService.createPasswordResetTokenForUser(user, token);
		mailSender.send(constructResetTokenEmail(getAppUrl(request), request.getLocale(), token, user));
		return new GenericResponse(messages.getMessage("message.resetPasswordEmailSent", null, request.getLocale()), true);
	}

// no 2. This controller checks if link is not expired then redirect to resend page or password&password matching page
	@GetMapping("/unauthorized/changePassword")
	public ModelAndView showChangePasswordPage(Locale locale, @RequestParam("id") long id,
											   @RequestParam("token") String token) {
		String result = securityService.validatePasswordResetToken(id, token);
		if (result != null) {
			return new ModelAndView("redirect:" + GlobalVariables.CLIENT_URL + "reset-token-expired");
		}
		//przekazać token do reacta
		return new ModelAndView("redirect:" + GlobalVariables.CLIENT_URL + "reset-password?id="+id+"&token="+token);
	}

	@GetMapping("/unauthorized/assignPassword")
	public ModelAndView showAssignPasswordPage(Locale locale, @RequestParam("id") long id,
											   @RequestParam("token") String token) {
		String result = securityService.validatePasswordNewRegisteredToken(id, token);
		if (result != null) {
			return new ModelAndView("redirect:" + GlobalVariables.CLIENT_URL + "reset-token-expired");
		}
		//przekazać token do reacta
		return new ModelAndView("redirect:" + GlobalVariables.CLIENT_URL + "assign-password?id="+id+"&token="+token);
	}

//	@PostMapping("/user/updatePassword")
//	@PreAuthorize("hasRole('READ_PRIVILEGE')")
//	@ResponseBody
//	public GenericResponse changeUserPassword(Locale locale, @RequestParam("password") String password,
//			@RequestParam("oldPassword") String oldPassword) {
//		User user = userService.findUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
//
//		if (!userService.checkIfValidOldPassword(user, oldPassword)) {
//			throw new InvalidOldPasswordException();
//		}
//		userService.changeUserPassword(user, password);
//		return new GenericResponse(messages.getMessage("message.updatePasswordSuc", null, locale));
//	}

	// no 4. New user, who received link from manager register his password
@PostMapping("/unauthorized/registration-new-password")
public GenericResponse saveNewPasswordRegistration(Locale locale, @RequestBody @Valid PasswordDts passwordDts, @RequestParam("id") long id,
												   @RequestParam("token") String token) {

	VerificationToken verificationToken = userService.getVerificationToken(token);
	if (verificationToken == null) {
		return new GenericResponse(messages.getMessage("auth.message.invalidToken", null, locale), false);
	}

	User user = verificationToken.getUser();
	if (checkIfTokenExpired(verificationToken)) {
		return new GenericResponse(messages.getMessage("auth.message.expired", null, locale), false);
	}

	user.setEnabled(true);
	user.setActive(true);
	user.setDeactivated(false);
	userService.changeUserPassword(user, passwordDts.getPassword());
	employeeService.saveEmployee(user);
//	userService.saveRegisteredUser(user);
	return new GenericResponse(messages.getMessage("auth.message.successful", null, locale), true);
}
	private boolean checkIfTokenExpired(VerificationToken token) {
		Calendar cal = Calendar.getInstance();
		if ((token.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			return true;
		}
		return false;
	}

// no 5. User who forgot the password register his new password
	@PostMapping("/unauthorized/new-password")
	@ResponseBody
	public GenericResponse saveNewPassword(Locale locale, @RequestBody @Valid PasswordDts passwordDts, @RequestParam("id") long id,
									   @RequestParam("token") String token) {
//		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		String result = securityService.validatePasswordResetToken(id, token);
		if (result != null) {
			return new GenericResponse(messages.getMessage("message.resetPasswordErr", null, locale),false);
		}
		User user = userService.findById(id);
		userService.changeUserPassword(user, passwordDts.getPassword());
		return new GenericResponse(messages.getMessage("message.resetPasswordSuc", null, locale),true);
	}

	// no 6. User changes password from old to new
	@PostMapping("/update-old-password")
	@ResponseBody
	public GenericResponse updateOldPassword(Locale locale, @RequestBody PasswordChangedDts passwordChangedDts
											 , @AuthenticationPrincipal UserDetails userDetails) {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(userService.checkIfValidOldPassword(passwordChangedDts.getOldPassword(), userDetails.getPassword())){
			userService.changeUserPassword((User) userDetails, passwordChangedDts.getNewPassword());
			return new GenericResponse(messages.getMessage("message.updatePasswordSuc", null, locale),true);
		}else{
			return new GenericResponse(messages.getMessage("message.updatePasswordErr", null, locale), false);
		}

	}

	@PostMapping("/token-used")
	@ResponseBody
	public GenericResponse expireToken(Locale locale, @RequestParam("id") long id, @RequestParam("token") String token) {

		Calendar calendar = Calendar.getInstance();
		VerificationToken verificationToken = userService.getVerificationToken(token);
		if(verificationToken != null){
			verificationToken.setExpiryDate(calendar.getTime());
			tokenRepository.save(verificationToken);
		}
		ResetToken resetToken = userService.getResetToken(token);
		if(resetToken != null){
			resetToken.setExpiryDate(calendar.getTime());
			resetTokenRepository.save(resetToken);
		}

		return new GenericResponse(messages.getMessage("auth.message.token.used", null, locale), false);
	}



	private SimpleMailMessage constructResetTokenEmail(String contextPath, Locale locale, String token, User user) {
		String url = contextPath + "/account/unauthorized/changePassword?id=" + user.getId() + "&token=" + token;
		String message = messages.getMessage("message.resetPassword", null, locale);
		return constructEmail("Reset Password", message + " \r\n" + url, user);
	}

	private SimpleMailMessage constructEmail(String subject, String body, User user) {
		SimpleMailMessage email = new SimpleMailMessage();
		email.setSubject(subject);
		email.setText(body);
		email.setTo(user.getEmail());
		email.setFrom(Objects.requireNonNull(env.getProperty("spring.mail.username")));
		return email;
	}

	private String getAppUrl(HttpServletRequest request) {
		return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
	}
}

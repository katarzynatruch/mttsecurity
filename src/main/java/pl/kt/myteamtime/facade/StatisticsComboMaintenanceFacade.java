package pl.kt.myteamtime.facade;

public interface StatisticsComboMaintenanceFacade {

	public int getHoursForGivenProjectNumberAndEmployeeIdentificator(String employeeIdentificator,
			String projectNumber, int weekFrom, int weekTo, int yearFrom, int yearTo);
}

package pl.kt.myteamtime.facade;

import pl.kt.myteamtime.ds.AvailabilitySingleEmployeeUpdateDs;
import pl.kt.myteamtime.dts.AvailabilityForManagerDts;
import pl.kt.myteamtime.dts.FilterDataRetrieveDts;
import pl.kt.myteamtime.dts.FilterRetrieveDts;

public interface AvailabilityFacade {

	AvailabilityForManagerDts loadLogs(String managerIdentificator, FilterDataRetrieveDts filterDataRetrieveDts);
	AvailabilityForManagerDts loadLogs(String managerIdentificator);

	void saveLogs(AvailabilitySingleEmployeeUpdateDs availabilitySingleEmployeeDs);

}

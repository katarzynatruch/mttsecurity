package pl.kt.myteamtime.facade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.ds.ProjectsAndTimeForEmployeeDs;
import pl.kt.myteamtime.ds.SingleProjectAndHoursEmployeeDs;
import pl.kt.myteamtime.ds.WorkLogWeekDs;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dto.ProjectEmployeeMaxHoursDTO;
import pl.kt.myteamtime.dto.WorkLogDTO;
import pl.kt.myteamtime.dts.*;
import pl.kt.myteamtime.service.*;

import static org.checkerframework.checker.units.UnitsTools.h;

@Service
public class EmployeeTimeReportFacadeImpl implements EmployeeTimeReportFacade {

    @Autowired
    ReminderMessageService reminderMessageService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private DataAndTimeService dataAndTime;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private WorkLogService workLogService;
    @Autowired
    private ProjectEmployeeMaxHoursService projectEmployeeMaxHoursService;
    @Autowired
    private StatisticsComboMaintenanceFacade statisticsComboMaintenanceFacade;

    @Override
    public EmployeeTimeReportDts loadTabData(EmployeeTimeReportUpdateDts employeeTimeReportUpdateDts) {

        EmployeeTimeReportDts tabDts = new EmployeeTimeReportDts();
        tabDts.setCurrentWeek(dataAndTime.getCurrentWeekGlobal());
        tabDts.setCurrentYear(dataAndTime.getCurrenYearGlobal());
        tabDts.setEmployee(employeeService.findByEmployeeIdentificatorDTO(employeeTimeReportUpdateDts.getIdentificator()));

        ProjectsAndTimeForEmployeeDs projectsAndTimeForEmployeeDs = new ProjectsAndTimeForEmployeeDs();
        List<SingleProjectAndHoursEmployeeDs> listOfAssignedHoursForProjectAndEmployee = new ArrayList<SingleProjectAndHoursEmployeeDs>();
        List<ProjectDTO> projects = projectService.findProjectsByEmployeeIdentificator(employeeTimeReportUpdateDts.getIdentificator());

        for (ProjectDTO project : projects) {
            SingleProjectAndHoursEmployeeDs hoursForProjectAndEmployee = new SingleProjectAndHoursEmployeeDs();
            long hoursUsed = statisticsComboMaintenanceFacade.getHoursForGivenProjectNumberAndEmployeeIdentificator(
                    employeeTimeReportUpdateDts.getIdentificator(), project.getNumber(), 0, tabDts.getCurrentWeek(), 0, tabDts.getCurrentYear());
            hoursForProjectAndEmployee.setHoursUsed(hoursUsed);
            hoursForProjectAndEmployee.setProject(project);
            listOfAssignedHoursForProjectAndEmployee.add(hoursForProjectAndEmployee);
            List<String> employees = new ArrayList<>();
            employees.add(employeeTimeReportUpdateDts.getIdentificator());
            hoursForProjectAndEmployee.setHoursLeft(
                    projectEmployeeMaxHoursService.calculateMaxHoursForProjectAndEmployees(project.getNumber(), employees)
                            .stream()
                            .mapToLong(ProjectEmployeeMaxHoursDTO::getMaxHours)
                            .sum()- hoursUsed);
        }
        projectsAndTimeForEmployeeDs
                .setEmployee(employeeService.findByEmployeeIdentificatorDTO(employeeTimeReportUpdateDts.getIdentificator()));
        projectsAndTimeForEmployeeDs.setProjectsWithHours(listOfAssignedHoursForProjectAndEmployee);
        tabDts.setProjectsAndTimeForEmployeeDs(projectsAndTimeForEmployeeDs);
        tabDts.setWorkLogWeeks(
                workLogService.findWorkLogWeeksByUserIdentificator(employeeTimeReportUpdateDts.getIdentificator()));

        return tabDts;
    }

    @Override
    public void saveOrUpdate(EmployeeTimeReportSaveDts employeeTimeReportSaveDts) {
        employeeTimeReportSaveDts.getWorkLogWeeks()
                .forEach(w -> w.getWorkLogs().forEach(ww -> ww.setWeek(w.getWeek())));
        employeeTimeReportSaveDts.getWorkLogWeeks()
                .forEach(w -> w.getWorkLogs().forEach(ww -> ww.setYear(w.getYear())));
        List<WorkLogDTO> workLogs = new ArrayList<WorkLogDTO>();
        employeeTimeReportSaveDts.getWorkLogWeeks().forEach(w -> workLogs.addAll(w.getWorkLogs()));

        workLogs.forEach(w -> w.setEmployee(employeeService.findByEmployeeIdentificatorDTO(employeeTimeReportSaveDts.getEmployeeIdentificator())));
        workLogService.saveOrUpdate(workLogs);

    }

    @Override
    public void approveWorkLogsByEmployee(ApproveWorkLogsByEmployeeDts approveWorkLogsByEmployeeDts) {
        List<WorkLogWeekDs> workLogWeekDsList = approveWorkLogsByEmployeeDts.getWorkLogDs();
        for (WorkLogWeekDs workLogWeekDs : workLogWeekDsList) {
            if (workLogWeekDs.getYear() == dataAndTime.getCurrenYearGlobal()) {
                if (workLogWeekDs.getWeek() == dataAndTime.getCurrentWeekGlobal()) {
                    for (WorkLogDTO workLog : workLogWeekDs.getWorkLogs()) {
                        workLog.setEmployee(employeeService.findByEmployeeIdentificatorDTO(approveWorkLogsByEmployeeDts.getEmployeeIdentificator()));
                    }
                    workLogService.updateEmployeeApproval(workLogWeekDs.getWorkLogs(), approveWorkLogsByEmployeeDts.getEmployeeIdentificator());
                }
            }
            if (workLogWeekDs.getYear() == (dataAndTime.getCurrenYearGlobal() - 1)) {
                if (workLogWeekDs.getWeek() == dataAndTime.getMaxWeekOfTheYear(dataAndTime.getCurrenYearGlobal() - 1)) {
                    for (WorkLogDTO workLog : workLogWeekDs.getWorkLogs()) {

                        workLog.setIsConfirmedByEmployee(true);
                    }
                    workLogService.updateEmployeeApproval(workLogWeekDs.getWorkLogs(), approveWorkLogsByEmployeeDts.getEmployeeIdentificator());
                }
            }
            if (workLogWeekDs.getYear() == dataAndTime.getCurrenYearGlobal()) {
                if (workLogWeekDs.getWeek() == (dataAndTime.getCurrentWeekGlobal() - 1)) {
                    for (WorkLogDTO workLog : workLogWeekDs.getWorkLogs()) {

                        workLog.setIsConfirmedByEmployee(true);
                    }
                    workLogService.updateEmployeeApproval(workLogWeekDs.getWorkLogs(), approveWorkLogsByEmployeeDts.getEmployeeIdentificator());
                }
            }
        }
    }

    @Override
    public void approveWorkLogsByManager(ApproveWorkLogsByEmployeeDts approveWorkLogsByEmployeeDts) {

        List<WorkLogWeekDs> workLogWeekDsList = approveWorkLogsByEmployeeDts.getWorkLogDs();
        for (WorkLogWeekDs workLogWeekDs : workLogWeekDsList) {

            if (workLogWeekDs.getYear() == dataAndTime.getCurrenYearGlobal()) {
                if (workLogWeekDs.getWeek() == dataAndTime.getCurrentWeekGlobal()) {
                    workLogService.updateManagerApproval(workLogWeekDs.getWorkLogs(), approveWorkLogsByEmployeeDts.getEmployeeIdentificator());
                }
            }
            if (workLogWeekDs.getYear() == (dataAndTime.getCurrenYearGlobal() - 1)) {
                if (workLogWeekDs.getWeek() == dataAndTime.getMaxWeekOfTheYear(dataAndTime.getCurrenYearGlobal() - 1)) {
                    workLogService.updateManagerApproval(workLogWeekDs.getWorkLogs(), approveWorkLogsByEmployeeDts.getEmployeeIdentificator());
                }
            }
            if (workLogWeekDs.getYear() == dataAndTime.getCurrenYearGlobal()) {
                if (workLogWeekDs.getWeek() == (dataAndTime.getCurrentWeekGlobal() - 1)) {
                    workLogService.updateManagerApproval(workLogWeekDs.getWorkLogs(), approveWorkLogsByEmployeeDts.getEmployeeIdentificator());
                }
            }

        }

    }

    @Override
    public void deleteWorkLog(WorkLogDeleteDts workLogDeleteDts) {
        workLogService.deleteWorkLog(workLogDeleteDts.getWorkLogId());
    }

}

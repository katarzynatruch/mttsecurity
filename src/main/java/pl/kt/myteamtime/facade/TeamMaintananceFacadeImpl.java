package pl.kt.myteamtime.facade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.TeamDTO;
import pl.kt.myteamtime.dts.EmployeeTeamLoadDts;
import pl.kt.myteamtime.service.EmployeeService;
import pl.kt.myteamtime.service.TeamService;

@Service
public class TeamMaintananceFacadeImpl implements TeamMaintananceFacade {

	@Autowired
	private TeamService teamService;

	@Autowired
	private EmployeeService employeeService;

	@Override
	public List<EmployeeTeamLoadDts> findTeamsByManagerIdentificator(String managerIdentificator) {

		List<TeamDTO> teams = teamService.findTeamsByManagerIdentificator(managerIdentificator);

		List<EmployeeTeamLoadDts> teamsWithEmployees = new ArrayList<>();

		for (TeamDTO teamDTO : teams) {

			EmployeeTeamLoadDts employeeTeamUpdateDts = new EmployeeTeamLoadDts();
			List<EmployeeDTO> employees = employeeService.findEmployeesByTeamId(teamDTO.getId());
			employeeTeamUpdateDts.setEmployees(employees);
			employeeTeamUpdateDts.setTeam(teamDTO);
			teamsWithEmployees.add(employeeTeamUpdateDts);
		}

		return teamsWithEmployees;
	}

}

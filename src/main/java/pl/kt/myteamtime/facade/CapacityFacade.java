package pl.kt.myteamtime.facade;

import pl.kt.myteamtime.dts.CapacityDts;
import pl.kt.myteamtime.dts.FilterRetrieveDts;
import pl.kt.myteamtime.dts.GraphFilterListDts;

public interface CapacityFacade {

	CapacityDts loadGraph(FilterRetrieveDts filterRetrieveDts);

	GraphFilterListDts loadFilters(String managerIdentificator);

    void exportToExcel(CapacityDts capacityDts);
}

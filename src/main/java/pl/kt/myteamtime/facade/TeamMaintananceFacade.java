package pl.kt.myteamtime.facade;

import java.util.List;

import pl.kt.myteamtime.dts.EmployeeTeamLoadDts;

public interface TeamMaintananceFacade {


	List<EmployeeTeamLoadDts> findTeamsByManagerIdentificator(String managerIdentificator);

	
	
}

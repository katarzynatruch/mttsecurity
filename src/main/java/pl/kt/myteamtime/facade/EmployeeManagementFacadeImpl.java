package pl.kt.myteamtime.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dts.EmployeesDts;
import pl.kt.myteamtime.service.EmployeeService;

@Service
public class EmployeeManagementFacadeImpl implements EmployeeManagementFacade {

	@Autowired
	EmployeeService employeeService;

	@Override
	public void saveOrUpdateEmployee(EmployeesDts employeesDts) {

		List<EmployeeDTO> employees;

		employees = employeesDts.getEmployees();
		employees.forEach((e -> e.setIsActive(true)));
		employees.forEach(e -> employeeService.saveEmployee(e));

	}

	@Override
	public EmployeesDts loadView(String managerIdentificator) {

		List<EmployeeDTO> employees = employeeService.findEmployeeByManagerIdentificator(managerIdentificator);
		EmployeesDts employeesDts = new EmployeesDts();
		employeesDts.setEmployees(employees);

		return employeesDts;
	}

	@Override
	public void deleteEmployee(Long employeeId) {
		employeeService.delete(employeeId);
	}

}

package pl.kt.myteamtime.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.dto.TeamDTO;
import pl.kt.myteamtime.ds.EmployeeTeamUpdateDs;
import pl.kt.myteamtime.dts.EmployeeTeamUpdateDts;
import pl.kt.myteamtime.dts.TeamManagementListDts;
import pl.kt.myteamtime.repository.TeamRepository;
import pl.kt.myteamtime.service.EmployeeService;
import pl.kt.myteamtime.service.TeamService;

@Service
public class TeamManagementFacadeImpl implements TeamManagementFacade {

	@Autowired
	private
	TeamService teamService;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	private
	TeamMaintananceFacade teamMaintananceFacade;

	@Autowired
	TeamRepository teamRepository;
	
	@Override
	public TeamManagementListDts loadTabData(String managerIdentificator) {
		TeamManagementListDts tabDts = new TeamManagementListDts();
		tabDts.setEmployeeTeamLoadDts(teamMaintananceFacade.findTeamsByManagerIdentificator(managerIdentificator));
		return tabDts;
	}

	@Override
	public void saveOrUpdateData(EmployeeTeamUpdateDts employeeTeamUpdateDsList) {
		List<EmployeeTeamUpdateDs> employeeTeamUpDsList = employeeTeamUpdateDsList.getEmployeeTeamUpdateDsList();
		employeeTeamUpDsList.forEach(this::saveTeamsWithAssignedEmployees);
	}

	@Override
	public void updateDataAfterDeleteTeams(EmployeeTeamUpdateDts employeeTeamUpdateDsList) {
		List<EmployeeTeamUpdateDs> employeeTeamUpDsList = employeeTeamUpdateDsList.getEmployeeTeamUpdateDsList();
		teamRepository.deleteAll();
		employeeTeamUpDsList.forEach(this::saveTeamsWithAssignedEmployees);
	}

	private void saveTeamsWithAssignedEmployees(EmployeeTeamUpdateDs employeeTeamUpdateDs) {

		teamService.saveTeamWithAssignedEmployees(employeeTeamUpdateDs.getTeam(), employeeTeamUpdateDs.getEmployeeIdentificatorList(),
				employeeTeamUpdateDs.getManagerIdentificator());
	}

	@Override
	public void saveOrUpdate(TeamDTO team) {

		teamService.saveTeam(team);
	}

	@Override
	public void saveOrUpdate(List<TeamDTO> teams) {

		teamService.saveTeams(teams);
		
	}

}

package pl.kt.myteamtime.facade;

import org.springframework.beans.factory.annotation.Autowired;
import pl.kt.myteamtime.dts.LoggedInUserIdentificatorDts;
import pl.kt.myteamtime.dts.UserDataDts;
import pl.kt.myteamtime.dts.UserRoleListDts;
import pl.kt.myteamtime.secService.UserService;

public interface UserFacade {



   public UserRoleListDts loadUserRoles(LoggedInUserIdentificatorDts loggedInUserIdentificator);

    UserDataDts loadUserData(LoggedInUserIdentificatorDts loggedInUserIdentificator);
}

package pl.kt.myteamtime.facade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.ds.ProjectsAndTimeForEmployeeDs;
import pl.kt.myteamtime.ds.SingleProjectAndHoursEmployeeDs;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dts.StatisticsComboDiagramDts;
import pl.kt.myteamtime.dts.StatisticsComboDiagramRetrieveDts;
import pl.kt.myteamtime.service.EmployeeService;
import pl.kt.myteamtime.service.ExcelService;
import pl.kt.myteamtime.service.ProjectEmployeeMaxHoursService;
import pl.kt.myteamtime.service.ProjectService;

@Service
public class StatisticsComboFacadeImpl implements StatisticsComboFacade {

    @Autowired
    private StatisticsComboMaintenanceFacade statisticsComboMaintenanceFacade;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private ProjectService projectService;

    @Autowired
    private ExcelService excelService;

    @Autowired
    private ProjectEmployeeMaxHoursService projectEmployeeMaxHoursService;

    @Override
    public StatisticsComboDiagramDts loadTabData(StatisticsComboDiagramRetrieveDts statisticsComboDiagramRetrieveDts) {

        List<String> employeeIdentificators = statisticsComboDiagramRetrieveDts.getEmployeeIdentificators();
        List<String> projectNumbers = statisticsComboDiagramRetrieveDts.getProjectNumbers();
        int weekFrom = statisticsComboDiagramRetrieveDts.getWeekFrom();
        int weekTo = statisticsComboDiagramRetrieveDts.getWeekTo();
        int yearFrom = statisticsComboDiagramRetrieveDts.getYearFrom();
        int yearTo = statisticsComboDiagramRetrieveDts.getYearTo();

        StatisticsComboDiagramDts statisticsComboDiagramDts = new StatisticsComboDiagramDts();

        List<ProjectsAndTimeForEmployeeDs> listOfAssignedProjectsWithHoursToEmployee = new ArrayList<ProjectsAndTimeForEmployeeDs>();

        for (String employeeIdentificator : employeeIdentificators) {

            ProjectsAndTimeForEmployeeDs projectsAndTimeForEmployeeDs = new ProjectsAndTimeForEmployeeDs();
            List<SingleProjectAndHoursEmployeeDs> listOfAssignedHoursForProjectAndEmployee = new ArrayList<SingleProjectAndHoursEmployeeDs>();
            for (String projectNumber : projectNumbers) {

                SingleProjectAndHoursEmployeeDs hoursForProjectAndEmployee = new SingleProjectAndHoursEmployeeDs();
                long hoursUsed = statisticsComboMaintenanceFacade.getHoursForGivenProjectNumberAndEmployeeIdentificator(
                        employeeIdentificator, projectNumber, weekFrom, weekTo, yearFrom, yearTo);
                hoursForProjectAndEmployee.setHoursUsed(hoursUsed);

                ProjectDTO project = projectService.findProjectByProjectNumberDTO(projectNumber);
                hoursForProjectAndEmployee.setProject(project);
                listOfAssignedHoursForProjectAndEmployee.add(hoursForProjectAndEmployee);
                List<EmployeeDTO> employees = new ArrayList<EmployeeDTO>();
                employees.add(employeeService.findByEmployeeIdentificatorDTO(employeeIdentificator));
                hoursForProjectAndEmployee.setHoursLeft(projectEmployeeMaxHoursService
                        .findMaxHoursForProjectAndEmployee(projectService.findProjectByProjectNumber(projectNumber),
                                employeeService.findByEmployeeIdentificator(employeeIdentificator))- hoursUsed);
            }

            projectsAndTimeForEmployeeDs
                    .setEmployee(employeeService.findByEmployeeIdentificatorDTO(employeeIdentificator));
            projectsAndTimeForEmployeeDs.setProjectsWithHours(listOfAssignedHoursForProjectAndEmployee);
            listOfAssignedProjectsWithHoursToEmployee.add(projectsAndTimeForEmployeeDs);
        }

        statisticsComboDiagramDts.setProjectsAndTimeForEmployeeDs(listOfAssignedProjectsWithHoursToEmployee);
        return statisticsComboDiagramDts;
    }

    @Override
    public void exportToExcel(StatisticsComboDiagramDts statisticsComboDiagramDts, int weekFrom, int weekTo, int yearFrom, int yearTo) {

        excelService.prepareComboGraph(statisticsComboDiagramDts.getProjectsAndTimeForEmployeeDs(), weekFrom, weekTo, yearFrom, yearTo);

    }

}

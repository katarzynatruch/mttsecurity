package pl.kt.myteamtime.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.dts.EmployeesTimeReportNotConfirmedDts;
import pl.kt.myteamtime.dts.EmployeesWorkLogConfirmedDts;
import pl.kt.myteamtime.service.DataAndTimeService;
import pl.kt.myteamtime.service.EmailService;
import pl.kt.myteamtime.service.ReminderMessageService;
import pl.kt.myteamtime.service.WorkLogService;

@Service
public class EmployeeWorkLogConfirmedFacadeImpl implements EmployeeWorkLogConfirmedFacade {

    @Autowired
    DataAndTimeService dataAndTimeService;


    @Autowired
    WorkLogService workLogService;

    @Autowired
    EmailService emailService;

    @Autowired
    ReminderMessageService reminderMessageService;

    @Override
    public EmployeesWorkLogConfirmedDts loadList(String managerIdentificator) {
        EmployeesWorkLogConfirmedDts employeesWorkLogConfirmedDts = new EmployeesWorkLogConfirmedDts();
        employeesWorkLogConfirmedDts.setCurrentWeek(dataAndTimeService.getCurrentWeekGlobal());
        employeesWorkLogConfirmedDts.setCurrentYear(dataAndTimeService.getCurrenYearGlobal());
        employeesWorkLogConfirmedDts.setEmployeesWithWorklogs(workLogService.findEmployeesWithWorklogsByConfirmedWorkLog(managerIdentificator));
        employeesWorkLogConfirmedDts.setReminderMessageDTO(reminderMessageService.findDefaultMessage(managerIdentificator));
        return employeesWorkLogConfirmedDts;
    }

    @Override
    public EmployeesTimeReportNotConfirmedDts loadListOfLazyEmployees(String managerIdentificator) {
        EmployeesTimeReportNotConfirmedDts employeesWorkLogNotConfirmedDts = new EmployeesTimeReportNotConfirmedDts();
        employeesWorkLogNotConfirmedDts.setCurrentWeek(dataAndTimeService.getCurrentWeekGlobal());
        employeesWorkLogNotConfirmedDts.setCurrentYear(dataAndTimeService.getCurrenYearGlobal());
        employeesWorkLogNotConfirmedDts.setEmployees(workLogService.findEmployeesByNotConfirmedWorkLog(managerIdentificator));
        employeesWorkLogNotConfirmedDts.setReminderMessageDTO(reminderMessageService.findDefaultMessage(managerIdentificator));
        return employeesWorkLogNotConfirmedDts;
    }


}

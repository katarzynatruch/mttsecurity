package pl.kt.myteamtime.facade;

import pl.kt.myteamtime.dts.EmployeesTimeReportNotConfirmedDts;
import pl.kt.myteamtime.dts.EmployeesWorkLogConfirmedDts;

public interface EmployeeWorkLogConfirmedFacade {

    EmployeesWorkLogConfirmedDts loadList(String managerIdentificator);


    EmployeesTimeReportNotConfirmedDts loadListOfLazyEmployees(String managerIdentificator);
}

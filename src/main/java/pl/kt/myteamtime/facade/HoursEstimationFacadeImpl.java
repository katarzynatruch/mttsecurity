package pl.kt.myteamtime.facade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.ds.HoursConsumptionForEmployees;
import pl.kt.myteamtime.ds.HoursConsumptionPerWeekDs;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dts.HoursEstimationDts;
import pl.kt.myteamtime.dts.HoursEstimationRetrieveDts;
import pl.kt.myteamtime.service.*;

@Service
public class HoursEstimationFacadeImpl implements HoursEstimationFacade {

	@Autowired
	ProjectService projectService;
	@Autowired
	ExcelService excelService;
	@Autowired
	private DataAndTimeService dataAndTime;
	@Autowired
	private WorkLogService workLogService;

	@Autowired
	private ProjectEmployeeMaxHoursService projectEmployeeMaxHoursService;

	@Autowired
	private EmployeeService employeeService;

	@Override
	public HoursEstimationDts loadTabView(HoursEstimationRetrieveDts hoursEstimationRetrieveDts) {

		HoursEstimationDts hoursEstimationDts = new HoursEstimationDts();
		hoursEstimationDts.setCurrentWeek(dataAndTime.getCurrentWeekGlobal());
		hoursEstimationDts.setCurrentYear(dataAndTime.getCurrenYearGlobal());
		hoursEstimationDts.setListOfHoursConsumptionPerWeekDs(calculateForHourConsumption(hoursEstimationRetrieveDts));

		return hoursEstimationDts;
	}

	@Override
	public void exportToExcel(HoursEstimationDts hoursEstimationDts, int weekFrom, int yearFrom, int weekTo,
			int yearTo) {
		excelService.prepareHoursEstimationChart(hoursEstimationDts.getListOfHoursConsumptionPerWeekDs(), weekFrom,
				yearFrom, weekTo, yearTo);
	}

	private List<HoursConsumptionPerWeekDs> calculateForHourConsumption(
			HoursEstimationRetrieveDts hoursEstimationRetrieveDts) {

		List<HoursConsumptionPerWeekDs> hoursConsumptionPerWeekDsList = new ArrayList<>();
		int numberOfWeeks = dataAndTime.numberOfWeeksFromDateToDate(hoursEstimationRetrieveDts.getWeekFrom(),
				hoursEstimationRetrieveDts.getYearFrom(), hoursEstimationRetrieveDts.getWeekTo(),
				hoursEstimationRetrieveDts.getYearTo());

		for (String projectNumber : hoursEstimationRetrieveDts.getProjectNumbers()) {

			int week = hoursEstimationRetrieveDts.getWeekFrom();
			int year = hoursEstimationRetrieveDts.getYearFrom();
			HoursConsumptionPerWeekDs hoursConsumptionPerWeekDs = new HoursConsumptionPerWeekDs();
			hoursConsumptionPerWeekDs.setProjectEmployeeMaxHoursDTOList(
					projectEmployeeMaxHoursService.calculateMaxHoursForProjectAndEmployees(projectNumber,
							hoursEstimationRetrieveDts.getEmployeeIdentificators()));

			hoursConsumptionPerWeekDs.setProject(projectService.findProjectByProjectNumberDTO(projectNumber));
			List<HoursConsumptionForEmployees> hoursConsumptionForEmployeesList = new ArrayList<>();
			hoursConsumptionPerWeekDs.setHoursConsumptionForEmployees(hoursConsumptionForEmployeesList);
			long sumOfCalculatedHours = 0;
			for (int i = 0; i < numberOfWeeks; i++) {

				HoursConsumptionForEmployees hoursConsumptionForEmployees = new HoursConsumptionForEmployees();

				List<EmployeeDTO> employees = new ArrayList<>();
				for (String empIden : hoursEstimationRetrieveDts.getEmployeeIdentificators()) {
					employees.add(employeeService.findByEmployeeIdentificatorDTO(empIden));
				}

				long calculatedHoursForWeek = workLogService.calculateHoursForProject(week, year, employees,
						projectService.findProjectByProjectNumberDTO(projectNumber));
				hoursConsumptionForEmployees.setEmployees(employees);
				hoursConsumptionForEmployees.setUsedHours(sumOfCalculatedHours += calculatedHoursForWeek);
				hoursConsumptionForEmployees.setWeek(week);
				hoursConsumptionForEmployees.setYear(year);

				if (week < dataAndTime.getMaxWeekOfTheYear(year)) {
					week++;
				} else {

					year++;
					week = 1;
				}

				hoursConsumptionForEmployeesList.add(hoursConsumptionForEmployees);

			}
			hoursConsumptionPerWeekDsList.add(hoursConsumptionPerWeekDs);
		}

		return hoursConsumptionPerWeekDsList;
	}

}

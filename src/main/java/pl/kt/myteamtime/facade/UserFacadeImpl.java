package pl.kt.myteamtime.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kt.myteamtime.dts.LoggedInUserIdentificatorDts;
import pl.kt.myteamtime.dts.UserDataDts;
import pl.kt.myteamtime.dts.UserRoleListDts;
import pl.kt.myteamtime.entity.Role;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.secService.UserService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserFacadeImpl implements UserFacade {
    @Autowired
    private UserService userService;

    @Override
    public UserRoleListDts loadUserRoles(LoggedInUserIdentificatorDts loggedInUserIdentificator) {
        UserRoleListDts userRoleListDts = new UserRoleListDts();
        userRoleListDts.setUserRoleNameList(retrieveRoles(loggedInUserIdentificator).stream()
                .map(e -> e.getName()).collect(Collectors.toList()));
        return userRoleListDts;
    }

    @Override
    public UserDataDts loadUserData(LoggedInUserIdentificatorDts loggedInUserIdentificator) {
        User user = userService.findUserByIdentificator(loggedInUserIdentificator.getLoggedInUserIdentificator());
        UserDataDts userDataDts = new UserDataDts();
        userDataDts.setIdentificator(user.getIdentificator());
        userDataDts.setFirstName(user.getFirstName());
        userDataDts.setLastName(user.getLastName());
        return userDataDts;
    }

    private List<Role> retrieveRoles(LoggedInUserIdentificatorDts loggedInUserIdentificator) {
        return userService.findUserByIdentificator(loggedInUserIdentificator.getLoggedInUserIdentificator()).getRoles();
    }
}

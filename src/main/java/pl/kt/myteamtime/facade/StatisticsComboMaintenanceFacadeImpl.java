package pl.kt.myteamtime.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.dto.WorkLogDTO;
import pl.kt.myteamtime.service.WorkLogService;

@Service
public class StatisticsComboMaintenanceFacadeImpl implements StatisticsComboMaintenanceFacade {

	@Autowired
	WorkLogService workLogService;

	public int getHoursForGivenProjectNumberAndEmployeeIdentificator(String employeeIdentificator, String projectNumber,
			int weekFrom, int weekTo, int yearFrom, int yearTo) {

		int sumHours = 0;
		List<WorkLogDTO> workLogs = workLogService.findWorkLogByEmployeeIdentificatorAndProjectNumber(
				employeeIdentificator, projectNumber, weekFrom, weekTo, yearFrom, yearTo);

		for (WorkLogDTO workLog : workLogs) {

		
						sumHours = (int) (sumHours + workLog.getWorkedHours());
		}

		return sumHours;
	}

}

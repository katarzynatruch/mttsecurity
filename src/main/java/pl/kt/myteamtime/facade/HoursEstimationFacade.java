package pl.kt.myteamtime.facade;

import pl.kt.myteamtime.dts.HoursEstimationDts;
import pl.kt.myteamtime.dts.HoursEstimationRetrieveDts;

public interface HoursEstimationFacade {

	HoursEstimationDts loadTabView(HoursEstimationRetrieveDts hoursEstimationRetrieveDts);

    void exportToExcel(HoursEstimationDts hoursEstimationDts, int weekFrom, int yearFrom, int weekTo, int yearTo);
}

package pl.kt.myteamtime.facade;

import pl.kt.myteamtime.ds.EmployeeProjectMaxHoursDs;
import pl.kt.myteamtime.ds.ProjectAndEmployeeDs;
import pl.kt.myteamtime.dts.*;

public interface ProjectManagementFacade {

	ProjectListDts loadTabData();

	void saveProject(ProjectDts projectDts);

	void updateProject(ProjectDts projectDts);

	ProjectManagementListDts loadProjectAndEmployeeData(String managerIdentificator);

	ListsOfEmployeeProjectCombinationDts loadEmployeeProjectCombinatedLists(String managerIdentificator);

	void saveOrUpdateAssignedProjectsToEmployee(ListsOfEmployeeProjectDts listsOfEmployeeProjectDts);

	void delete(String projectNumber);

    void saveOrUpdateAssignedEmployeesToProject(ListsOfProjectEmployeeDts listsOfProjectEmployeeDts);

	void deleteProjectFromList(ProjectAndEmployeeDs projectAndEmployeeDs);

	void saveSingleEmployeeWithProjectsAndMaxHours(EmployeeProjectMaxHoursDs employeeProjectMaxHoursDs);

	Long loadMaxHours(String employeeIdentificator, String projectNumber);

	ProjectNewData loadDataNewProject();

}

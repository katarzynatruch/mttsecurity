package pl.kt.myteamtime.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kt.myteamtime.dto.UserDTO;
import pl.kt.myteamtime.dts.UserDts;
import pl.kt.myteamtime.secService.UserService;

import java.util.List;

@Service
public class AdminFacadeImpl implements AdminFacade {

    @Autowired
    UserService userService;

    @Override
    public UserDts findAllUsers() {

        List<UserDTO> users = userService.findAll();
        UserDts userDts = new UserDts();
        userDts.setUsers(users);

        return userDts;
    }
}

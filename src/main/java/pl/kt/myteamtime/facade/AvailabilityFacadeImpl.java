package pl.kt.myteamtime.facade;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.ds.AvailabilitySingleEmployeeDs;
import pl.kt.myteamtime.ds.AvailabilitySingleEmployeeUpdateDs;
import pl.kt.myteamtime.dto.AvailabilityDTO;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dts.AvailabilityForManagerDts;
import pl.kt.myteamtime.dts.FilterDataRetrieveDts;
import pl.kt.myteamtime.service.AvailabilityService;
import pl.kt.myteamtime.service.EmployeeService;

@Service
public class AvailabilityFacadeImpl implements AvailabilityFacade {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    AvailabilityService availabilityService;

    @Override
    public AvailabilityForManagerDts loadLogs(String managerIdentificator, FilterDataRetrieveDts filterDataRetrieveDts) {
        AvailabilityForManagerDts availabilityForManagerDts = new AvailabilityForManagerDts();
        List<AvailabilitySingleEmployeeDs> availabilitySingleEmployeeDsList = new ArrayList<>();
        List<EmployeeDTO> employees = employeeService.findEmployeeByManagerIdentificator(managerIdentificator).stream()
                .distinct().collect(Collectors.toList());
        List<EmployeeDTO> filteredEmployees = employees.stream().filter(e -> filterDataRetrieveDts.getEmployeeIdentificators().contains(e.getIdentificator()))
                .collect(Collectors.toList());

        for (EmployeeDTO employee : filteredEmployees) {

            AvailabilitySingleEmployeeDs availabilitySingleEmployeeDs = new AvailabilitySingleEmployeeDs();
            availabilitySingleEmployeeDs.setEmployee(employee);
            List<AvailabilityDTO> availabilityList = availabilityService.findByEmployeeAndData(employee, filterDataRetrieveDts);
            availabilitySingleEmployeeDs.setAvailability(availabilityList);

            availabilitySingleEmployeeDsList.add(availabilitySingleEmployeeDs);
        }
        availabilityForManagerDts.setAvailabilitySingleEmployeeDsList(availabilitySingleEmployeeDsList);
        return availabilityForManagerDts;
    }
    @Override
    public AvailabilityForManagerDts loadLogs(String managerIdentificator) {
        AvailabilityForManagerDts availabilityForManagerDts = new AvailabilityForManagerDts();
        List<AvailabilitySingleEmployeeDs> availabilitySingleEmployeeDsList = new ArrayList<>();
        List<EmployeeDTO> employees = employeeService.findEmployeeByManagerIdentificator(managerIdentificator).stream()
                .distinct().collect(Collectors.toList());

        for (EmployeeDTO employee : employees) {

            AvailabilitySingleEmployeeDs availabilitySingleEmployeeDs = new AvailabilitySingleEmployeeDs();
            availabilitySingleEmployeeDs.setEmployee(employee);
            List<AvailabilityDTO> availabilityList = availabilityService.findByEmployee(employee);
            availabilitySingleEmployeeDs.setAvailability(availabilityList);

            availabilitySingleEmployeeDsList.add(availabilitySingleEmployeeDs);
        }
        availabilityForManagerDts.setAvailabilitySingleEmployeeDsList(availabilitySingleEmployeeDsList);
        return availabilityForManagerDts;
    }

    @Override
    public void saveLogs(AvailabilitySingleEmployeeUpdateDs availabilitySingleEmployeeUpdateDs) {
        availabilityService.save(availabilitySingleEmployeeUpdateDs);
    }

}

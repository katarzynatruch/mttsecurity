package pl.kt.myteamtime.facade;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.ds.*;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dto.ProjectEmployeeMaxHoursDTO;
import pl.kt.myteamtime.dts.*;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.enums.ProjectMilestone;
import pl.kt.myteamtime.enums.ProjectType;
import pl.kt.myteamtime.service.EmployeeService;
import pl.kt.myteamtime.service.ProjectEmployeeMaxHoursService;
import pl.kt.myteamtime.service.ProjectService;

@Service
public class ProjectManagementFacadeImpl implements ProjectManagementFacade {

    @Autowired
    ProjectService projectService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    ProjectEmployeeMaxHoursService projectEmployeeMaxHoursService;

    @Override
    public ProjectListDts loadTabData() {
        return projectService.loadTabData();
    }

    @Override
    public void saveProject(ProjectDts projectDts) {
        ProjectDTO projectDTO = getProjectDTO(projectDts);
        projectService.save(projectDTO);
    }

    @Override
    public void updateProject(ProjectDts projectDts) {
        ProjectDTO projectDTO = getProjectDTO(projectDts);
        projectService.update(projectDTO);
    }

    private ProjectDTO getProjectDTO(ProjectDts projectDts) {
        ProjectDTO projectDTO = new ProjectDTO();

        if (projectDts.getCostCenter() != null)
            projectDTO.setCostCenter(projectDts.getCostCenter());
        if (projectDts.getDeadlines() != null)
            projectDTO.setDeadlines(projectDts.getDeadlines());
        if (projectDts.getId() != null)
            projectDTO.setId(projectDts.getId());
        if (projectDts.getMaxHours() != null)
            projectDTO.setMaxHours(projectDts.getMaxHours());
        if (projectDts.getName() != null)
            projectDTO.setName(projectDts.getName());
        if (projectDts.getNumber() != null)
            projectDTO.setNumber(projectDts.getNumber());
        if (projectDts.getType() != null)
            projectDTO.setType(projectDts.getType());
        return projectDTO;
    }

    @Override
    public ProjectManagementListDts loadProjectAndEmployeeData(String managerIdentificator) {
        List<EmployeeDTO> employees = employeeService.findEmployeeByManagerIdentificator(managerIdentificator);
        List<ProjectDTO> projects = projectService.findAll();
        ProjectManagementListDts projectManagementListDts = new ProjectManagementListDts();
        projectManagementListDts.setEmployees(employees);
        projectManagementListDts.setProjects(projects);

        return projectManagementListDts;
    }

    @Override
    public ListsOfEmployeeProjectCombinationDts loadEmployeeProjectCombinatedLists(String managerIdentificator) {
        List<ProjectWithEmployeeListDs> projectsAndItsEmployees = projectService
                .findProjectWithEmployees(managerIdentificator);
        List<EmployeeWithProjectListDs> employeesAndTheirProjects = employeeService
                .findEmployeeWithProjects(managerIdentificator);
        ArrayList<ProjectEmployeeMaxHoursDTO> maxHours = new ArrayList<>();
        for (EmployeeWithProjectListDs employeeAndProjects : employeesAndTheirProjects) {
            for (ProjectDTO project : employeeAndProjects.getProjects()) {
                ProjectEmployeeMaxHoursDTO projectEmployeeMaxHoursDTO = new ProjectEmployeeMaxHoursDTO();
                projectEmployeeMaxHoursDTO.setProject(project);
                projectEmployeeMaxHoursDTO.setEmployee(employeeAndProjects.getEmployee());
                projectEmployeeMaxHoursDTO.setMaxHours(projectEmployeeMaxHoursService.findMaxHoursForProjectAndEmployee(project, employeeAndProjects.getEmployee()));
                maxHours.add(projectEmployeeMaxHoursDTO);
            }
        }
        ListsOfEmployeeProjectCombinationDts listsOfEmployeeProjectCombinationDts = new ListsOfEmployeeProjectCombinationDts();
        listsOfEmployeeProjectCombinationDts.setEmployeeWithProjectListDs(employeesAndTheirProjects.stream().distinct().collect(Collectors.toList()));
        listsOfEmployeeProjectCombinationDts.setProjectWithEmployeeListDs(projectsAndItsEmployees.stream().distinct().collect(Collectors.toList()));
        listsOfEmployeeProjectCombinationDts.setMaxHoursDs(maxHours);

        return listsOfEmployeeProjectCombinationDts;
    }

    @Override
    public void saveOrUpdateAssignedProjectsToEmployee(ListsOfEmployeeProjectDts listsOfEmployeeProjectDts) {
        List<EmployeeProjectList> employeeProjectList = listsOfEmployeeProjectDts.getEmployeeProjectList();
        for (EmployeeProjectList employeeCombination : employeeProjectList) {
            employeeService.saveEmployeeProjectRelations(employeeCombination.getEmployeeIdentificator(),
                    employeeCombination.getProjectNumberWithMaxHoursDsList());
        }
    }

    @Override
    public void delete(String projectNumber) {
        projectService.delete(projectNumber);
    }

    @Override
    public void saveOrUpdateAssignedEmployeesToProject(ListsOfProjectEmployeeDts listsOfProjectEmployeeDts) {
        List<ProjectEmployeeList> projectEmployeeList = listsOfProjectEmployeeDts.getProjectEmployeesList();
        for (ProjectEmployeeList projectCombination : projectEmployeeList) {
            projectService.saveProjectEmployeeRelations(projectCombination.getProjectNumber(),
                    projectCombination.getEmployeeIdentificators());
        }
    }

    @Override
    public void deleteProjectFromList(ProjectAndEmployeeDs projectAndEmployeeDs) {
        Employee employee = employeeService.findByEmployeeIdentificator(projectAndEmployeeDs.getEmployeeIdentificator());
        Project project = projectService.findProjectByProjectNumber(projectAndEmployeeDs.getProjectNumber());
        projectEmployeeMaxHoursService.deleteProjectEmployeeRelation(project, employee);
    }

    @Override
    public void saveSingleEmployeeWithProjectsAndMaxHours(EmployeeProjectMaxHoursDs employeeProjectMaxHoursDs) {
        Project project = projectService.findProjectByProjectNumber(employeeProjectMaxHoursDs.getProjectNumber());
        Employee employee = employeeService.findByEmployeeIdentificator(employeeProjectMaxHoursDs.getEmployeeIdentificator());
        projectEmployeeMaxHoursService.save(project, employee, employeeProjectMaxHoursDs.getMaxHours());
    }

    @Override
    public Long loadMaxHours(String employeeIdentificator, String projectNumber) {
        Project project = projectService.findProjectByProjectNumber(projectNumber);
        Employee employee = employeeService.findByEmployeeIdentificator(employeeIdentificator);
        return projectEmployeeMaxHoursService.findMaxHoursForProjectAndEmployee(project, employee);
    }

    @Override
    public ProjectNewData loadDataNewProject() {
        ProjectNewData projectNewData = new ProjectNewData();
        List<String> costCenters = projectService.findAllCostCenters();
        projectNewData.setMilestones(ProjectMilestone.values());
        projectNewData.setCostCenters(costCenters);
        projectNewData.setTypes(ProjectType.values());
        return projectNewData;
    }


}

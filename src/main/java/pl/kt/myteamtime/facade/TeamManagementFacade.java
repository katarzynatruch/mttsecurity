package pl.kt.myteamtime.facade;

import java.util.List;

import pl.kt.myteamtime.dto.TeamDTO;
import pl.kt.myteamtime.dts.EmployeeTeamUpdateDts;
import pl.kt.myteamtime.dts.TeamManagementListDts;

public interface TeamManagementFacade {

	public TeamManagementListDts loadTabData(String managerIdentificator);

    void saveOrUpdateData(EmployeeTeamUpdateDts employeeTeamUpdateDsList);

    void updateDataAfterDeleteTeams(EmployeeTeamUpdateDts employeeTeamUpdateDsList);

    public void saveOrUpdate(TeamDTO team);

	public void saveOrUpdate(List<TeamDTO> teams);
	
	

}

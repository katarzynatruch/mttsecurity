package pl.kt.myteamtime.facade;

import pl.kt.myteamtime.dts.UserDts;

public interface AdminFacade {
    UserDts findAllUsers();

}

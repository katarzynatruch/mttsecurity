package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pl.kt.myteamtime.dts.*;
import pl.kt.myteamtime.facade.EmployeeTimeReportFacade;
import pl.kt.myteamtime.facade.EmployeeWorkLogConfirmedFacade;
import pl.kt.myteamtime.service.EmailService;
import pl.kt.myteamtime.service.ReminderMessageService;

// DLG_Lazy_Employees
@RestController
@RequestMapping("/lazy-employee")
public class LazyEmployeesController {

    @Autowired
    EmployeeTimeReportFacade employeeTimeReportFacade;

    @Autowired
    EmployeeWorkLogConfirmedFacade employeeWorkLogConfirmedFacade;

    @Autowired
    EmailService emailService;

    @Autowired
    ReminderMessageService reminderMessageService;

    @GetMapping("/employee-workLog-confirmed/{managerIdentificator}")
    public EmployeesWorkLogConfirmedDts loadList(@PathVariable String managerIdentificator) {
        return employeeWorkLogConfirmedFacade
                .loadList(managerIdentificator);
    }

    @GetMapping("/employee-workLog-not-confirmed/{managerIdentificator}")
    public EmployeesTimeReportNotConfirmedDts loadListOfLazyEmployees(@PathVariable String managerIdentificator) {
        return employeeWorkLogConfirmedFacade
                .loadListOfLazyEmployees(managerIdentificator);
    }

    @PostMapping("/sending/")
    public void sendReminder(@RequestBody EmployeesReminderDts employeesReminderDts) {
        emailService.sendMessageToGroup(employeesReminderDts.getEmployeeIdentificators(), employeesReminderDts.getMessage());
    }

    @PostMapping("/change-default/")
    public void changeDefaultMessage(@RequestBody DefaultReminderMessageDts defaultReminderMessageDts) {
        reminderMessageService.changeDefaultMessage(defaultReminderMessageDts.getManagerIdentificator(),
                defaultReminderMessageDts.getReminderMessageDTO());

    }

    @PostMapping("/timeReport/approved-by-manager")
    public void approveWorkLogsByManager(@RequestBody ApproveWorkLogsByEmployeeDts approveWorkLogsByEmployeeDts) {
        employeeTimeReportFacade.approveWorkLogsByManager(approveWorkLogsByEmployeeDts);
    }
}

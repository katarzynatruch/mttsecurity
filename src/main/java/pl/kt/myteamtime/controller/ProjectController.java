package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pl.kt.myteamtime.dts.ProjectDts;
import pl.kt.myteamtime.dts.ProjectNewData;
import pl.kt.myteamtime.facade.ProjectManagementFacade;

//DLG_Project_New
@RestController
@RequestMapping("/new-project")
public class ProjectController {

	@Autowired
	private ProjectManagementFacade projectManagementFacade;

	@PostMapping("/add")
	public void saveProject(@RequestBody ProjectDts projectDts) {
		projectManagementFacade.saveProject(projectDts);
	}

	@PostMapping("/override")
	public void updateProject(@RequestBody ProjectDts projectDts) {
		projectManagementFacade.updateProject(projectDts);
	}

	@PostMapping("/deleteProject")
	public void  deleteProject(@PathVariable String projectNumber){
		projectManagementFacade.delete(projectNumber);
	}

	@GetMapping("/loadData")
	public ProjectNewData loadData() {
		ProjectNewData p = projectManagementFacade.loadDataNewProject();
		return projectManagementFacade.loadDataNewProject();
	}

}

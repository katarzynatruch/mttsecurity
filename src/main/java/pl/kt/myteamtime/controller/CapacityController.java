package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pl.kt.myteamtime.dts.CapacityDts;
import pl.kt.myteamtime.dts.FilterRetrieveDts;
import pl.kt.myteamtime.dts.GraphFilterListDts;
import pl.kt.myteamtime.facade.CapacityFacade;

//DLG_Capacity
@RestController
@RequestMapping("/statistics")
public class CapacityController {

	
	@Autowired
	private
	CapacityFacade capacityFacade;	
	
	
	@PostMapping("/capacityGraph")
	public CapacityDts loadGraph(@RequestBody FilterRetrieveDts filterRetrieveDts) {
		
		return capacityFacade.loadGraph(filterRetrieveDts);
		
	}
	
	@GetMapping("/filters/{managerIdentificator}")
	public GraphFilterListDts loadFilters(@PathVariable String managerIdentificator) {
		
		return capacityFacade.loadFilters(managerIdentificator);
	}

	@PostMapping("/export-to-excel-capacity")
	public void exportToExcel(@RequestBody FilterRetrieveDts filterRetrieveDts){
		CapacityDts capacityDts = capacityFacade.loadGraph(filterRetrieveDts);
		capacityFacade.exportToExcel(capacityDts);
	}
	
}

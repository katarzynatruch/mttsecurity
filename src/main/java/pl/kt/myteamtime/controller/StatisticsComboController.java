package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pl.kt.myteamtime.dts.StatisticsComboDiagramDts;
import pl.kt.myteamtime.dts.StatisticsComboDiagramRetrieveDts;
import pl.kt.myteamtime.facade.StatisticsComboFacade;

//DLG_Statistics_Combo
@RestController
@RequestMapping("/statistics")
public class StatisticsComboController {

	@Autowired
	private StatisticsComboFacade statisticsComboFacade;

	@PostMapping("/comboDiagram")
	StatisticsComboDiagramDts loadDiagram(
			@RequestBody StatisticsComboDiagramRetrieveDts statisticsComboDiagramRetrieveDts) {

		return statisticsComboFacade.loadTabData(statisticsComboDiagramRetrieveDts);
	}

	@PostMapping("/export-to-excel-combo")
	public void exportToExcel(
			@RequestBody StatisticsComboDiagramRetrieveDts statisticsComboDiagramRetrieveDts){

		StatisticsComboDiagramDts statisticsComboDiagramDts = statisticsComboFacade.loadTabData(statisticsComboDiagramRetrieveDts);

		statisticsComboFacade.exportToExcel(statisticsComboDiagramDts, statisticsComboDiagramRetrieveDts.getWeekFrom(),
				statisticsComboDiagramRetrieveDts. getWeekTo(), statisticsComboDiagramRetrieveDts.getYearFrom(),
				statisticsComboDiagramRetrieveDts.getYearTo());

	}

}

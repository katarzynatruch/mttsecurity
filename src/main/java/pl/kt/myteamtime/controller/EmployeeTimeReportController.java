package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pl.kt.myteamtime.dts.*;
import pl.kt.myteamtime.facade.EmployeeTimeReportFacade;
import pl.kt.myteamtime.facade.EmployeeWorkLogConfirmedFacade;

// DLG_Employee_Time_Report
@RestController
@RequestMapping("/employee")
public class EmployeeTimeReportController {

    @Autowired
    private EmployeeWorkLogConfirmedFacade employeeWorkLogConfirmedFacade;

    @Autowired
    private EmployeeTimeReportFacade employeeTimeReportFacade;

    @PostMapping(value = "/timeReport/score")
    public EmployeeTimeReportDts loadView(@RequestBody EmployeeTimeReportUpdateDts employeeTimeReportUpdateDts) {
        return employeeTimeReportFacade.loadTabData(employeeTimeReportUpdateDts);
    }

    @PostMapping("/timeReport/save")
    public void saveOrUpdateEmployeeTimeReport(@RequestBody EmployeeTimeReportSaveDts employeeTimeReportSaveDts) {
        employeeTimeReportFacade.saveOrUpdate(employeeTimeReportSaveDts);
    }

    @PostMapping("/timeReport/approved-by-emp")
    public void approveWorkLogsByEmployee(@RequestBody ApproveWorkLogsByEmployeeDts approveWorkLogsByEmployeeDts) {
        employeeTimeReportFacade.approveWorkLogsByEmployee(approveWorkLogsByEmployeeDts);
    }

    @PostMapping("/timeReport/delete-worklog")
    public void deleteWorkLog(@RequestBody WorkLogDeleteDts workLogDeleteDts) {
        employeeTimeReportFacade.deleteWorkLog(workLogDeleteDts);
    }

}

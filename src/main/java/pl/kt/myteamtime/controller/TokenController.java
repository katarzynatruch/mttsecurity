package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.token.TokenService;
import org.springframework.web.bind.annotation.*;
import pl.kt.myteamtime.controller.util.GenericResponse;
import pl.kt.myteamtime.dts.EmailDts;
import pl.kt.myteamtime.secEntity.ResetToken;
import pl.kt.myteamtime.secEntity.VerificationToken;
import pl.kt.myteamtime.secService.UserService;

import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/token")
public class TokenController {

    @Autowired
    private MessageSource messages;

    @Autowired
    private UserService userService;

    @GetMapping("/get-verification-token/{userIdentificator}")
    public String getVerificationToken(@PathVariable String userIdentificator) {
        VerificationToken verificationToken = userService.getVerificationTokenByUser(userIdentificator);
        if (verificationToken == null) {
            ResetToken resetToken = userService.getResetTokenByUser(userIdentificator);
            if (resetToken == null) {
                return null;
            }
            return resetToken.getToken();
        }
        return verificationToken.getToken();
    }
}

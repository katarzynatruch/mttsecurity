package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.kt.myteamtime.dts.LoggedInUserIdentificatorDts;
import pl.kt.myteamtime.dts.UserDataDts;
import pl.kt.myteamtime.dts.UserDts;
import pl.kt.myteamtime.dts.UserRoleListDts;
import pl.kt.myteamtime.entity.Role;
import pl.kt.myteamtime.facade.UserFacade;
import pl.kt.myteamtime.secService.UserService;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    UserFacade userFacade;

    @PostMapping("/get_roles")
    public UserRoleListDts getUserRole(@RequestBody LoggedInUserIdentificatorDts loggedInUserIdentificator) {
        return userFacade.loadUserRoles(loggedInUserIdentificator);
    }

    @PostMapping("/get_user_data")
    public UserDataDts getUserData(@RequestBody LoggedInUserIdentificatorDts loggedInUserIdentificator) {
        return userFacade.loadUserData(loggedInUserIdentificator);
    }

    @PostMapping("/get_id")
    public Long getUserId(@RequestBody LoggedInUserIdentificatorDts loggedInUserIdentificator) {
        return userService.findUserByIdentificator(loggedInUserIdentificator.getLoggedInUserIdentificator()).getId();
    }

    @GetMapping(value = "/load-list-all")
    public UserDts loadViewAll() {
        return userService.findAllNotDeactivated();
    }
}

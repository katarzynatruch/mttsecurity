package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.kt.myteamtime.dts.ProjectListDts;
import pl.kt.myteamtime.facade.ProjectManagementFacade;

//DLG_Project_List
@RestController
@RequestMapping("/project")
public class ProjectListController {

	@Autowired
	ProjectManagementFacade projectManagementFacade;
	
	@GetMapping("/plist")
	public ProjectListDts loadView() {
		
		ProjectListDts projectListDts = projectManagementFacade.loadTabData();
		
		return projectListDts;
		
	}
	
	
	
}

package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pl.kt.myteamtime.dts.HoursEstimationDts;
import pl.kt.myteamtime.dts.HoursEstimationRetrieveDts;
import pl.kt.myteamtime.facade.HoursEstimationFacade;

//DLG_Statistics_Hours_Estimation
@RestController
@RequestMapping("/statistics")
public class HoursEstimationController {

	@Autowired
	private HoursEstimationFacade hoursEstimationFacade;

	@PostMapping("/hours-estimation-diagram")
	public HoursEstimationDts loadDiagram(@RequestBody HoursEstimationRetrieveDts hoursEstimationRetrieveDts) {

		return hoursEstimationFacade.loadTabView(hoursEstimationRetrieveDts);

	}

	@PostMapping("/export-to-excel-estimation")
	public void exportToExcel(@RequestBody HoursEstimationRetrieveDts hoursEstimationRetrieveDts) {

		HoursEstimationDts hoursEstimationDts = hoursEstimationFacade.loadTabView(hoursEstimationRetrieveDts);
		hoursEstimationFacade.exportToExcel(hoursEstimationDts, hoursEstimationRetrieveDts.getWeekFrom(),
				hoursEstimationRetrieveDts.getYearFrom(), hoursEstimationRetrieveDts.getWeekTo(), hoursEstimationRetrieveDts.getYearTo());
	}
}

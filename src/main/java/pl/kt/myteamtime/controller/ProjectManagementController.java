package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pl.kt.myteamtime.ds.EmployeeProjectMaxHoursDs;
import pl.kt.myteamtime.ds.ProjectAndEmployeeDs;
import pl.kt.myteamtime.dts.ListsOfEmployeeProjectCombinationDts;
import pl.kt.myteamtime.dts.ListsOfEmployeeProjectDts;
import pl.kt.myteamtime.dts.ListsOfProjectEmployeeDts;
import pl.kt.myteamtime.dts.ProjectManagementListDts;
import pl.kt.myteamtime.facade.ProjectManagementFacade;

//DLG_Project_Management
@RestController
@RequestMapping("/project-management")
public class ProjectManagementController {

	@Autowired
	private ProjectManagementFacade projectManagementFacade;

	@GetMapping("/general-list/{managerIdentificator}")
	public ProjectManagementListDts loadView(@PathVariable String managerIdentificator) {
		return projectManagementFacade.loadProjectAndEmployeeData(managerIdentificator);
	}

	@GetMapping("/loadMaxHours/{employeeIdentificator}&{projectNumber}")
	public Long loadMaxHours(@PathVariable("employeeIdentificator") String employeeIdentificator, @PathVariable("projectNumber") String projectNumber) {
		Long maxHours = projectManagementFacade.loadMaxHours(employeeIdentificator, projectNumber);
		return projectManagementFacade.loadMaxHours(employeeIdentificator, projectNumber);
	}

	@GetMapping("/list/{managerIdentificator}")
	public ListsOfEmployeeProjectCombinationDts assignedProjectAndEmployee(@PathVariable String managerIdentificator) {
		return projectManagementFacade.loadEmployeeProjectCombinatedLists(managerIdentificator);
	}

	@PostMapping("/list/saveEmployee")
	public void saveOrUpdateAssignedProjects(@RequestBody ListsOfEmployeeProjectDts listsOfEmployeeProjectDts) {
		projectManagementFacade.saveOrUpdateAssignedProjectsToEmployee(listsOfEmployeeProjectDts);
	}

	@PostMapping("/list/saveSingleEmployee")
	public void saveSingleEmployeeWithProjectsAndMaxHours(@RequestBody EmployeeProjectMaxHoursDs employeeProjectMaxHoursDs) {
		projectManagementFacade.saveSingleEmployeeWithProjectsAndMaxHours(employeeProjectMaxHoursDs);
	}

	@PostMapping("/list/saveProject")
	public void saveOrUpdateAssignedEmployees(@RequestBody ListsOfProjectEmployeeDts listsOfProjectEmployeeDts) {
		projectManagementFacade.saveOrUpdateAssignedEmployeesToProject(listsOfProjectEmployeeDts);
	}

	@PostMapping("/deleteProject")
	public void  deleteProject(@PathVariable String projectNumber){
		projectManagementFacade.delete(projectNumber);
	}

	@PostMapping("/deleteProjectFromList")
	public void  deleteProjectFromList(@RequestBody ProjectAndEmployeeDs projectAndEmployeeDs){
		projectManagementFacade.deleteProjectFromList(projectAndEmployeeDs);
	}

}

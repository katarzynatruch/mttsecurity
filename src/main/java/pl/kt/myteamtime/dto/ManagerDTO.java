package pl.kt.myteamtime.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class ManagerDTO {

	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	private String managerIdentificator;

	// Relations

	@JsonIgnore
	private List<TeamDTO> teams;

}
package pl.kt.myteamtime.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;
import pl.kt.myteamtime.validation.PasswordMatches;
import pl.kt.myteamtime.validation.ValidPassword;

@Data
@PasswordMatches
public class UserValidDTO {

    @NotNull
    @NotEmpty
    private String firstName;

    @NotNull
    @NotEmpty
    private String lastName;

    @ValidPassword
    private String password;
    private String matchingPassword;

    @NotNull
    @NotEmpty
    private String identificator;

    @NotNull
    @NotEmpty
    private String email;

    @NotNull
    @NotEmpty
    private String role1;

    @NotNull
    @NotEmpty
    private String role2;

    @NotNull
    @NotEmpty
    private String role3;

    @NotNull
    @NotEmpty
    private String role4;

    @NotNull
    @NotEmpty
    private String role5;

    private boolean enabled;
    private boolean active;
}

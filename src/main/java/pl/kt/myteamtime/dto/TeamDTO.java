package pl.kt.myteamtime.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import pl.kt.myteamtime.entity.Manager;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.entity.WorkLog;

@Getter
@Setter
public class TeamDTO {

	private Long id;
	private String name;

	// Relations

//	@JsonIgnore
//	private List<WorkLog> workLogs;
	@JsonIgnore
	private User manager;
	@JsonIgnore
	private List<EmployeeDTO> employees;

}
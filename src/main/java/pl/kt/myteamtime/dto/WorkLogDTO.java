package pl.kt.myteamtime.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class WorkLogDTO {

	private Long id;
	private long workedHours;
	private long leftHours;
	private Integer week;
	private Integer year;
	private Boolean isConfirmedByEmployee;
	private Boolean isConfirmedByManager;

	// Relations

	@JsonIgnore
	private EmployeeDTO employee;
//	@JsonIgnore
	private ProjectDTO project;
//	@JsonIgnore
//	private TeamDTO team;

}
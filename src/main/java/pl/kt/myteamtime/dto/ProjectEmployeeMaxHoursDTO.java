package pl.kt.myteamtime.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectEmployeeMaxHoursDTO {

    //	private Long id;
    private long hoursUsed;
    private long maxHours;
    private ProjectDTO project;
    private EmployeeDTO employee;
}

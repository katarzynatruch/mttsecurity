package pl.kt.myteamtime.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;
import pl.kt.myteamtime.entity.Deactivated;

@Repository
public interface DeactivatedRepository extends JpaRepository<Deactivated, Long> {
}

package pl.kt.myteamtime.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.kt.myteamtime.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	@Query("select DISTINCT e from Employee e join e.teams t where t.id =:teamsId and e.isActive = true and e.deactivated=false")
	List<Employee> findActiveEmployeesByTeamsId(@Param("teamsId") Long teamsId);

	@Query("select DISTINCT e from Employee e where e.identificator =:employeeIdentificator and e.isActive = true and e.deactivated=false")
	Employee findActiveByIdentificator(@Param("employeeIdentificator") String employeeIdentificator);
	// Optional<Employee> findByIdentificator(String identificator);

	@Query("select DISTINCT e from Employee e where e.identificator =:employeeIdentificator and e.deactivated=false")
	Employee findByIdentificator(@Param("employeeIdentificator") String employeeIdentificator);

	@Query("select DISTINCT e.id from Employee e where e.identificator =:employeeIdentificator and e.isActive = true and e.deactivated=false")
	Long findIdByIdentificator(@Param("employeeIdentificator") String employeeIdentificator);

	@Query("select DISTINCT e from Employee e join e.teams t where t.manager.identificator=:managerIdentificator and e.isActive=true and e.deactivated=false")
	List<Employee> findActiveByManagerIdentificator(@Param("managerIdentificator") String managerIdentificator);

	@Query("select DISTINCT e from Employee e join e.teams t where t.manager.id=:managerId and e.deactivated=false")
	List<Employee> findActiveByManagerId(@Param("managerId") Long managerId);

//	@Query("select e from Employee e join e.teams t join e.projects p where t.manager.identificator =:managerIdentificator and p.id =:projectId and e.isActive = true")
	@Query("select DISTINCT e from Employee e join e.teams t join e.projects p where t.manager.identificator =:managerIdentificator and p.id =:projectId and e.isActive = true and e.deactivated=false")
	List<Employee> findActiveByProjectAndManagerIdentificator(@Param("projectId") Long id,
															  @Param("managerIdentificator") String managerIdentificator);

	@Query("select DISTINCT e from Employee e where e.id=:employeeId and e.isActive=true and e.deactivated=false")
	Optional<Employee> findActiveById(@Param("employeeId") Long employeeId);
}

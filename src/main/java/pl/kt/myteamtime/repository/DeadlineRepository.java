package pl.kt.myteamtime.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.kt.myteamtime.entity.Deadline;
import pl.kt.myteamtime.entity.Project;

import java.util.List;

@Repository
public interface DeadlineRepository extends JpaRepository <Deadline, Long>{

    @Query("select d from Deadline d where d.project=:project")
    List<Deadline> findAllByProjectId(@Param("project") Project project);
}
package pl.kt.myteamtime.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.kt.myteamtime.entity.Manager;
import pl.kt.myteamtime.entity.User;

@Repository
public interface ManagerRepository extends JpaRepository <User, Long>{

    @Query("select m from User m where m.identificator=:identificator")
    User findByIdentificator(@Param("identificator")String managerIdentificator);
}

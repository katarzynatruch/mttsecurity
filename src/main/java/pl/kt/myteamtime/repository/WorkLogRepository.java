package pl.kt.myteamtime.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.entity.WorkLog;

@Repository
public interface WorkLogRepository extends JpaRepository<WorkLog, Long> {

	@Query("select w from WorkLog w where w.employee.identificator=:employee and w.employee.deactivated=false")
	List<WorkLog> findByEmployeeIdentificator(@Param("employee") String employeeIdentificator);

	@Query("select w from WorkLog w where w.employee.identificator=:employeeIdentificator and w.project.number=:projectNumber and w.employee.deactivated=false")
	List<WorkLog> findByEmpIdentificatorAndProjectNumber(@Param("employeeIdentificator") String employeeIdentificator,
			@Param("projectNumber") String projectNumber);

	@Query("select w from WorkLog w where w.employee=:employee and w.project=:project and w.week=:week and w.year=:year and w.employee.deactivated=false")
	List<WorkLog> findByEmployeeProjectWeekAndYear(@Param("employee") Employee employee,
			@Param("project") Project project, @Param("week") int week, @Param("year") int year);

	@Query("select w from WorkLog w where w.employee=:employee and w.week=:week and w.year=:year and w.employee.deactivated=false")
	List<WorkLog> findByEmployeeWeekAndYear(@Param("employee") Employee employee, @Param("week") int week, @Param("year") int year);

//	@Query("select w from WorkLog w join w.employee e join e.teams t join t.manager m where " +
//			"m.identificator=:managerIdentificator and w.week=:week and w.year=:year")
//	List<WorkLog> findByConfirmationAndManager(@Param("managerIdentificator") String user,
//											   @Param("week") int week, @Param("year") int year);

	@Query("select w from WorkLog w join w.employee e join e.teams t where " +
			"t.manager=:user and w.week=:week and w.year=:year and w.employee.deactivated=false")
	List<WorkLog> findByConfirmationAndManager(@Param("user") User user,
											   @Param("week") int week, @Param("year") int year);

	@Query("select w from WorkLog w where w.id=:workLogId")
	WorkLog findByIdOwn(@Param("workLogId") Long workLogId);
}

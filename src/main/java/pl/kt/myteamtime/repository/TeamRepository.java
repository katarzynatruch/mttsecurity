package pl.kt.myteamtime.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.Team;

@Repository
public interface TeamRepository extends JpaRepository <Team, Long>{

	
	@Query("select t from Team t join t.manager m where m.identificator = :managerIdentificator")
	List<Team> findTeamsByManagerIdentificator(@Param ("managerIdentificator") String managerIdentificator);

	@Query("select t from Team t where t.name=:name")
	Team findByName(@Param("name") String name);

	@Query("select t from Team t join t.employees e where e.id=:employee and e.deactivated=false")
	List<Team> findByEmployee(@Param("employee")Long employee);

}

package pl.kt.myteamtime.repository;

import com.google.common.base.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.entity.ProjectEmployeeMaxHours;

@Repository
public interface ProjectEmployeeMaxHoursRepository extends JpaRepository<ProjectEmployeeMaxHours, Long> {


    @Query("select h from ProjectEmployeeMaxHours h where h.employee=:employee and h.project=:project and h.employee.deactivated=false")
    Optional<ProjectEmployeeMaxHours> findByEmployeeAndProject(@Param("employee") Employee employee, @Param("project") Project project);

    @Query("select p from ProjectEmployeeMaxHours p where p.employee = ?1 and p.project = ?2 and p.employee.deactivated=false")
    Optional<ProjectEmployeeMaxHours> findbyEmployeeAndProject(Employee employee, Project project);


}

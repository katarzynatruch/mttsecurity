package pl.kt.myteamtime.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.enums.ProjectType;

@Repository
public interface ProjectRepository extends JpaRepository <Project, Long>{

	//@Query("select p from Project p join p.employee e where e.employeeId = :employeeId")
	//select e from Event e join e.place p where p.idPlace = :idPlace and p.active = 1
	@Query("select p from Employee e join e.projects p where e.id=:employeeIdentificator and p.isActive=true and e.deactivated=false")
	List<Project> findActiveByUserIdentificator(@Param ("employeeIdentificator")String employeeIdentificator);

	@Query("select p from Project p where p.number=:projectNumber and p.isActive=true")
	Optional<Project> findActiveByProjectNumber(@Param ("projectNumber")String projectNumber);

	@Query("select p from Employee e join e.projects p where e.id=:employeeId and p.isActive=true and e.deactivated=false")
	List<Project> findActiveByEmployeeId(@Param ("employeeId")Long employeeId);

	@Query("select p from Project p where p.type=:type and p.isActive=true")
	List<Project> findActiveByType(@Param("type")ProjectType type);

	@Query("select p from Project p where p.isActive=true")
	List<Project> findAllActive();

	@Query("select p from Project p where p.id=:projectId and p.isActive=true")
	Optional<Project> findActiveById(@Param(("projectId")) Long projectId);
}

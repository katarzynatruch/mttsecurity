package pl.kt.myteamtime.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pl.kt.myteamtime.entity.Availability;
import pl.kt.myteamtime.entity.Employee;

public interface AvailabilityRepository extends JpaRepository <Availability, Long> {

	@Query("select a from Availability a where a.employee=:employee and a.employee.deactivated=false")
	List<Availability> findByEmployee(@Param("employee")Employee employee);

}

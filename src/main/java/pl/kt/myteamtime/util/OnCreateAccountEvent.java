package pl.kt.myteamtime.util;

import org.springframework.context.ApplicationEvent;
import pl.kt.myteamtime.entity.User;

public class OnCreateAccountEvent extends ApplicationEvent {
    private String appUrl;
    private User account;

    public OnCreateAccountEvent(User account, String appUrl) {
        super(account);

        this.account = account;
        this.appUrl = appUrl;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public User getAccount() {
        return account;
    }
}

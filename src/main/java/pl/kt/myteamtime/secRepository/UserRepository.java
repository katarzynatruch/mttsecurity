package pl.kt.myteamtime.secRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.kt.myteamtime.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {


    @Query("select u from User u where u.email=:email and u.active=true and u.deactivated=false")
    User findActiveByEmail(@Param("email")String email);

    @Query("select u from User u where u.identificator=:identificator and u.active=true and u.deactivated=false")
    User findActiveByIdentificator(@Param("identificator")String identificator);

    @Query("select u from User u where u.active=true  and u.deactivated=false")
    Optional<List<User>> findAllNotDeactivated();
}

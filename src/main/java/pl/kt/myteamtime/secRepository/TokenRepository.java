package pl.kt.myteamtime.secRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.secEntity.VerificationToken;

import java.util.List;
import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<VerificationToken, Long> {

    @Query("select t from VerificationToken t where t.token=:token")
    VerificationToken findByToken(@Param("token") String token);

    @Query("select t from VerificationToken t where t.user=:user and t.user.deactivated=false")
    Optional<List<VerificationToken>> findListByUser(@Param("user") User user);

    @Query("select t from VerificationToken t where t.user=:user and t.user.deactivated=false")
    VerificationToken findByUser(@Param("user") User user);
}

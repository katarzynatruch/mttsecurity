package pl.kt.myteamtime.secRepository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.secEntity.ResetToken;

import java.util.List;
import java.util.Optional;

@Repository
public interface ResetTokenRepository extends JpaRepository<ResetToken, Long> {

	@Query("select t from ResetToken t where t.token=:token")
	ResetToken findByToken(@Param("token") String token);

	@Query("select t from ResetToken t where t.user=:user and t.user.deactivated=false")
	Optional<List<ResetToken>> findListByUser(@Param("user") User user);

	@Query("select t from ResetToken t where t.user=:user and t.user.deactivated=false")
	ResetToken findByUser(@Param("user") User user);
}

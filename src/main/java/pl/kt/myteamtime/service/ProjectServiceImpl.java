package pl.kt.myteamtime.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.ds.ProjectWithEmployeeListDs;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dts.ProjectListDts;
import pl.kt.myteamtime.entity.Deadline;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.enums.ProjectType;
import pl.kt.myteamtime.mapper.CycleAvoidingMappingContext;
import pl.kt.myteamtime.mapper.ProjectMapper;
import pl.kt.myteamtime.repository.ProjectRepository;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private ProjectMapper projectMapper;

    @Autowired
    private DeadlineService deadlineService;

    @Autowired
    private ProjectEmployeeMaxHoursService projectEmployeeMaxHoursService;

    @Override
    public List<ProjectDTO> findProjectsByEmployeeIdentificator(String employeeIdentificator) {

        EmployeeDTO employee = employeeService.findByEmployeeIdentificatorDTO(employeeIdentificator);

        return employee.getProjects();
    }

    @Override
    public ProjectDTO findProjectByProjectNumberDTO(String projectNumber) {
        Optional<Project> project = projectRepository.findActiveByProjectNumber(projectNumber);
        return projectMapper.mapToProjectDTO(project.get(), new CycleAvoidingMappingContext());
    }

    @Override
    public Project findProjectByProjectNumber(String projectNumber) {
        Optional<Project> project = projectRepository.findActiveByProjectNumber(projectNumber);
        if (project.isPresent()) {
            return project.get();
        } else {
            return null;
        }
    }

    @Override
    public void saveProjectEmployeeRelations(String projectNumber, List<String> employeeIdentificators) {
        Optional<Project> project = projectRepository.findActiveByProjectNumber(projectNumber);
        List<Employee> employees = new ArrayList<>();
        for (String identificator : employeeIdentificators) {
            Employee employee = employeeService.findByEmployeeIdentificator(identificator);
            if (employee != null) {
                employees.add(employee);
                if (project.isPresent()) {
                    Long maxHours;
                    if (projectEmployeeMaxHoursService.getProjectEmployeeMaxHoursOptional(project.get(), employee).isPresent()) {
                        maxHours = projectEmployeeMaxHoursService.findMaxHoursForProjectAndEmployee(project.get(), employee);
                    } else {
                        maxHours = 0l;
                    }
                    projectEmployeeMaxHoursService.save(project.get(), employee, maxHours);
                }
            }
        }
    }

    @Override
    public List<String> findAllCostCenters() {
        List<String> costCenters = projectRepository.findAllActive().stream()
                .map(e -> e.getCostCenter())
                .distinct()
                .collect(Collectors.toList());
        if (costCenters.size() > 0) {
        return costCenters;
        }
        return null;
    }



    @Override
    public ProjectListDts loadTabData() {
        List<Project> projects = new ArrayList<Project>();
        projects = projectRepository.findAll();

        ProjectListDts projectListDts = new ProjectListDts();
        projectListDts.setProjects(
                projectMapper.mapToProjectListToProjectDTOList(projects, new CycleAvoidingMappingContext()));
        return projectListDts;
    }

    @Override
    public void save(ProjectDTO projectDTO) {
        projectDTO.setIsActive(true);
        Project managedProject = projectRepository
                .save(projectMapper.mapToProject(projectDTO, new CycleAvoidingMappingContext()));
        List<Deadline> deadlines = projectDTO.getDeadlines();
        deadlines.forEach(d -> d.setProject(managedProject));
        deadlineService.saveList(deadlines);
    }

    @Override
    public void update(ProjectDTO projectDTO) {

        Optional<Project> project = projectRepository.findActiveByProjectNumber(projectDTO.getNumber());

        projectDTO.setIsActive(true);
        if(project.isPresent()){
            Project project1 = project.get();
            project1.setMaxHours(projectDTO.getMaxHours());
            project1.setName(projectDTO.getName());
            project1.setNumber(projectDTO.getNumber());
            project1.setCostCenter(projectDTO.getCostCenter());
            project1.setType(projectDTO.getType());
            projectRepository.save(project1);
            Optional<List<Deadline>> oldDeadlines = deadlineService.findAllByProjectNumber(project1.getNumber());
            if(oldDeadlines.isPresent())
                oldDeadlines.get().forEach(od -> deadlineService.delete(od));
            List<Deadline> deadlines = projectDTO.getDeadlines();
            deadlines.forEach(d -> d.setProject(project.get()));
            deadlineService.saveList(deadlines);
        }
//        projectRepository.delete(project.get());
//        Project managedProject = projectRepository
//                .save(projectMapper.mapToProject(projectDTO, new CycleAvoidingMappingContext()));

    }

    @Override
    public List<ProjectDTO> findAll() {
        List<Project> managedProjects = projectRepository.findAllActive();
        return projectMapper.mapToProjectListToProjectDTOList(managedProjects, new CycleAvoidingMappingContext());
    }

    @Override
    public List<ProjectWithEmployeeListDs> findProjectWithEmployees(String managerIdentificator) {

        List<ProjectWithEmployeeListDs> projectWithEmployeeListDses = new ArrayList<>();
        List<Project> projects = projectRepository.findAllActive();
        for (Project project : projects) {
            ProjectWithEmployeeListDs projectWithEmployeeListDs = new ProjectWithEmployeeListDs();
            List<EmployeeDTO> employees = employeeService.findEmployeeByProjectAndManagerIdentificator(project,
                    managerIdentificator).stream().distinct().collect(Collectors.toList());
            projectWithEmployeeListDs.setEmployees(employees);
            projectWithEmployeeListDs
                    .setProject(projectMapper.mapToProjectDTO(project, new CycleAvoidingMappingContext()));
            projectWithEmployeeListDses.add(projectWithEmployeeListDs);
        }
        return projectWithEmployeeListDses;
    }

    @Override
    public List<Project> mapProjectsList(List<ProjectDTO> projects) {
        return projectMapper.mapToProjectListToProjectList(projects, new CycleAvoidingMappingContext());
    }

    @Override
    public List<ProjectDTO> findByType(ProjectType type) {
        List<Project> managedProjects = projectRepository.findActiveByType(type);
        return projectMapper.mapToProjectListToProjectDTOList(managedProjects, new CycleAvoidingMappingContext());
    }

    @Override
    public Project findById(Long projectId) {
        return projectRepository.findActiveById(projectId).orElse(null);
    }

    @Override
    public void delete(String projectNumber) {
        Optional<Project> project = projectRepository.findActiveByProjectNumber(projectNumber);
        if (project.isPresent()) {
            project.get().setIsActive(false);
            projectRepository.save(project.get());
        }
    }

}

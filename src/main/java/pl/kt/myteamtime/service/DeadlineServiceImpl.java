package pl.kt.myteamtime.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.entity.Deadline;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.repository.DeadlineRepository;
import pl.kt.myteamtime.repository.ProjectRepository;

@Service
public class DeadlineServiceImpl implements DeadlineService {

	@Autowired
	private
	DeadlineRepository deadlineRepository;

	@Autowired
	private
	ProjectRepository projectRepository;
	
	@Override
	public void saveList(List<Deadline> deadlines) {
		deadlines.forEach(d -> deadlineRepository.save(d));
	}

	@Override
	public void deleteList(List<Deadline> deadlines) {
		deadlines.forEach(d -> deadlineRepository.delete(d));
	}

	@Override
	public void delete(Deadline deadline) {
		deadlineRepository.delete(deadline);
	}

	@Override
	public Optional<List<Deadline>> findAllByProjectNumber(String projectNumber) {
		Optional<Project> project = projectRepository.findActiveByProjectNumber(projectNumber);
		List<Deadline> foundDeadlines = deadlineRepository.findAllByProjectId(project.get());
		return Optional.ofNullable(foundDeadlines);
	}
}

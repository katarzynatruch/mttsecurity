package pl.kt.myteamtime.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.TeamDTO;
import pl.kt.myteamtime.entity.Team;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.mapper.CycleAvoidingMappingContext;
import pl.kt.myteamtime.mapper.EmployeeMapper;
import pl.kt.myteamtime.mapper.TeamMapper;
import pl.kt.myteamtime.repository.TeamRepository;
import pl.kt.myteamtime.secService.UserService;

@Service
public class TeamServiceImpl implements TeamService {

	@Autowired
	TeamRepository teamRepository;

	@Autowired
	TeamMapper teamMapper;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	EmployeeMapper employeeMapeer;

	@Autowired
	UserService userService;

	@Override
	public List<TeamDTO> findTeamsByManagerIdentificator(String managerIdentificator) {
		List<Team> teams = teamRepository.findTeamsByManagerIdentificator(managerIdentificator);
		List<TeamDTO> teamDTOS = teamMapper.mapToTeamDTOList(teams, new CycleAvoidingMappingContext());
		return teamDTOS;
	}

	@Override
	public void saveTeam(TeamDTO teamDto) {

		teamRepository.save(teamMapper.mapToTeam(teamDto, new CycleAvoidingMappingContext()));
	}

	@Override
	public void saveTeams(List<TeamDTO> teamDTOList) {

		teamDTOList.stream()
				.forEach(t ->saveTeam(t));
	}

	@Override
	public List<Team> findTeamsByNames(List<String> teamNames) {
		return teamNames.stream()
				.map(teamName -> teamRepository.findByName(teamName))
				.collect(Collectors.toList());
	}

	
	@Override
	public void saveTeamWithAssignedEmployees(TeamDTO teamDto, List<String> employeesIdentificatorList, String managerIdentificator) {
		Team team = teamMapper.mapToTeam(teamDto, new CycleAvoidingMappingContext());
		team.setManager(userService.findByIdentificator(managerIdentificator));
		team.setEmployees(employeeService.findEmployeesByIdentificators(employeesIdentificatorList));
		teamRepository.save(team);
	}

	private void przykladoweStreamy(List<EmployeeDTO> employees) {
		employees.stream();

		List<String> emails = employees.stream().map(e -> e.getEmail()).collect(Collectors.toList());
		
		String email = "oberdor@gmail.com";
		boolean isAnyEmployeeStreamOberdor = employees.stream().anyMatch(e -> e.getEmail().equals(email));
		
		List<EmployeeDTO> employeesWithNameContainsHub = employees.stream().filter(e -> e.getFirstName().contains("Hub")).collect(Collectors.toList());
		
		employees.stream().forEach(e -> jakastammetoda(e));
	}
	
	private void jakastammetoda(EmployeeDTO employeeDTO) {
		System.out.println(employeeDTO.getFirstName());
	}

	@Override
	public List<TeamDTO> findByEmployee(EmployeeDTO employee) {

		List<Team> managedTeams = teamRepository.findByEmployee(employeeMapeer.mapToEmployee(employee, new CycleAvoidingMappingContext()).getId());
		return teamMapper.mapToTeamDTOList(managedTeams,  new CycleAvoidingMappingContext());
	}
	
}

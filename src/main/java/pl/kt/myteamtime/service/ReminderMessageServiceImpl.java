package pl.kt.myteamtime.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.dto.ReminderMessageDTO;
import pl.kt.myteamtime.entity.ReminderMessage;
import pl.kt.myteamtime.mapper.CycleAvoidingMappingContext;
import pl.kt.myteamtime.mapper.ReminderMessageMapper;
import pl.kt.myteamtime.repository.ReminderMessageRepository;

@Service
public class ReminderMessageServiceImpl implements ReminderMessageService {

	@Autowired
	private ReminderMessageRepository reminderMessageRepository;

	@Autowired
	private ReminderMessageMapper reminderMessageMapper;

	@Autowired
	private ManagerService managerService;

	@Override
	public ReminderMessageDTO findDefaultMessage(String managerIdentificator) {
		ReminderMessage message =  reminderMessageRepository.findByManagerIdentificator(managerIdentificator).orElse(null);
		return reminderMessageMapper.mapToReminderMessageDTO(message, new CycleAvoidingMappingContext());
	}

	@Override
	public void changeDefaultMessage(String managerIdentificator, ReminderMessageDTO reminderMessageDTO) {

		ReminderMessage reminderMessage = new ReminderMessage();
		reminderMessage.setManager(managerService.findByIdentificator(managerIdentificator));
		reminderMessage.setDefaultReminderMessage(reminderMessageDTO.getDefaultReminderMessage());
		if(reminderMessageRepository.findByManagerIdentificator(managerIdentificator).isPresent())
		reminderMessageRepository.delete(reminderMessageRepository.findByManagerIdentificator(managerIdentificator).get());
		reminderMessageRepository.save(reminderMessage);
	}

}

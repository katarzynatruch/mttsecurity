package pl.kt.myteamtime.service;

import java.util.List;

import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.TeamDTO;
import pl.kt.myteamtime.entity.Team;

public interface TeamService {

	List<TeamDTO> findTeamsByManagerIdentificator(String managerIdentificator);

	void saveTeam(TeamDTO team);

	void saveTeams(List<TeamDTO> teams);

	List<Team> findTeamsByNames(List<String> retrieveTeamsNames);

	void saveTeamWithAssignedEmployees(TeamDTO teamDto, List<String> employeesIdentificatorList, String managerIdentificator);

	List<TeamDTO> findByEmployee(EmployeeDTO employee);

	
}

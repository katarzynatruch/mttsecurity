package pl.kt.myteamtime.service;

import java.util.List;

import pl.kt.myteamtime.dto.EmployeeDTO;

public interface EmailService {

	void sendMessageWithAttachment(String to, String subject, String text, String pathToAttachment);

	void sendMessage(String to, String subject, String text);

	void sendMessageToGroup(List<String> employeeIdentificators, String message);
}

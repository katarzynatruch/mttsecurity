package pl.kt.myteamtime.service;

import java.util.List;

import pl.kt.myteamtime.ds.AvailabilitySingleEmployeeUpdateDs;
import pl.kt.myteamtime.dto.AvailabilityDTO;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dts.FilterDataRetrieveDts;

public interface AvailabilityService {

	List<AvailabilityDTO> findByEmployee(EmployeeDTO employee);

	List <AvailabilityDTO> findByEmployeeAndData(EmployeeDTO employee, FilterDataRetrieveDts filterDataRetrieveDts);

	void save(AvailabilitySingleEmployeeUpdateDs availabilitySingleEmployeeDs);

}

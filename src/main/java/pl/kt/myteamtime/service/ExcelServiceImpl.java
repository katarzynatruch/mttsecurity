package pl.kt.myteamtime.service;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gembox.spreadsheet.ExcelFile;
import com.gembox.spreadsheet.ExcelWorksheet;
import com.gembox.spreadsheet.SpreadsheetInfo;
import com.gembox.spreadsheet.charts.ChartGrouping;
import com.gembox.spreadsheet.charts.ChartType;
import com.gembox.spreadsheet.charts.ColumnChart;
import com.gembox.spreadsheet.charts.LineChart;

import pl.kt.myteamtime.ds.*;
import pl.kt.myteamtime.dto.ProjectDTO;

@Service
public class ExcelServiceImpl implements ExcelService {

	@Autowired
	private DataAndTimeService dataAndTimeService;

	@Override
	public void prepareCapacityGraph(List<CapacityOfEmployeeDs> capacityOfEmployees) {

		SpreadsheetInfo.setLicense("FREE-LIMITED-KEY");

		ExcelFile workbook = new ExcelFile();
		ExcelWorksheet worksheetGraph = workbook.addWorksheet("Capacity_Chart");
		ExcelWorksheet worksheetData = workbook.addWorksheet("Capacity_Data");

		int numberOfEmployees = capacityOfEmployees.size();
		int numberOfWeeks = 12;

		LineChart chart = (LineChart) worksheetGraph.getCharts().add(ChartType.LINE, "B2", "R25");
		chart.selectData(worksheetData.getCells().getSubrangeAbsolute(0, 1, numberOfEmployees, numberOfWeeks + 1));

		chart.getAxes().getHorizontal().getTitle().setText("Week");
		chart.getAxes().getVertical().getTitle().setText("Hours");

		fulfillCapacityEmployeeColumn(capacityOfEmployees, worksheetData);
		fulfillCapacityRowHeader(capacityOfEmployees, worksheetData);

		int employeeIterator = 0;
		for (CapacityOfEmployeeDs c : capacityOfEmployees) {
			int weekIterator = 0;
			for (CapacityParametersDs cp : c.getCapacityParameters()) {
				worksheetData.getCell(employeeIterator + 1, weekIterator + 2).setValue(cp.getWorkedHoursInSum());
				weekIterator++;
			}

			employeeIterator++;
		}

		try {
			workbook.save("Capacity_Graph.xlsx");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void fulfillCapacityRowHeader(List<CapacityOfEmployeeDs> capacityOfEmployees,
			ExcelWorksheet worksheetData) {
		int weekIterator = 0;

		for (CapacityParametersDs cp : capacityOfEmployees.get(0).getCapacityParameters()) {
			worksheetData.getCell(0, weekIterator + 2).setValue("w" + cp.getWeek() + "y" + cp.getYear());
			weekIterator++;
		}
	}

	private void fulfillCapacityEmployeeColumn(List<CapacityOfEmployeeDs> capacityOfEmployees,
			ExcelWorksheet worksheetData) {
		int employeeIterator = 0;
		for (CapacityOfEmployeeDs c : capacityOfEmployees) {
			worksheetData.getCell(employeeIterator + 1, 1)
					.setValue(c.getEmployee().getFirstName() + " " + c.getEmployee().getLastName());
			employeeIterator++;
		}
	}

	@Override
	public void prepareHoursEstimationChart(List<HoursConsumptionPerWeekDs> listOfHoursConsumptionPerWeekDs,
			int weekFrom, int yearFrom, int weekTo, int yearTo) {

		SpreadsheetInfo.setLicense("FREE-LIMITED-KEY");

		ExcelFile workbook = new ExcelFile();
		ExcelWorksheet worksheetGraph = workbook.addWorksheet("Hours_Estimation_Chart");
		ExcelWorksheet worksheetData = workbook.addWorksheet("Hours_Estimation__Data");

		Set<ProjectDTO> projects = new HashSet<>();
		for (HoursConsumptionPerWeekDs h : listOfHoursConsumptionPerWeekDs) {
			projects.add(h.getProject());
		}

		int numberOfProjects = projects.size();
		int numberOfWeeks = dataAndTimeService.numberOfWeeksFromDateToDate(weekFrom, yearFrom, weekTo, yearTo);

		LineChart chart = (LineChart) worksheetGraph.getCharts().add(ChartType.LINE, "B2", "R25");
		chart.selectData(worksheetData.getCells().getSubrangeAbsolute(0, 1, numberOfProjects + 1, numberOfWeeks + 1));

		chart.setShowMarkers(true);

		chart.getAxes().getHorizontal().getTitle().setText("Week");
		chart.getAxes().getVertical().getTitle().setText("Hours");
		int projectIterator = 0;
		for (HoursConsumptionPerWeekDs h : listOfHoursConsumptionPerWeekDs) {

			worksheetData.getCell(projectIterator + 1, 1).setValue(h.getProject().getNumber());

			for (int weekIterator = 0; weekIterator < numberOfWeeks; weekIterator++) {
				setWeekAsHeader(worksheetData, h, weekIterator);
			}
			projectIterator++;
		}
		int projectIterator2 = 0;
		for (HoursConsumptionPerWeekDs h : listOfHoursConsumptionPerWeekDs) {
			for (int weekIterator = 0; weekIterator < numberOfWeeks; weekIterator++) {
				worksheetData.getCell(projectIterator2 + 1, weekIterator + 2)
						.setValue(h.getHoursConsumptionForEmployees().get(weekIterator).getUsedHours());
			}

			projectIterator2++;
		}

		try {
			workbook.save("Estimation_Graph.xlsx");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void setWeekAsHeader(ExcelWorksheet worksheetData, HoursConsumptionPerWeekDs h, int weekIterator) {
		worksheetData.getCell(0, weekIterator + 2)
				.setValue("w" + h.getHoursConsumptionForEmployees().get(weekIterator).getWeek() + "y"
						+ h.getHoursConsumptionForEmployees().get(weekIterator).getYear());
	}

	@Override
	public void prepareComboGraph(List<ProjectsAndTimeForEmployeeDs> projectsAndTimeForEmployeeDs, int weekFrom,
			int weekTo, int yearFrom, int yearTo) {

		SpreadsheetInfo.setLicense("FREE-LIMITED-KEY");

		ExcelFile workbook = new ExcelFile();
		ExcelWorksheet worksheetGraph = workbook.addWorksheet("ComboGraph");
		ExcelWorksheet worksheetData = workbook.addWorksheet("ComboGraphData");

		Set<ProjectDTO> projects = new HashSet<>();
		for (ProjectsAndTimeForEmployeeDs p : projectsAndTimeForEmployeeDs) {

			List<SingleProjectAndHoursEmployeeDs> singleProjectAndHoursEmployeeDsList = p.getProjectsWithHours();
			for (SingleProjectAndHoursEmployeeDs s : singleProjectAndHoursEmployeeDsList) {
				projects.add(s.getProject());
			}
		}

		int numberOfEmployees = projectsAndTimeForEmployeeDs.size();
		int numberOfProjects = projects.size();

		ColumnChart chart = (ColumnChart) worksheetGraph.getCharts().add(ChartType.COLUMN, "B2", "R25");
		chart.selectData(worksheetData.getCells().getSubrangeAbsolute(0, 1, numberOfProjects, numberOfEmployees + 1));

		chart.setGrouping(ChartGrouping.STACKED);

		if (yearFrom == yearTo) {
			if (weekFrom == weekTo) {
				chart.getTitle().setText("Work logs in week w" + weekFrom + "y" + yearFrom);
			} else {
				chart.getTitle()
						.setText("Work logs in time w" + weekFrom + "y" + yearFrom + " - w" + weekTo + "y" + yearTo);
			}

		} else {
			chart.getTitle()
					.setText("Work logs in time w" + weekFrom + "y" + yearFrom + " - w" + weekTo + "y" + yearTo);
		}

		chart.getAxes().getHorizontal().getTitle().setText("Employees");
		chart.getAxes().getVertical().getTitle().setText("Hours");

		createWorkHoursChart(projectsAndTimeForEmployeeDs, worksheetData, numberOfProjects);
		try {
			workbook.save("Combo_Graph.xlsx");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void createWorkHoursChart(List<ProjectsAndTimeForEmployeeDs> projectsAndTimeForEmployeeDs,
			ExcelWorksheet worksheetData, int numberOfProjects) {
		int dsIterator = 0;
		for (ProjectsAndTimeForEmployeeDs mainDsIterator : projectsAndTimeForEmployeeDs) {
			fillWorksheetWithEmployeeName(worksheetData, dsIterator, mainDsIterator);
			int projectIterator = 0;
			fillWorksheetWithLoggedHours(projectsAndTimeForEmployeeDs, worksheetData, numberOfProjects, dsIterator,
					mainDsIterator, projectIterator);
			dsIterator++;
		}
	}

	private void fillWorksheetWithLoggedHours(List<ProjectsAndTimeForEmployeeDs> projectsAndTimeForEmployeeDs,
			ExcelWorksheet worksheetData, int numberOfProjects, int dsIterator,
			ProjectsAndTimeForEmployeeDs mainDsIterator, int projectIterator) {
		for (SingleProjectAndHoursEmployeeDs s : projectsAndTimeForEmployeeDs.get(projectIterator).getProjectsWithHours()) {
			for (int columnIterator = 1; columnIterator <= numberOfProjects; columnIterator++) {
				if (checkIfCurrentProjectColumn(worksheetData, mainDsIterator, projectIterator, columnIterator)) {
					provideUsedHours(worksheetData, dsIterator, mainDsIterator, projectIterator, columnIterator);
					break;
				} else if (checkIfCurrentProjectColumnEmpty(worksheetData, columnIterator)) {
					saveProjectNameAsHeader(worksheetData, s, columnIterator);
					provideUsedHours(worksheetData, dsIterator, mainDsIterator, projectIterator, columnIterator);
					break;
				}
			}
			projectIterator++;
		}
	}

	private void fillWorksheetWithEmployeeName(ExcelWorksheet worksheetData, int dsIterator,
			ProjectsAndTimeForEmployeeDs mainDsIterator) {
		worksheetData.getCell(0, dsIterator + 2).setValue(
				mainDsIterator.getEmployee().getFirstName() + " " + mainDsIterator.getEmployee().getLastName());
	}

	private void saveProjectNameAsHeader(ExcelWorksheet worksheetData, SingleProjectAndHoursEmployeeDs s,
			int columnIterator) {
		worksheetData.getCell(columnIterator, 1).setValue(s.getProject().getNumber());
	}

	private boolean checkIfCurrentProjectColumnEmpty(ExcelWorksheet worksheetData, int columnIterator) {
		return worksheetData.getCell(columnIterator, 1).getValue() == null;
	}

	private void provideUsedHours(ExcelWorksheet worksheetData, int i, ProjectsAndTimeForEmployeeDs p,
			int projectIterator, int columnIterator) {
		worksheetData.getCell(columnIterator, i + 2).setValue(p.getProjectsWithHours().get(projectIterator).getHoursUsed());
	}

	private boolean checkIfCurrentProjectColumn(ExcelWorksheet worksheetData, ProjectsAndTimeForEmployeeDs p,
			int projectIterator, int columnIterator) {
		return p.getProjectsWithHours().get(projectIterator).getProject().getNumber()
				.equals(worksheetData.getCell(columnIterator, 1).getValue());
	}

}

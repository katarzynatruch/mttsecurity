package pl.kt.myteamtime.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.repository.ManagerRepository;

@Service
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    private
    ManagerRepository managerRepository;

    @Override
    public User findByIdentificator(String managerIdentificator) {
        return managerRepository.findByIdentificator(managerIdentificator);
    }

    @Override
    public User findById(Long managerId) {
        return managerRepository.findById(managerId).orElse(null);
    }
}

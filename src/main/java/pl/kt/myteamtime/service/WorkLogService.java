package pl.kt.myteamtime.service;

import java.util.List;

import pl.kt.myteamtime.ds.WorkLogWeekDs;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dto.WorkLogDTO;
import pl.kt.myteamtime.dts.TimeReportManagerLoadDts;

public interface WorkLogService {

	List<WorkLogWeekDs> findWorkLogWeeksByUserIdentificator(String employeeIdentificator);

	List<WorkLogDTO> findWorkLogByEmployeeIdentificatorAndProjectNumber(String employeeIdentificator,
			String projectNumber, int weekFrom, int weekTo, int yearFrom, int yearTo);

	void saveOrUpdate(List<WorkLogDTO> workLogs);

	long calculateHoursForProject(int week, int year, List<EmployeeDTO> employees, ProjectDTO project);

	long calculateHoursForEmployee(EmployeeDTO employee, List<ProjectDTO> projects, int week, int year);

	List<EmployeeDTO> findEmployeesByConfirmedWorkLog(String managerIdentificator);

	List<EmployeeDTO> findEmployeesByNotConfirmedWorkLog(String managerIdentificator);

    void deleteWorkLog(Long workLogId);

	void updateEmployeeApproval(List<WorkLogDTO> workLogs, String employeeIdentificator);

	void updateManagerApproval(List<WorkLogDTO> workLogs, String employeeIdentificator);

	List<TimeReportManagerLoadDts> findEmployeesWithWorklogsByConfirmedWorkLog(String managerIdentificator);


}

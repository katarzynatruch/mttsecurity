package pl.kt.myteamtime.service;

import java.util.List;

import pl.kt.myteamtime.ds.CapacityOfEmployeeDs;
import pl.kt.myteamtime.ds.HoursConsumptionPerWeekDs;
import pl.kt.myteamtime.ds.ProjectsAndTimeForEmployeeDs;

public interface ExcelService {
	void prepareComboGraph(List<ProjectsAndTimeForEmployeeDs> projectsAndTimeForEmployeeDs, int weekFrom, int weekTo,
			int yearFrom, int yearTo);

    void prepareHoursEstimationChart(List<HoursConsumptionPerWeekDs> listOfHoursConsumptionPerWeekDs, int weekFrom, int yearFrom, int weekTo, int yearTo);

    void prepareCapacityGraph(List<CapacityOfEmployeeDs> capacityOfEmployees);
}

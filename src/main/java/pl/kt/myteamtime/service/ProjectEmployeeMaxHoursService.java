package pl.kt.myteamtime.service;

import java.util.List;

import com.google.common.base.Optional;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dto.ProjectEmployeeMaxHoursDTO;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.entity.ProjectEmployeeMaxHours;

public interface ProjectEmployeeMaxHoursService {

	List<ProjectEmployeeMaxHoursDTO> calculateMaxHoursForProjectAndEmployees(String project, List<String> employees);

	void save(Project project, Employee employee, Long maxHours);

	long findMaxHoursForProjectAndEmployee(ProjectDTO project, EmployeeDTO employee);

	long findMaxHoursForProjectAndEmployee(Project project, Employee employee);

    void deleteProjectEmployeeRelation(Project project, Employee employee);

    void delete(Project project, Employee employee);

	Optional<ProjectEmployeeMaxHours> getProjectEmployeeMaxHoursOptional(Project project, Employee employee);
}

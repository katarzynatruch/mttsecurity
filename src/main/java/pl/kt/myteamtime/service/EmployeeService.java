package pl.kt.myteamtime.service;

import java.util.List;

import pl.kt.myteamtime.ds.EmployeeWithProjectListDs;
import pl.kt.myteamtime.ds.ProjectNumberWithMaxHoursDs;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.UserDTO;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.entity.User;

public interface EmployeeService {

    EmployeeDTO findByEmployeeIdentificatorDTO(String employeeIdentificator);

    Employee findByEmployeeId(Long employeeId);

    List<EmployeeDTO> findEmployeesByTeamId(Long teamId);

    void saveEmployee(EmployeeDTO employeeDTO);

    List<EmployeeDTO> findEmployeeByManagerIdentificator(String managerIdentificator);

    List<EmployeeWithProjectListDs> findEmployeeWithProjects(String managerIdentificator);

    List<EmployeeDTO> findEmployeeByProjectAndManagerIdentificator(Project project, String managerIdentificator);

    List<Employee> findEmployeesById(List<Long> employeesIdList);

    List<Employee> findEmployeesByIdentificators(List<String> employeesIdentificatorList);

	void saveEmployeeProjectRelations(String employeeIdentificator, List<ProjectNumberWithMaxHoursDs> projectNumberWithMaxHoursDsList);

    void delete(Long employeeId);

    Employee findByEmployeeIdentificator(String employeeIdentificator);

    void saveEmployee(User user);

    Employee saveEmployee(UserDTO user);

    public void deactivateEmployee(String employeeIdentificator);
}

package pl.kt.myteamtime.service;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dto.ProjectEmployeeMaxHoursDTO;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.entity.ProjectEmployeeMaxHours;
import pl.kt.myteamtime.mapper.CycleAvoidingMappingContext;
import pl.kt.myteamtime.mapper.EmployeeMapper;
import pl.kt.myteamtime.mapper.ProjectEmployeeMaxHoursMapper;
import pl.kt.myteamtime.mapper.ProjectMapper;
import pl.kt.myteamtime.repository.ProjectEmployeeMaxHoursRepository;

@Service
public class ProjectEmployeeMaxHoursServiceImpl implements ProjectEmployeeMaxHoursService {

    @Autowired
    private
    ProjectEmployeeMaxHoursRepository projectEmployeeMaxHoursRepository;

    @Autowired
    ProjectEmployeeMaxHoursMapper projectEmployeeMaxHoursMapper;

    @Autowired
    private
    EmployeeMapper employeeMapper;

    @Autowired
    private
    ProjectMapper projectMapper;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private ProjectService projectService;

    @Override
    public List<ProjectEmployeeMaxHoursDTO> calculateMaxHoursForProjectAndEmployees(String projectNumber,
                                                                              List<String> employeeIdentificators) {
        List<ProjectEmployeeMaxHoursDTO> projectEmployeeMaxHoursDTOList = new ArrayList<>();
        for (String employeeIdentificator : employeeIdentificators) {
        ProjectEmployeeMaxHoursDTO projectEmployeeMaxHoursDTO = new ProjectEmployeeMaxHoursDTO();
        int maxHoursForProject = 0;


            Optional<ProjectEmployeeMaxHours> currentHours = projectEmployeeMaxHoursRepository.findByEmployeeAndProject(
                    employeeService.findByEmployeeIdentificator(employeeIdentificator),
                    projectService.findProjectByProjectNumber(projectNumber));

            if (currentHours.isPresent()) {
                maxHoursForProject = (int) (maxHoursForProject + currentHours.get().getMaxHours());
            }
            projectEmployeeMaxHoursDTO.setProject(projectService.findProjectByProjectNumberDTO(projectNumber));
            projectEmployeeMaxHoursDTO.setMaxHours(maxHoursForProject);
            projectEmployeeMaxHoursDTO.setEmployee(employeeService.findByEmployeeIdentificatorDTO(employeeIdentificator));
          projectEmployeeMaxHoursDTOList.add(projectEmployeeMaxHoursDTO);
        }


        return projectEmployeeMaxHoursDTOList;
    }

    @Override
    public void save(Project project, Employee employee, Long maxHours) {

        ProjectEmployeeMaxHours projectEmployeeMaxHours = new ProjectEmployeeMaxHours();
        projectEmployeeMaxHours.setEmployee(employee);
        projectEmployeeMaxHours.setProject(project);
        projectEmployeeMaxHours.setMaxHours(maxHours);
        Optional<ProjectEmployeeMaxHours> tempProjectEmployeeMaxHours = getProjectEmployeeMaxHoursOptional(project, employee);
        if(tempProjectEmployeeMaxHours.isPresent()){
            projectEmployeeMaxHoursRepository.delete(tempProjectEmployeeMaxHours.get());
        }
        projectEmployeeMaxHoursRepository.save(projectEmployeeMaxHours);

    }

    @Override
    public Optional<ProjectEmployeeMaxHours> getProjectEmployeeMaxHoursOptional(Project project, Employee employee) {
        Optional<ProjectEmployeeMaxHours> tempProjectEmployeeMaxHours = projectEmployeeMaxHoursRepository.findbyEmployeeAndProject(employee, project);
        return tempProjectEmployeeMaxHours;
    }

    @Override
    public long findMaxHoursForProjectAndEmployee(ProjectDTO project, EmployeeDTO employee) {
        Optional<ProjectEmployeeMaxHours> maxHours = projectEmployeeMaxHoursRepository.findByEmployeeAndProject(
                employeeMapper.mapToEmployee(employee, new CycleAvoidingMappingContext()),
                projectMapper.mapToProject(project, new CycleAvoidingMappingContext()));
        if(!maxHours.isPresent()){
                return 0;
        }
        return maxHours.get().getMaxHours();
    }

    public long findMaxHoursForProjectAndEmployee(Project project, Employee employee) {
        Optional<ProjectEmployeeMaxHours> maxHours = projectEmployeeMaxHoursRepository.findByEmployeeAndProject(employee, project);
        if(!maxHours.isPresent()){
                return 0;
        }

        return maxHours.get().getMaxHours();
    }

    @Override
    public void deleteProjectEmployeeRelation(Project project, Employee employee) {
        Optional<ProjectEmployeeMaxHours> projectEmployeeMaxHours = projectEmployeeMaxHoursRepository.findByEmployeeAndProject(employee, project);
        projectEmployeeMaxHoursRepository.delete(projectEmployeeMaxHours.get());
    }

    @Override
    public void delete(Project project, Employee employee) {
        Optional<ProjectEmployeeMaxHours> projectEmployeeMaxHours = projectEmployeeMaxHoursRepository.findByEmployeeAndProject(employee,project);
        if(projectEmployeeMaxHours != null)
        projectEmployeeMaxHoursRepository.delete(projectEmployeeMaxHours.get());
    }

}

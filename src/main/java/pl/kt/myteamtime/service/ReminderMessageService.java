package pl.kt.myteamtime.service;

import pl.kt.myteamtime.dto.ReminderMessageDTO;

public interface ReminderMessageService {
    ReminderMessageDTO findDefaultMessage(String managerIdentificator);

    void  changeDefaultMessage(String managerIdentificator, ReminderMessageDTO reminderMessageDTO);
}

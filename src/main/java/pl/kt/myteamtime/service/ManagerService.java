package pl.kt.myteamtime.service;

import pl.kt.myteamtime.entity.Manager;
import pl.kt.myteamtime.entity.User;

public interface ManagerService {

    User findByIdentificator(String managerIdentificator);

    User findById(Long managerId);
}

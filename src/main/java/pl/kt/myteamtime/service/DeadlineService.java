package pl.kt.myteamtime.service;

import java.util.List;
import java.util.Optional;

import pl.kt.myteamtime.entity.Deadline;

public interface DeadlineService {

	void saveList(List<Deadline> deadlines);

    void deleteList(List<Deadline> deadlines);

    void delete(Deadline deadline);

    Optional<List<Deadline>> findAllByProjectNumber(String number);

}

package pl.kt.myteamtime.service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.ds.WorkLogWeekDs;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dto.WorkLogDTO;
import pl.kt.myteamtime.dts.TimeReportManagerLoadDts;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.entity.WorkLog;
import pl.kt.myteamtime.mapper.*;
import pl.kt.myteamtime.repository.WorkLogRepository;
import pl.kt.myteamtime.secService.UserService;

@Service
public class WorkLogServiceImpl implements WorkLogService {

    @Autowired
    TeamMapper teamMapper;
    @Autowired
    private WorkLogRepository workLogRepository;
    @Autowired
    private DataAndTimeService dataAndTimeService;
    @Autowired
    private WorkLogMapper workLogMapper;
    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private ProjectMapper projectMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private ProjectEmployeeMaxHoursService projectEmployeeMaxHoursService;

    @Override
    public List<WorkLogWeekDs> findWorkLogWeeksByUserIdentificator(String employeeIdentificator) {

        List<WorkLogDTO> workLogList = workLogMapper.mapToWorkLogDTOList(
                workLogRepository.findByEmployeeIdentificator(employeeIdentificator),
                new CycleAvoidingMappingContext());
        if (workLogList.size() < 1) {
            return null;
        }

        workLogList.forEach(w -> w.setLeftHours(calculateLeftHours(w.getEmployee(), w.getProject())));
        List<WorkLogWeekDs> workLogWeekDsList = groupWorkLogsByWeekAndYear(workLogList);

        return workLogWeekDsList;
    }

    @Override
    public List<TimeReportManagerLoadDts> findEmployeesWithWorklogsByConfirmedWorkLog(String managerIdentificator) {
        List<TimeReportManagerLoadDts> employeesWithWorklogs = new ArrayList<>();
        int week = dataAndTimeService.getCurrentWeekGlobal();
        int year = dataAndTimeService.getCurrenYearGlobal();
        User user = userService.findUserByIdentificator(managerIdentificator);
        List<WorkLog> workLogs = workLogRepository.findByConfirmationAndManager(user, week,
                year).stream().distinct().collect(Collectors.toList());
        List<Employee> employees = workLogs.stream().map(WorkLog::getEmployee).distinct().collect(Collectors.toList());
        for (Employee employee : employees) {
            List<WorkLog> worklogsFulfilled = workLogRepository.findByEmployeeWeekAndYear(employee, week, year)
                    .stream().filter(WorkLog::getIsConfirmedByEmployee).collect(Collectors.toList());
            TimeReportManagerLoadDts timeReportManagerLoadDts = new TimeReportManagerLoadDts();
            timeReportManagerLoadDts.setEmployee(employeeMapper.mapToEmployeeDTO(employee, new CycleAvoidingMappingContext()));
            timeReportManagerLoadDts.setWorklogs(workLogMapper.mapToWorkLogDTOList(worklogsFulfilled, new CycleAvoidingMappingContext()));
            employeesWithWorklogs.add(timeReportManagerLoadDts);
        }
        return employeesWithWorklogs;
    }

    @Override
    public List<EmployeeDTO> findEmployeesByConfirmedWorkLog(String managerIdentificator) {

        List<EmployeeDTO> employees = new ArrayList<>();
        int week = dataAndTimeService.getCurrentWeekGlobal();
        int year = dataAndTimeService.getCurrenYearGlobal();
        User user = userService.findUserByIdentificator(managerIdentificator);
        List<WorkLog> workLogs = workLogRepository.findByConfirmationAndManager(user, week,
                year).stream().distinct().collect(Collectors.toList());
        for (WorkLog workLog : workLogs) {
            if (workLog != null && workLog.getIsConfirmedByEmployee()) {
                Employee employee = workLog.getEmployee();
                employees.add(employeeMapper.mapToEmployeeDTO(employee, new CycleAvoidingMappingContext()));
            }
        }

        return employees.stream().distinct().collect(Collectors.toList());
    }

    @Override
    public List<WorkLogDTO> findWorkLogByEmployeeIdentificatorAndProjectNumber(String employeeIdentificator,
                                                                               String projectNumber, int weekFrom, int weekTo, int yearFrom, int yearTo) {

        List<WorkLogDTO> workLogList = workLogMapper.mapToWorkLogDTOList(
                workLogRepository.findByEmpIdentificatorAndProjectNumber(employeeIdentificator, projectNumber),
                new CycleAvoidingMappingContext());

        List<WorkLogDTO> workLogsFilteredByDate = new ArrayList<WorkLogDTO>();

        for (WorkLogDTO workLogDTO : workLogList) {

            int thatYear = workLogDTO.getYear();
            int thatWeek = workLogDTO.getWeek();

            if (checkIfThatTimeIsContainedInRange(thatYear, thatWeek, weekFrom, weekTo, yearFrom, yearTo)) {
                WorkLogDTO workLogFiltered = workLogDTO;
                workLogsFilteredByDate.add(workLogFiltered);
            }
        }
        return workLogsFilteredByDate;
    }

    @Override
    public List<EmployeeDTO> findEmployeesByNotConfirmedWorkLog(String managerIdentificator) {
        List<EmployeeDTO> employees = new ArrayList<EmployeeDTO>();
        int week = dataAndTimeService.getCurrentWeekGlobal();
        int year = dataAndTimeService.getCurrenYearGlobal();

        List<EmployeeDTO> allEmployees = employeeService.findEmployeeByManagerIdentificator(managerIdentificator).stream().distinct().collect(Collectors.toList());
        List<EmployeeDTO> lazyEmployees = new ArrayList<>();
        for (EmployeeDTO employee : allEmployees) {
            List<WorkLog> worklogs = workLogRepository.findByEmployeeWeekAndYear(employeeMapper.mapToEmployee(employee, new CycleAvoidingMappingContext()),
                    week, year);
            if (worklogs.size() < 1) {
                lazyEmployees.add(employee);
            } else if (worklogs.stream().anyMatch(w -> !w.getIsConfirmedByEmployee())) {
                lazyEmployees.add(employee);
            }
        }
        return Optional.ofNullable(lazyEmployees).orElse(Collections.EMPTY_LIST);
    }

    @Override
    public void deleteWorkLog(Long workLogId) {
        workLogRepository.deleteById(workLogId);
        Optional<WorkLog> workLog = workLogRepository.findById(workLogId);
        workLog.ifPresent(log -> workLogRepository.delete(log));
    }

    private List<WorkLogDTO> findWorkLogsByYearAndWeek(int year, int week, List<WorkLogDTO> sortedWorkLogs) {

        List<WorkLogDTO> workLogsWithSameDate = sortedWorkLogs.stream()
                .filter(workLog -> workLog.getYear() == year)
                .filter(workLog -> workLog.getWeek() == week)
                .collect(Collectors.toList());

        return workLogsWithSameDate;
    }

    private List<WorkLogWeekDs> groupWorkLogsByWeekAndYear(List<WorkLogDTO> workLogList) {

        List<WorkLogDTO> sortedWorkLogs = sortWorkLogs(workLogList);
        if (sortedWorkLogs.size() < 1) {
            return null;
        }
        WorkLogDTO oldestWorkLog = sortedWorkLogs.get(0);
        int oldestYear = oldestWorkLog.getYear();
        int oldestWeek = oldestWorkLog.getWeek();
        int currentYear = dataAndTimeService.getCurrenYearGlobal();
        int currentWeek = dataAndTimeService.getCurrentWeekGlobal();

        List<WorkLogWeekDs> sortedWorkLogWeekDs = new ArrayList<WorkLogWeekDs>();

        for (int year = oldestYear; year <= currentYear; year++) {
            for (int week = oldestWeek; week <= dataAndTimeService.getMaxWeekOfTheYear(year); week++) {
                if (checkIfDateIsBelowCurrent(year, week, currentYear, currentWeek)) {
                    WorkLogWeekDs workLogWeekDs = new WorkLogWeekDs();
                    workLogWeekDs.setYear(year);
                    workLogWeekDs.setWeek(week);
                    workLogWeekDs.setWorkLogs(findWorkLogsByYearAndWeek(year, week, sortedWorkLogs));
                    sortedWorkLogWeekDs.add(workLogWeekDs);
                }
            }
            oldestWeek = 1;
        }
        return sortedWorkLogWeekDs;
    }

    private List<WorkLogDTO> sortWorkLogs(List<WorkLogDTO> workLogList) {
        List<WorkLogDTO> sortedWorkLogs = workLogList.stream().sorted(new WorklogComparator())
                .collect(Collectors.toList());
        return sortedWorkLogs;
    }

    private boolean checkIfDateIsBelowCurrent(int year, int week, int currentYear, int currentWeek) {
        if (year == currentYear && week > currentWeek) {
            return false;
        }
        return true;
    }

    private boolean checkIfThatTimeIsContainedInRange(int thatYear, int thatWeek, int weekFrom, int weekTo,
                                                      int yearFrom, int yearTo) {
        if (thatYear >= yearFrom && thatYear <= yearTo) {
            boolean yearSameAsFrom = thatYear == yearFrom;
            boolean yearSameAsTo = thatYear == yearTo;
            boolean weekGreaterThanTo = thatWeek > weekTo;
            boolean weekLowerThanFrom = thatWeek < weekFrom;
            if (yearSameAsFrom && yearSameAsTo) {
                return !weekLowerThanFrom && !weekGreaterThanTo;
            } else if (yearSameAsTo) {
                return !weekGreaterThanTo;
            } else if (yearSameAsFrom) {
                return !weekLowerThanFrom;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    @Override
    public void saveOrUpdate(List<WorkLogDTO> workLogs) {
        for (WorkLogDTO workLog : workLogs) {
            List<WorkLog> worklogsToBeRemoved = workLogRepository.findByEmployeeWeekAndYear(employeeMapper.mapToEmployee(workLog.getEmployee(), new CycleAvoidingMappingContext()),
                    workLog.getWeek(), workLog.getYear());
            worklogsToBeRemoved.forEach(w -> workLogRepository.delete(w));
        }
        workLogs.forEach(workLog -> workLog.setIsConfirmedByEmployee(false));
        workLogs.forEach(workLog -> workLog.setIsConfirmedByManager(false));
        for (WorkLogDTO workLog : workLogs) {
            workLogRepository.save(workLogMapper.mapToWorkLog(workLog, new CycleAvoidingMappingContext()));
        }
    }

    @Override
    public void updateEmployeeApproval(List<WorkLogDTO> workLogs, String employeeIdentificator) {
        for (WorkLogDTO workLog : workLogs) {
            List<WorkLog> worklogsToBeRemoved = workLogRepository.findByEmployeeWeekAndYear(employeeMapper.mapToEmployee(workLog.getEmployee(), new CycleAvoidingMappingContext()),
                    workLog.getWeek(), workLog.getYear());
            worklogsToBeRemoved.forEach(w -> workLogRepository.delete(w));
        }
        workLogs.forEach(workLog -> updateWorklogApprovalByEmployee(workLog, employeeIdentificator));
        for (WorkLogDTO workLog : workLogs) {
            workLogRepository.save(workLogMapper.mapToWorkLog(workLog, new CycleAvoidingMappingContext()));
        }
    }

    private void updateWorklogApprovalByEmployee(WorkLogDTO workLog, String employeeIdentificator) {
        workLog.setIsConfirmedByEmployee(true);
        workLog.setIsConfirmedByManager(false);
        workLog.setEmployee(employeeService.findByEmployeeIdentificatorDTO(employeeIdentificator));
    }

    private void updateWorklogApprovalByManager(WorkLogDTO workLog, String employeeIdentificator) {
        workLog.setIsConfirmedByEmployee(true);
        workLog.setIsConfirmedByManager(true);
        workLog.setEmployee(employeeService.findByEmployeeIdentificatorDTO(employeeIdentificator));
    }

    @Override
    public void updateManagerApproval(List<WorkLogDTO> workLogs, String employeeIdentificator) {
        for (WorkLogDTO workLog : workLogs) {
            List<WorkLog> worklogsToBeRemoved = workLogRepository.findByEmployeeWeekAndYear(employeeMapper.mapToEmployee(workLog.getEmployee(), new CycleAvoidingMappingContext()),
                    workLog.getWeek(), workLog.getYear());
            worklogsToBeRemoved.forEach(w -> workLogRepository.delete(w));
        }
        workLogs.forEach(worklog -> updateWorklogApprovalByManager(worklog, employeeIdentificator));
        for (WorkLogDTO workLog : workLogs) {
            workLogRepository.save(workLogMapper.mapToWorkLog(workLog, new CycleAvoidingMappingContext()));
        }
    }

    private Function<? super WorkLogDTO, ? extends WorkLog> saveWorklog() {
        return workLog -> workLogRepository
                .save(workLogMapper.mapToWorkLog(workLog, new CycleAvoidingMappingContext()));
    }

    private long calculateLeftHours(EmployeeDTO employee, ProjectDTO project) {

        long workedHours = calculateWorkedHours(project, employee);
        long maxHours = projectEmployeeMaxHoursService.findMaxHoursForProjectAndEmployee(project, employee);

        return (maxHours - workedHours);
    }

    private long calculateWorkedHours(ProjectDTO project, EmployeeDTO employee) {

        List<WorkLog> workLogs = workLogRepository.findByEmpIdentificatorAndProjectNumber(employee.getIdentificator(),
                project.getNumber());
        long workedHours = 0;

        // workLogs.stream().forEach(w -> workedHours += w.getWorkedHours());
        for (WorkLog workLog : workLogs) {

            workedHours += workLog.getWorkedHours();
        }

        return workedHours;
    }

    @Override
    public long calculateHoursForProject(int week, int year, List<EmployeeDTO> employees, ProjectDTO project) {

        long sumOfHours = 0;

        for (EmployeeDTO employeeDTO : employees) {

            List<WorkLog> workLogs = workLogRepository.findByEmployeeProjectWeekAndYear(
                    employeeMapper.mapToEmployee(employeeDTO, new CycleAvoidingMappingContext()),
                    projectMapper.mapToProject(project, new CycleAvoidingMappingContext()), week, year);

            if (workLogs != null) {
                sumOfHours += sumWorkedHours(workLogs);
            }
        }

        return sumOfHours;
    }

    private long sumWorkedHours(List<WorkLog> workLogs) {

        return workLogs.stream().map(WorkLog::getWorkedHours).reduce(0L, Long::sum);
    }

    @Override
    public long calculateHoursForEmployee(EmployeeDTO employee, List<ProjectDTO> projects, int week, int year) {

        long workedHoursSum = 0;

        for (ProjectDTO projectDTO : projects) {
            List<WorkLog> workedHoursList = workLogRepository.findByEmployeeProjectWeekAndYear(
                    employeeMapper.mapToEmployee(employee, new CycleAvoidingMappingContext()),
                    projectMapper.mapToProject(projectDTO, new CycleAvoidingMappingContext()), week, year);

            if (workedHoursList != null) {
                for (WorkLog workLog : workedHoursList) {
                    workedHoursSum += workLog.getWorkedHours();
                }

            }
        }

        return workedHoursSum;
    }

    private static class WorklogComparator implements Comparator<WorkLogDTO> {
        public int compare(WorkLogDTO workLog1, WorkLogDTO workLog2) {
            if (Objects.equals(workLog1.getYear(), workLog2.getYear())) {
                return Integer.compare(workLog1.getWeek(), workLog2.getWeek());
            } else {
                return Integer.compare(workLog1.getYear(), workLog2.getYear());
            }
        }
    }
    //
//		if (thatYear == yearFrom && thatYear != yearTo) {
//			if (thatWeek >= weekFrom) {
//				return true;
//			} else {
//				return false;
//			}
//		}
//
//		if (thatYear == yearTo && thatYear != yearFrom) {
//			if (thatWeek <= weekTo) {
//				return true;
//			} else {
//				return false;
//			}
//		}
//
//		if (thatYear > yearFrom && thatYear < yearTo) {
//			return true;
//		}
//
//		if (thatYear == yearFrom && thatYear == yearTo) {
//			if (thatWeek >= weekFrom) {
//				if (thatWeek <= weekTo) {
//					return true;
//				} else {
//					return false;
//				}
//			} else {
//				return false;
//			}
//		}
//
//		return false;
//	}

}

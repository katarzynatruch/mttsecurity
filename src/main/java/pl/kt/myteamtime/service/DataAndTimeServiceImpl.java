package pl.kt.myteamtime.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;

import org.joda.time.DateTime;
import org.joda.time.Weeks;
import org.springframework.stereotype.Service;

@Service
public class DataAndTimeServiceImpl implements DataAndTimeService {

	@Override
	public int getCurrentWeekGlobal() {

		LocalDate currentDate = LocalDate.now();
		Date date = Date.valueOf(currentDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		return calendar.get(Calendar.WEEK_OF_YEAR);
	}

	@Override
	public int getCurrenYearGlobal() {

		LocalDate currentDate = LocalDate.now();

		return currentDate.getYear();

	}

	@Override
	public int numberOfWeeksFromDateToDate(int weekFrom, int yearFrom, int weekTo, int yearTo) {

		Calendar calendarFrom = Calendar.getInstance();
		calendarFrom.set(Calendar.YEAR, yearFrom);
		calendarFrom.set(Calendar.WEEK_OF_YEAR, weekFrom);
		java.util.Date dateFrom = calendarFrom.getTime();

		Calendar calendarTo = Calendar.getInstance();
		calendarTo.set(Calendar.YEAR, yearTo);
		calendarTo.set(Calendar.WEEK_OF_YEAR, weekTo);
		java.util.Date dateTo = calendarTo.getTime();

		DateTime dateTimeFrom = new DateTime(dateFrom);
		DateTime dateTimeTo = new DateTime(dateTo);

		return Weeks.weeksBetween(dateTimeFrom, dateTimeTo).getWeeks();
	}

	@Override
	public int getMaxWeekOfTheYear(int year) {

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);

		return calendar.getMaximum(Calendar.WEEK_OF_YEAR);
	}

}

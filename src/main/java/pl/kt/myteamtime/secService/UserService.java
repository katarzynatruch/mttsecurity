package pl.kt.myteamtime.secService;

import pl.kt.myteamtime.dto.UserDTO;
import pl.kt.myteamtime.dts.UserDts;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.secEntity.ResetToken;
import pl.kt.myteamtime.secEntity.VerificationToken;
import pl.kt.myteamtime.authExceptions.EmailExistsException;

import java.util.List;

public interface UserService {
    User registerNewUserAccount(UserDTO accountDto, Employee employee) throws EmailExistsException;

    void createVerificationToken(User user, String token);

    VerificationToken getVerificationToken(String token);

    VerificationToken getVerificationTokenByUser(String userIdentificator);

    ResetToken getResetToken(String token);

    ResetToken getResetTokenByUser(String userIdentificator);

    void saveRegisteredUser(User user);

    User getUser(String verificationToken);

    User findByIdentificator(String userIdentificator);

    VerificationToken generateNewVerificationToken(String existingToken);

    User findUserByEmail(String userEmail);

    User findUserByIdentificator(String userIdentificator);

     User findById(Long id);

    void createPasswordResetTokenForUser(User user, String token);

    public void changeUserPassword(User user, String password);

    boolean checkIfValidOldPassword(String oldPasswordFromSystem, String oldPassword);

    void saveUser(User user);

    List<UserDTO> findAll();

    boolean saveEditedUser(UserDTO accountDto);

    public void deactivateUser(String userIdentificator);

    UserDts findAllNotDeactivated();

}

package pl.kt.myteamtime.secService;

public interface SecurityService {
    String validatePasswordResetToken(long id, String token);
    String validatePasswordNewRegisteredToken(long id, String token);
}

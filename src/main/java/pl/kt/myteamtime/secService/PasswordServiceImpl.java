package pl.kt.myteamtime.secService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.secRepository.UserRepository;
import pl.kt.myteamtime.secEntity.Password;
import pl.kt.myteamtime.secEntity.ResetToken;
import pl.kt.myteamtime.secRepository.ResetTokenRepository;

@Service
public class PasswordServiceImpl implements PasswordService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ResetTokenRepository resetTokenRepository;

    @Override
    public void createResetToken(Password password, String token) {
        ResetToken resetToken = new ResetToken();
        resetToken.setToken(token);
        resetToken.getUser().setEmail(password.getEmail());
        resetToken.getUser().setIdentificator((password.getIdentificator()));

        resetTokenRepository.save(resetToken);
    }

    @Override
    public boolean confirmResetToken(ResetToken token) {
        return false;
    }

    @Override
    public void update(String password, String identificator) {
        User user = userRepository.findActiveByIdentificator(identificator);
        user.setPassword(password);
        userRepository.save(user);

//        passwordRepository.update(password, identificator);
    }

    public void saveToken(ResetToken resetToken) {
        resetTokenRepository.save(resetToken);
    }

    public ResetToken findByToken(String token) {
       return resetTokenRepository.findByToken(token);

    }


}

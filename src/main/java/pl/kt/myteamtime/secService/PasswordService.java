package pl.kt.myteamtime.secService;


import pl.kt.myteamtime.secEntity.Password;
import pl.kt.myteamtime.secEntity.ResetToken;

public interface PasswordService {

    void createResetToken(Password password, String token);

    boolean confirmResetToken(ResetToken token);

    void update(String password, String identificator);

}

package pl.kt.myteamtime.secService;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.dto.UserDTO;
import pl.kt.myteamtime.dts.UserDts;
import pl.kt.myteamtime.entity.Deactivated;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.repository.DeactivatedRepository;
import pl.kt.myteamtime.repository.RoleRepository;
import pl.kt.myteamtime.secEntity.ResetToken;
import pl.kt.myteamtime.entity.Role;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.secEntity.VerificationToken;
import pl.kt.myteamtime.mapper.CycleAvoidingMappingContext;
import pl.kt.myteamtime.mapper.UserMapper;
import pl.kt.myteamtime.secRepository.ResetTokenRepository;
import pl.kt.myteamtime.secRepository.TokenRepository;
import pl.kt.myteamtime.secRepository.UserRepository;
import pl.kt.myteamtime.authExceptions.EmailExistsException;
import pl.kt.myteamtime.authExceptions.ValidUserNoPasswordException;
import pl.kt.myteamtime.service.EmployeeService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private DeactivatedRepository deactivatedRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ResetTokenRepository resetTokenRepository;

    @Autowired
    private EmployeeService employeeService;

    @Override
    public User registerNewUserAccount(UserDTO accountDto, Employee employee) throws EmailExistsException {
        if (emailExist(accountDto.getEmail())) {
            throw new EmailExistsException("There is an account with that email address: " + accountDto.getEmail());
        }
        accountDto.setEnabled(false); // enabled means that account is validated by User
        accountDto.setActive(true); // active means that account is not expired
        List<Role> roles = getRoles(accountDto);
        User newUser = mapNewUser(accountDto, roles);
        accountDto.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        accountDto.setMatchingPassword(passwordEncoder.encode(accountDto.getPassword()));
        if(employee == null) {
            employee = employeeService.findByEmployeeIdentificator(accountDto.getIdentificator());
        }
        newUser.setEmployee(employee);
        User savedUser = userRepository.save(newUser);
//        VerificationToken myToken = new VerificationToken(UUID.randomUUID().toString(), savedUser);
//        tokenRepository.save(myToken);
        return savedUser;
    }

    private User mapNewUser(UserDTO accountDto, List<Role> roles) {
        User newUser = new User();
        newUser.setRoles(roles);
        newUser.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        newUser.setFirstName(accountDto.getFirstName());
        newUser.setLastName(accountDto.getLastName());
        newUser.setIdentificator(accountDto.getIdentificator());
        newUser.setEmail(accountDto.getEmail());
        newUser.setEnabled(accountDto.isEnabled());
        newUser.setActive(accountDto.isActive());
        newUser.setDeactivated(false);
        return newUser;
    }

    private List<Role> getRoles(UserDTO accountDto) {
        List<String> roleNames = new ArrayList<>();
        List<Role> roles = new ArrayList<>();
        roleNames.add(accountDto.getRole1());
        roleNames.add(accountDto.getRole2());
        roleNames.add(accountDto.getRole3());
        roleNames.add(accountDto.getRole4());
        roleNames.add(accountDto.getRole5());
        for (String r : roleNames) {
            if (r != null) {
                Role role = roleRepository.findByName(r);
                roles.add(role);
            }
        }
        return roles;
    }

    @Override
    public void saveRegisteredUser(User user) {
        if (userRepository.findActiveByIdentificator(user.getIdentificator()) != null) {

            User foundUser = userRepository.findActiveByIdentificator(user.getIdentificator());
            if (foundUser.isEnabled()) {
                if (foundUser.getPassword() == null) {
                    throw new ValidUserNoPasswordException();
                }
            }
        }

        employeeService.saveEmployee(user);
        userRepository.save(user);
    }

    @Override
    public User getUser(String verificationToken) {
        return tokenRepository.findByToken(verificationToken).getUser();
    }

    @Override
    public User findByIdentificator(String userIdentificator) {
        return userRepository.findActiveByIdentificator(userIdentificator);
    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id).get();
    }

    @Override
    public VerificationToken generateNewVerificationToken(String existingToken) {
        VerificationToken vToken = tokenRepository.findByToken(existingToken);
        vToken.updateToken(UUID.randomUUID().toString());
        vToken = tokenRepository.save(vToken);
        return vToken;
    }

    @Override
    public User findUserByEmail(String userEmail) {
        User activeByEmail = userRepository.findActiveByEmail(userEmail);
        return userRepository.findActiveByEmail(userEmail);
    }

    @Override
    public User findUserByIdentificator(String userIdentificator) {
        Optional<User> user = Optional.ofNullable(userRepository.findActiveByIdentificator(userIdentificator));
        if (user.isPresent()) {
            return userRepository.findActiveByIdentificator(userIdentificator);
        } else {
            return null;
        }
    }

    private boolean emailExist(String email) {
        User user = userRepository.findActiveByEmail(email);
        if (user != null) {
            return true;
        }
        return false;
    }

    public void createPasswordResetTokenForUser(User user, String token) {
        ResetToken myToken = new ResetToken(token, user);
        resetTokenRepository.save(myToken);
    }

    @Override
    public void createVerificationToken(User user, String token) {
        VerificationToken myToken = new VerificationToken(token, user);
        tokenRepository.save(myToken);
    }

    @Override
    public VerificationToken getVerificationToken(String token) {
        return tokenRepository.findByToken(token);
    }

    @Override
    public VerificationToken getVerificationTokenByUser(String userIdentificator) {
        if (tokenRepository.findListByUser(findUserByIdentificator(userIdentificator)).isPresent()) {
            List<VerificationToken> tokens = tokenRepository.findListByUser(findUserByIdentificator(userIdentificator)).get();
            List<VerificationToken> filteredTokens = tokens.stream().filter(t -> !checkIfTokenExpired(t)).collect(Collectors.toList());
            if (filteredTokens.size() == 1) {
                return filteredTokens.get(0);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public ResetToken getResetToken(String token) {
        return resetTokenRepository.findByToken(token);
    }

    @Override
    public ResetToken getResetTokenByUser(String userIdentificator) {
        if (resetTokenRepository.findListByUser(findUserByIdentificator(userIdentificator)).isPresent()) {
            List<ResetToken> tokens = resetTokenRepository.findListByUser(findUserByIdentificator(userIdentificator)).get();
            List<ResetToken> filteredTokens = tokens.stream().filter(t -> !checkIfTokenExpired(t)).collect(Collectors.toList());
            if (filteredTokens.size() == 1) {
                return filteredTokens.get(0);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private boolean checkIfTokenExpired(VerificationToken token) {
        Calendar cal = Calendar.getInstance();
        if ((token.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            return true;
        }
        return false;
    }

    private boolean checkIfTokenExpired(ResetToken token) {
        Calendar cal = Calendar.getInstance();
        if ((token.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            return true;
        }
        return false;
    }

    @Override
    public void changeUserPassword(User user, String password) {
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
    }

    @Override
    public boolean checkIfValidOldPassword(String oldPassword, String oldPasswordFromSystem) {
        return passwordEncoder.matches(oldPassword, oldPasswordFromSystem);
    }

    @Override
    public void saveUser(User user) {

        userRepository.save(user);
    }

    @Override
    public List<UserDTO> findAll() {
        return userMapper.mapToUserDTOList(userRepository.findAll(), new CycleAvoidingMappingContext());
    }

    @Override
    public boolean saveEditedUser(UserDTO accountDto) {
        //TODO return true if successfully saved to database
        User user = findUserByIdentificator(accountDto.getIdentificator());
        user.setEmail(accountDto.getEmail());
        user.setFirstName(accountDto.getFirstName());
        user.setLastName(accountDto.getLastName());
        userRepository.save(user);
        employeeService.saveEmployee(user);
        return true;
    }

    @Override
    public void deactivateUser(String userIdentificator) {
        User user = findUserByIdentificator(userIdentificator);
        Deactivated deactivated = new Deactivated();
        deactivated.setFirstName(user.getFirstName());
        deactivated.setLastName(user.getLastName());
        deactivated.setIdentificator(user.getIdentificator());
        deactivated.setEmail(user.getEmail());
        deactivated.setId(user.getId());
        deactivatedRepository.save(deactivated);
        user.setDeactivated(true);
        user.setActive(false);
        user.setEnabled(false);
        userRepository.save(user);
    }

    @Override
    public UserDts findAllNotDeactivated() {
        if (userRepository.findAllNotDeactivated().isPresent()) {
            List<User> users = userRepository.findAllNotDeactivated().get();
            UserDts userDts = new UserDts();
            userDts.setUsers(userMapper.mapToUserDTOList(users, new CycleAvoidingMappingContext()));
            return userDts;
        }
        return null;
    }


}

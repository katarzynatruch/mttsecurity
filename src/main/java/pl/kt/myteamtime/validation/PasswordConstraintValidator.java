package pl.kt.myteamtime.validation;

import com.google.common.base.Joiner;
import edu.vt.middleware.password.*;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

	@Override
	public void initialize(final ValidPassword arg0) {

	}

	@Override
	public boolean isValid(final String passwordPlain, final ConstraintValidatorContext context) {
		// @formatter:off

		List<Rule> ruleList = new ArrayList<>();
		ruleList.add(new DigitCharacterRule(1));
		ruleList.add(new LengthRule(5,999));
		ruleList.add(new WhitespaceRule());

		final PasswordValidator validator = new PasswordValidator(ruleList);

		Password password = new Password(passwordPlain);

		RuleResult result = validator.validate(new PasswordData(password));
		if (result.isValid()) {
			return true;
		}
		context.disableDefaultConstraintViolation();
		context.buildConstraintViolationWithTemplate(Joiner.on(",").join(validator.getMessages(result)))
				.addConstraintViolation();
		return false;
	}

}

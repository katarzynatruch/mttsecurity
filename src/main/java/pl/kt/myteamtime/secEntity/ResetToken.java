package pl.kt.myteamtime.secEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.kt.myteamtime.entity.AbstractEntity;
import pl.kt.myteamtime.entity.User;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "reset_token")
public class ResetToken extends AbstractEntity {

    // 60 * 24  =  24 hours
    public static final int EXPIRATION = 30;


    private String token;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    private Date expiryDate;

    public Date calculateExpiryDate(final int expiryTimeInMinutes) {
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(new Date().getTime());
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return new Date(cal.getTime().getTime());
    }

    public ResetToken() {
    }

    public ResetToken(String token, User user) {
        this.token = token;
        this.user = user;
        this.expiryDate = calculateExpiryDate(EXPIRATION);
    }
}

package pl.kt.myteamtime.secEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;

@Data
@EqualsAndHashCode(callSuper=false)
public class Registration {

    @NotEmpty
    private String name;

}

package pl.kt.myteamtime.ds;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.AvailabilityDTO;
import pl.kt.myteamtime.dto.EmployeeDTO;

@Data
public class AvailabilitySingleEmployeeUpdateDs {

	private Long employeeId;
	private List<AvailabilityDTO> availability;
}

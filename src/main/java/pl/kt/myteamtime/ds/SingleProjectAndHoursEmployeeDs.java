package pl.kt.myteamtime.ds;

import lombok.Data;
import pl.kt.myteamtime.dto.ProjectDTO;

@Data
public class SingleProjectAndHoursEmployeeDs {

	private long hoursUsed;
	private long hoursLeft;
	private ProjectDTO project;
	private int weekFrom;
	private int weekTo;
	private int yearFrom;
	private int yearTo;

}
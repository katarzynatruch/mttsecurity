package pl.kt.myteamtime.ds;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dto.ProjectEmployeeMaxHoursDTO;

@Data
public class HoursConsumptionPerWeekDs {

	private ProjectDTO project;
	private List<HoursConsumptionForEmployees> hoursConsumptionForEmployees;
	private List<ProjectEmployeeMaxHoursDTO> projectEmployeeMaxHoursDTOList;
	
}

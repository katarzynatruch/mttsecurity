package pl.kt.myteamtime.ds;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeProjectMaxHoursDs {
    String projectNumber;
    String employeeIdentificator;
    Long maxHours;

}

package pl.kt.myteamtime.ds;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.TeamDTO;

@Data
public class EmployeeTeamUpdateDs {
	private TeamDTO team;
	private List<String> employeeIdentificatorList;
	private String managerIdentificator;

}

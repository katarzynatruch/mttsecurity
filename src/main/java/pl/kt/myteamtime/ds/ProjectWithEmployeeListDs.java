package pl.kt.myteamtime.ds;

import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;

@Getter
@Setter
public class ProjectWithEmployeeListDs {
	ProjectDTO project;
	List<EmployeeDTO> employees;
}

package pl.kt.myteamtime.ds;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.EmployeeDTO;

@Data
public class HoursConsumptionForEmployees {

	
	private List<EmployeeDTO> employees;
	//User hours in project to that week -> sum of previous weeks
	private long usedHours;
	private int week;
	private int year;
	
	
}

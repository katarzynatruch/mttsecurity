package pl.kt.myteamtime.ds;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectAndEmployeeDs {
    String projectNumber;
    String employeeIdentificator;
}

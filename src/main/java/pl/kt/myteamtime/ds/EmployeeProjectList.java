package pl.kt.myteamtime.ds;

import java.util.List;

import lombok.Data;

@Data
public class EmployeeProjectList {

	List<ProjectNumberWithMaxHoursDs> projectNumberWithMaxHoursDsList;
	private String employeeIdentificator;

}

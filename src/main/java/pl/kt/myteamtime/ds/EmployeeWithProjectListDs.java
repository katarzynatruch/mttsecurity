package pl.kt.myteamtime.ds;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;

@Data
public class EmployeeWithProjectListDs {

	EmployeeDTO employee;
	List<ProjectDTO> projects;
	
}

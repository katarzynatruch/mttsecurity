package pl.kt.myteamtime.ds;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.WorkLogDTO;

@Data
public class WorkLogWeekDs {

	private int week;
	private int year;
	private List<WorkLogDTO> workLogs;
	
	
}

package pl.kt.myteamtime.ds;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ProjectEmployeeList {

	List<String> employeeIdentificators;
	private String projectNumber;

}

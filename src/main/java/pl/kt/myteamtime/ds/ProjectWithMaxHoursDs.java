package pl.kt.myteamtime.ds;

import lombok.Data;
import pl.kt.myteamtime.dto.ProjectDTO;

@Data
public class ProjectWithMaxHoursDs {

	ProjectDTO project;
	Integer maxHours;
}

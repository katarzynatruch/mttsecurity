package pl.kt.myteamtime.ds;

import lombok.Data;

@Data
public class ProjectNumberWithMaxHoursDs {

	private String projectNumber;
 	private Long maxHours;
}

package pl.kt.myteamtime.dts;

import lombok.Getter;
import lombok.Setter;
import pl.kt.myteamtime.enums.ProjectMilestone;
import pl.kt.myteamtime.enums.ProjectType;

import java.util.List;

@Getter
@Setter
public class ProjectNewData {

    private List<String> costCenters;
    private ProjectType[] types;
    private ProjectMilestone[] milestones;

}

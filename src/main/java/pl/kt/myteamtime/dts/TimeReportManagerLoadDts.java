package pl.kt.myteamtime.dts;

import lombok.Getter;
import lombok.Setter;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.WorkLogDTO;

import java.util.List;

@Getter
@Setter
public class TimeReportManagerLoadDts {
    EmployeeDTO employee;
    List<WorkLogDTO> worklogs;
}

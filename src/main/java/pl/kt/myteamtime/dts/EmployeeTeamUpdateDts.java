package pl.kt.myteamtime.dts;

import lombok.Data;
import pl.kt.myteamtime.ds.EmployeeTeamUpdateDs;

import java.util.List;

@Data
public class EmployeeTeamUpdateDts {

	private List<EmployeeTeamUpdateDs> employeeTeamUpdateDsList;

}

package pl.kt.myteamtime.dts;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import pl.kt.myteamtime.validation.PasswordMatches;
import pl.kt.myteamtime.validation.ValidPassword;

@Data
public class PasswordDts {

	@ValidPassword
	private String password;

//	@NotNull
//	@Size(min = 1)
	@PasswordMatches
	private String matchingPassword;

}

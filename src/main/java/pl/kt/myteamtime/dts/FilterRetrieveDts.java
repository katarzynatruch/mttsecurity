package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;

@Data
public class FilterRetrieveDts {

	List<String> employeeIdentificators;
	List<String> projectNumbers;
	
}

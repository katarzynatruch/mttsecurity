package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.ds.AvailabilitySingleEmployeeDs;

@Data
public class AvailabilityForManagerDts {

	private List<AvailabilitySingleEmployeeDs> availabilitySingleEmployeeDsList;

}

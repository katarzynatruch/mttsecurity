package pl.kt.myteamtime.dts;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import pl.kt.myteamtime.ds.ProjectsAndTimeForEmployeeDs;
import pl.kt.myteamtime.ds.WorkLogWeekDs;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;

import java.util.List;

@Getter
@Setter
public class EmployeeTimeReportDts {

	private ProjectsAndTimeForEmployeeDs projectsAndTimeForEmployeeDs;
	private EmployeeDTO employee;
	private List<WorkLogWeekDs> workLogWeeks;
	private int currentWeek;
	private int currentYear;

	
	
}

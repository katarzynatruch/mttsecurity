package pl.kt.myteamtime.dts;

import lombok.Data;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ReminderMessageDTO;

import java.util.List;

@Data
public class EmployeesWorkLogConfirmedDts {

	private int currentWeek;
	private int currentYear;
	private List<TimeReportManagerLoadDts> employeesWithWorklogs;
	private ReminderMessageDTO reminderMessageDTO;

	
	
}

package pl.kt.myteamtime.dts;

import lombok.Data;

@Data
public class WorkLogDeleteDts {

    private Long employeeId;
    private Long workLogId;
}

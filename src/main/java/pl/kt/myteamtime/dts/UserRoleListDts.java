package pl.kt.myteamtime.dts;

import lombok.Data;
import pl.kt.myteamtime.entity.Role;

import java.util.List;

@Data
public class UserRoleListDts {

    List<String> userRoleNameList;
}

package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.ProjectDTO;

@Data
public class GraphFilterListDts {

	List<EmployeeTeamLoadDts> employeesInTeams;
	List<ProjectDTO> commonProjects;
	List<ProjectDTO> regularProjects;
	
	
}

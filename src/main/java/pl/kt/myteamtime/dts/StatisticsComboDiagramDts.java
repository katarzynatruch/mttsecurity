package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.ds.ProjectsAndTimeForEmployeeDs;

@Data
public class StatisticsComboDiagramDts {

	private List<ProjectsAndTimeForEmployeeDs> projectsAndTimeForEmployeeDs;



}

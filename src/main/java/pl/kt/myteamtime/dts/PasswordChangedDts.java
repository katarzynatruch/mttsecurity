package pl.kt.myteamtime.dts;

import lombok.Data;
import pl.kt.myteamtime.validation.PasswordMatches;
import pl.kt.myteamtime.validation.ValidPassword;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class PasswordChangedDts {

	private String oldPassword;

	@ValidPassword
	private String newPassword;

	@PasswordMatches
//	@NotNull
//	@Size(min = 1)
	private String matchingPassword;

}

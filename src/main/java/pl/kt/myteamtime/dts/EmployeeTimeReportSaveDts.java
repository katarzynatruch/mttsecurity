package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.ds.WorkLogWeekDs;

@Data
public class EmployeeTimeReportSaveDts {

	private String employeeIdentificator;
	private List<WorkLogWeekDs> workLogWeeks;
}

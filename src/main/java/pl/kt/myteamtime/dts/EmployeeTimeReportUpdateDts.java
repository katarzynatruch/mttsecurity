package pl.kt.myteamtime.dts;

import lombok.Data;

@Data
public class EmployeeTimeReportUpdateDts {
    private String identificator;
}

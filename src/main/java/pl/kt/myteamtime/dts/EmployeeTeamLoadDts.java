package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.TeamDTO;

@Getter
@Setter
public class EmployeeTeamLoadDts {
	private TeamDTO team;
	private List<EmployeeDTO> employees;
	private String managerIdentificator;

}

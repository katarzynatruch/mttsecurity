package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.EmployeeDTO;

@Data
public class EmployeesDts {

	List<EmployeeDTO> employees;
	
}

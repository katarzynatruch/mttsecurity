package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamManagementListDts {

	private List<EmployeeTeamLoadDts> employeeTeamLoadDts;

}

package pl.kt.myteamtime.dts;

import lombok.Data;

import java.util.List;

@Data
public class FilterDataRetrieveDts {

	List<String> employeeIdentificators;
	int weekFrom;
	int weekTo;
	int yearFrom;
	int yearTo;
	
}

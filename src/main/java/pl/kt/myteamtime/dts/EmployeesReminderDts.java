package pl.kt.myteamtime.dts;

import lombok.Data;

import java.util.List;

@Data
public class EmployeesReminderDts {

	private List<String> employeeIdentificators;
	private String message;

	
	
}

package pl.kt.myteamtime.dts;

import lombok.Data;

@Data
public class LoggedInUserIdentificatorDts {
    String loggedInUserIdentificator;
}

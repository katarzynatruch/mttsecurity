package pl.kt.myteamtime.dts;

import lombok.Data;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ReminderMessageDTO;

import java.util.List;

@Data
public class EmployeesTimeReportNotConfirmedDts {

	private int currentWeek;
	private int currentYear;
	private List<EmployeeDTO> employees;
	private ReminderMessageDTO reminderMessageDTO;

	
	
}

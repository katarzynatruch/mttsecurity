package pl.kt.myteamtime.dts;

import lombok.Data;
import pl.kt.myteamtime.validation.ValidEmail;
import pl.kt.myteamtime.validation.ValidPassword;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class EmailDts {

	@ValidEmail
	private String email;


}

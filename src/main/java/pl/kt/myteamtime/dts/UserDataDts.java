package pl.kt.myteamtime.dts;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDataDts {
    private String firstName;
    private String lastName;
    private String identificator;
}

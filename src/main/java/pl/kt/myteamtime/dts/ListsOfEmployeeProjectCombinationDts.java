package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import pl.kt.myteamtime.ds.EmployeeWithProjectListDs;
import pl.kt.myteamtime.ds.ProjectWithEmployeeListDs;
import pl.kt.myteamtime.dto.ProjectEmployeeMaxHoursDTO;

@Getter
@Setter
public class ListsOfEmployeeProjectCombinationDts {

	List<EmployeeWithProjectListDs> employeeWithProjectListDs;
	List<ProjectWithEmployeeListDs> projectWithEmployeeListDs;
	List<ProjectEmployeeMaxHoursDTO> maxHoursDs;
	
}

package pl.kt.myteamtime.dts;

import lombok.Getter;
import lombok.Setter;
import pl.kt.myteamtime.ds.ProjectEmployeeList;

import java.util.List;

@Getter
@Setter
public class ListsOfProjectEmployeeDts {
    List<ProjectEmployeeList> projectEmployeesList;
}

package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;

@Data
public class HoursEstimationRetrieveDts {

	private List<String> employeeIdentificators;
	private List<String> projectNumbers;
	private int weekFrom;
	private int yearFrom;
	private int weekTo;
	private int yearTo;
}

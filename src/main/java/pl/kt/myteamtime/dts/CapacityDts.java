package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.ds.CapacityOfEmployeeDs;

@Data
public class CapacityDts {

	List<CapacityOfEmployeeDs> capacityOfEmployees;
	
}

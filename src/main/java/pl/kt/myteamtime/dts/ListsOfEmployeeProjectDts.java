package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.ds.EmployeeProjectList;

@Data
public class ListsOfEmployeeProjectDts {

	List<EmployeeProjectList> employeeProjectList;

}

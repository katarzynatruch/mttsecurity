package pl.kt.myteamtime.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "team")
public class Team extends AbstractEntity {


	@Column(name = "team_name", unique = true)
	private String name;

	// Relationss

//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "team", cascade = { CascadeType.DETACH, CascadeType.REFRESH })
//	private List<WorkLog> workLogs;

	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH })
	@JoinColumn(name = "manager_id")
	private User manager;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "employee_team", joinColumns = @JoinColumn(name = "team_id"), inverseJoinColumns = @JoinColumn(name = "employee_id"))
	private List<Employee> employees;


}

package pl.kt.myteamtime.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "work_log")
public class WorkLog extends AbstractEntity{

	@Column(name = "worked_hours")
	private long workedHours;
	@Column(name = "week")
	private Integer week;
	@Column(name = "year")
	private Integer year;
	@Column(name = "is_confirmed_by_employee")
	private Boolean isConfirmedByEmployee;
	@Column(name = "is_confirmed_by_manager")
	private Boolean isConfirmedByManager;

	// Relations

	@ManyToOne
	@JoinColumn(name = "employee_id")
	private Employee employee;

	@ManyToOne
	@JoinColumn(name = "project_id")
	private Project project;

//	@ManyToOne(cascade = { CascadeType.DETACH,  CascadeType.REFRESH })
//	@JoinColumn(name = "team_id")
//	private Team team;


}
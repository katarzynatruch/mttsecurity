package pl.kt.myteamtime.entity;

import java.util.List;

import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pl.kt.myteamtime.enums.ProjectType;

@Getter
@Setter
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "project")
public class Project extends AbstractEntity{

	@Column(name = "name")
	private String name;
	@Column(name = "number", unique = true)
	private String number;
	@Column(name = "cost_center")
	private String costCenter;
	@Column(name = "project_max_hours")
	private Long maxHours;
	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private ProjectType type;
	@Column(name = "is_active")
	private Boolean isActive;
	

	// Relations

	@ManyToMany(mappedBy = "projects")
//	@JoinTable(name = "project_employee_max_hours", joinColumns = @JoinColumn(name = "project_id"), inverseJoinColumns = @JoinColumn(name = "employee_id"))
	private List<Employee> employees;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = { CascadeType.DETACH, CascadeType.REFRESH })
	private List<ProjectEmployeeMaxHours> listOfProjectEmployeeMaxHours;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = { CascadeType.DETACH, CascadeType.REFRESH })
	private List<WorkLog> workLogs;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = { CascadeType.DETACH, CascadeType.REFRESH })
	private List<Deadline> deadlines;
	
	public Project() {
	}

}
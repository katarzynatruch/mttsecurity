package pl.kt.myteamtime.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "message")
public class ReminderMessage extends AbstractEntity {

    @Column(name = "defaultReminderMessage")
    private String defaultReminderMessage;

    @OneToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH })
    @JoinColumn(name = "manager_id")
    private User manager;

}


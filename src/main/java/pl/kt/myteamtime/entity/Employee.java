package pl.kt.myteamtime.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Employee")
public class Employee extends AbstractEntity {

	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "email")
	private String email;
	@Column(name = "identificator", unique = true)
	private String identificator;
	@Column(name = "is_active")
	private Boolean isActive;
	@Column(name = "deactivated")
	private Boolean deactivated;

	// Relations

//	@OneToOne(mappedBy = "user")
//	@OneToOne(mappedBy = "employee")
//	private User user;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "employee_team", joinColumns = @JoinColumn(name = "employee_id"), inverseJoinColumns = @JoinColumn(name = "team_id"))
	private Set<Team> teams;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "project_employee_max_hours", joinColumns = @JoinColumn(name = "employee_id"), inverseJoinColumns = @JoinColumn(name = "project_id"))
	private Set<Project> projects;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	private Set<ProjectEmployeeMaxHours> listOfProjectEmployeeMaxHours;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	private Set<WorkLog> workLogs;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	private Set<Availability> availabilityList;

}

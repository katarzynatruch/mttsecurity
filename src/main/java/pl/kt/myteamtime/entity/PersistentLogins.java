package pl.kt.myteamtime.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "persistent_logins")
public class PersistentLogins{

    @Id
    @Column(name = "id", unique = true)
    private Long id;
    @NotNull
    @Column(name = "username")
    private String username;
    @NotNull
    @Column(name = "series", unique = true)
    private String series;
    @NotNull
    @Column(name = "token")
    private String token;
    @NotNull
//    @CreationTimestamp
//    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_used")
    private Date lastUsed;

}

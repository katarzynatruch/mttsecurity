package pl.kt.myteamtime.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "Manager")
public class Manager extends AbstractEntity{

	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "email")
	private String email;
	@Column(name = "identificator", unique = true)
	private String identificator;

	// Relations

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "manager", cascade = { CascadeType.DETACH, CascadeType.REFRESH })
	private List<Team> teams;

	public Manager() {
	}
}
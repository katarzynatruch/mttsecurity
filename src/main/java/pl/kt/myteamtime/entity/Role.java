package pl.kt.myteamtime.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.kt.myteamtime.enums.Roles;

import java.util.List;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = false)
@Data
@Entity
public class Role extends AbstractEntity {

	@Column(name = "role")
	//@Enumerated(EnumType.STRING)
    private String name;

	@ManyToMany(mappedBy = "roles")
	private List<User> users;

}

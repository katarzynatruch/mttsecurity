package pl.kt.myteamtime.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pl.kt.myteamtime.enums.ProjectMilestone;

@Getter
@Setter
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "deadline")
public class Deadline extends AbstractEntity{
	
	@Column(name = "milestone")
	@Enumerated(EnumType.STRING)
	private ProjectMilestone milestone;
	@Column(name = "week")
	private Integer week;
	@Column(name = "year")
	private Integer year;
	@Column(name="comment")
	private String comment;
	
	// Relations
	@JsonIgnore
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH })
	@JoinColumn(name = "project_id")
	private Project project;

}

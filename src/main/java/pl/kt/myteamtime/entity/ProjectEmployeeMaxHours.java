package pl.kt.myteamtime.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Null;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

@Getter
@Setter
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "project_employee_max_hours")
public class ProjectEmployeeMaxHours extends AbstractEntity {


	@ManyToOne
	@JoinColumn(name = "employee_id")
	private Employee employee;
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH })
	@JoinColumn(name = "project_id")
	private Project project;
	@Column(name = "max_hours")
	@Nullable
	private Long maxHours;

}

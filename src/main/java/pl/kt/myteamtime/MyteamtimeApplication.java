package pl.kt.myteamtime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//@ComponentScan(basePackages = {"src"})
@SpringBootApplication
@EnableAutoConfiguration
@EnableSwagger2
@ComponentScan(basePackages = {"src", "pl.kt.myteamtime.security", "AdminController.class"})
public class MyteamtimeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyteamtimeApplication.class, args);
	}

}

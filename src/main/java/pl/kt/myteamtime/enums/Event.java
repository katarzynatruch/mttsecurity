package pl.kt.myteamtime.enums;

public enum Event {

	SL("SL", "Sick leave"), DL("DL", "Delegation"), HOL("HOL", "Holiday"), POT("POT", "Pickup overtime"),
	T("T", "Training"), OT("OT", "Overtime"), HO("HO", "Home office"), PL("PL", "Private leave"), W("W", "Work");

	private String code;
	private String name;

	private Event(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

}

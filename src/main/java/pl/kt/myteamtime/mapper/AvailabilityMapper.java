package pl.kt.myteamtime.mapper;

import java.util.List;

import org.mapstruct.Context;
import org.mapstruct.Mapper;

import pl.kt.myteamtime.dto.AvailabilityDTO;
import pl.kt.myteamtime.entity.Availability;

@Mapper(componentModel = "spring")
public abstract class AvailabilityMapper {

	public abstract Availability mapToAvailability(AvailabilityDTO AvailabilityDTO,
			@Context CycleAvoidingMappingContext context);

	public abstract AvailabilityDTO mapToAvailabilityDTO(Availability Availability,
			@Context CycleAvoidingMappingContext context);

	public abstract List<AvailabilityDTO> mapToAvailabilityDTOList(List<Availability> Availabilitys,
			@Context CycleAvoidingMappingContext context);

	public abstract List<Availability> mapToAvailabilityList(List<AvailabilityDTO> availabilityList,
			@Context CycleAvoidingMappingContext context);

}

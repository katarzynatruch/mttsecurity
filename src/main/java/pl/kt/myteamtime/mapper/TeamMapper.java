package pl.kt.myteamtime.mapper;

import java.util.List;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import pl.kt.myteamtime.dto.TeamDTO;
import pl.kt.myteamtime.entity.Team;

//@Mapper(uses = {EmployeeMapper.class}, componentModel = "spring")
@Mapper(componentModel = "spring")
public abstract class 	TeamMapper {


	public abstract Team mapToTeam(TeamDTO teamDTO, @MappingTarget Team team);

//	@InheritInverseConfiguration(name = "mapToTeam")
//	@Mapping(ignore = true, target = "manager")
//	@Mapping(ignore = true, target = "workLogs")
//	@Mapping(ignore = true, target = "employees")
	public abstract TeamDTO mapToTeamDTO(Team team, @Context CycleAvoidingMappingContext context);
	
//	@Mapping(ignore = true, target = "manager")
//	@Mapping(ignore = true, target = "workLogs")
//	@Mapping(ignore = true, target = "employees")
	public abstract Team mapToTeam(TeamDTO teamDTO, @Context CycleAvoidingMappingContext context);
	
//	@Mapping(ignore = true, target = "manager")
//	@Mapping(ignore = true, target = "workLog")
	public abstract List<TeamDTO> mapToTeamDTOList(List<Team> teams, @Context CycleAvoidingMappingContext context);
	
//	@Mapping(ignore = true, target = "manager")
//	@Mapping(ignore = true, target = "workLog")
	public abstract List<TeamDTO> mapToTeamList(List<TeamDTO> teamsDTO, @Context CycleAvoidingMappingContext context);
}

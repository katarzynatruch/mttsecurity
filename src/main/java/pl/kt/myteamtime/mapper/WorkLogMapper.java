package pl.kt.myteamtime.mapper;

import java.util.List;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import pl.kt.myteamtime.dto.WorkLogDTO;
import pl.kt.myteamtime.entity.WorkLog;

@Mapper(componentModel = "spring")
public abstract class WorkLogMapper {

//	@Mapping(ignore = true, target = "team")
////	@Mapping(ignore = true, target = "employee")
//	@Mapping(ignore = true, target = "project")
	public abstract WorkLog mapToWorkLog(WorkLogDTO workLogDTO, @MappingTarget WorkLog workLog);
	
//	@Mapping(ignore = true, target = "team")
////	@Mapping(ignore = true, target = "employee")
//	@Mapping(ignore = true, target = "project")
	public abstract WorkLog mapToWorkLog(WorkLogDTO workLogDTO, @Context CycleAvoidingMappingContext context);

//	@Mapping(ignore = true, target = "team")
//	@Mapping(ignore = true, target = "employee")
//	@Mapping(ignore = true, target = "project")
	public abstract WorkLogDTO mapToWorkLogDTO(WorkLog workLog, @Context CycleAvoidingMappingContext context);

	//@InheritInverseConfiguration(name = "mapToWorkLogList")
//	@Mapping(ignore = true, target = "team")
//	@Mapping(ignore = true, target = "employee")
//	@Mapping(ignore = true, target = "project")
	public abstract List<WorkLogDTO> mapToWorkLogDTOList(List<WorkLog> worklogs, @Context CycleAvoidingMappingContext context);
}

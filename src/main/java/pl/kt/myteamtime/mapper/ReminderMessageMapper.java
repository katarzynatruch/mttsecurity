package pl.kt.myteamtime.mapper;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import pl.kt.myteamtime.dto.ReminderMessageDTO;
import pl.kt.myteamtime.entity.ReminderMessage;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class ReminderMessageMapper {

	public abstract ReminderMessage mapToReminderMessage(ReminderMessageDTO ReminderMessageDTO,
														 @Context CycleAvoidingMappingContext context);

	public abstract ReminderMessageDTO mapToReminderMessageDTO(ReminderMessage ReminderMessage,
															   @Context CycleAvoidingMappingContext context);

	public abstract List<ReminderMessageDTO> mapToReminderMessageDTOList(List<ReminderMessage> ReminderMessages,
																		 @Context CycleAvoidingMappingContext context);

	public abstract List<ReminderMessage> mapToReminderMessageList(List<ReminderMessageDTO> ReminderMessageList,
																   @Context CycleAvoidingMappingContext context);


}

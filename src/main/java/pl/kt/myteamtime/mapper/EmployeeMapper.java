package pl.kt.myteamtime.mapper;

import java.util.List;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.entity.Employee;

//@Mapper(uses = {TeamMapper.class, ProjectMapper.class}, componentModel = "spring")
@Mapper(uses = { ProjectMapper.class }, componentModel = "spring")
public abstract class EmployeeMapper {

	// @InheritInverseConfiguration(name = "mapToEmployee")
	// @Mapping(ignore = true, target = "teams")
	public abstract EmployeeDTO mapToEmployeeDTO(Employee employee, @Context CycleAvoidingMappingContext context);

	@Mapping(ignore = true, target = "teams")
	public abstract Employee mapToEmployee(EmployeeDTO employeeDTO, @Context CycleAvoidingMappingContext context);

	// @InheritInverseConfiguration(name = "mapToEmployeesList")
	// @Mapping(ignore = true, target = "workLogs")
	public abstract List<EmployeeDTO> mapToEmployeeDTOList(List<Employee> employees,
			@Context CycleAvoidingMappingContext context);

	// @Mapping(ignore = true, target = "workLogs")
	public abstract List<Employee> mapToEmployeeList(List<EmployeeDTO> employeesDTO,
			@Context CycleAvoidingMappingContext context);

}

package pl.kt.myteamtime.mapper;

import org.mapstruct.Context;
import org.mapstruct.Mapper;

import pl.kt.myteamtime.dto.ProjectEmployeeMaxHoursDTO;
import pl.kt.myteamtime.entity.ProjectEmployeeMaxHours;

@Mapper(componentModel = "spring")
public abstract class ProjectEmployeeMaxHoursMapper {

	public abstract ProjectEmployeeMaxHoursDTO mapToProjectEmployeeMaxHoursMapperDTO(ProjectEmployeeMaxHours projectEmployeeMaxHours, @Context CycleAvoidingMappingContext context);

	public abstract ProjectEmployeeMaxHours mapToProjectEmployeeMaxHours(ProjectEmployeeMaxHoursDTO projectEmployeeMaxHoursDTO, @Context CycleAvoidingMappingContext context);

}

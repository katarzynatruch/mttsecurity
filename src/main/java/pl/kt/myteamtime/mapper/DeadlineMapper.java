package pl.kt.myteamtime.mapper;

import java.util.List;

import org.mapstruct.Context;
import org.mapstruct.Mapper;

import pl.kt.myteamtime.dto.DeadlineDTO;
import pl.kt.myteamtime.entity.Deadline;

@Mapper(uses = {ProjectMapper.class}, componentModel = "spring")
public abstract class DeadlineMapper {

	public abstract Deadline mapToDeadline(DeadlineDTO deadlineDTO, @Context CycleAvoidingMappingContext context);
	
	public abstract DeadlineDTO mapToDeadlineDTO(Deadline deadline, @Context CycleAvoidingMappingContext context);
	
	public abstract List<Deadline> mapToDeadlineList(List<DeadlineDTO> deadlinesDTO, @Context CycleAvoidingMappingContext context);
	
	public abstract List<DeadlineDTO> mapToDeadlineDTOList(List<Deadline> deadlines, @Context CycleAvoidingMappingContext context);
	

	
}

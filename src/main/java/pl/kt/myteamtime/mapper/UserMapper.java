package pl.kt.myteamtime.mapper;

import java.util.List;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import pl.kt.myteamtime.dto.UserDTO;
import pl.kt.myteamtime.entity.User;

@Mapper(componentModel = "spring")
public abstract class UserMapper {

	public abstract UserDTO mapToUserDTO(User user, @Context CycleAvoidingMappingContext context);

	public abstract User mapToUser(UserDTO userDTO, @Context CycleAvoidingMappingContext context);

	public abstract List<UserDTO> mapToUserDTOList(List<User> users, @Context CycleAvoidingMappingContext context);

	public abstract List<User> mapToUserList(List<UserDTO> usersDTO, @Context CycleAvoidingMappingContext context);

}
